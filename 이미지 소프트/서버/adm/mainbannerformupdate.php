<?php
$sub_menu = '100330';
include_once('./_common.php');
auth_check_menu($auth, $sub_menu, "w");
if(!$bn_name || !$bn_url)
	alert('필수 입력값이 입력되지 않았습니다.');

if($bn_target != '1') $bn_target = '';
$columns = "
	bn_name = '{$bn_name}'
	, bn_sdate = '{$bn_sdate}'
	, bn_edate = '{$bn_edate}'
	, bn_url = '{$bn_url}'
	, bn_target = '{$bn_target}'
	, bn_order = '{$bn_order}'
";

if($bn_no){
	$sql = "
		update {$g5['banner_table']} set
			{$columns}
		where bn_no = '{$bn_no}'
	";
	sql_query($sql,true);
}else{
	$sql = "
		insert into {$g5['banner_table']} set
			{$columns}
	";
	sql_query($sql, true);
	$bn_no = sql_insert_id();
}

@mkdir(G5_DATA_BANNER_PATH, G5_DIR_PERMISSION);
@chmod(G5_DATA_BANNER_PATH, G5_DIR_PERMISSION);

$bn = sql_fetch("seelct * from {$g5['banner_table']} where bn_no = '{$bn_no}' ");

$chars_array = array_merge(range(0,9), range('a','z'), range('A','Z'));

if(isset($_FILES['bn_file1']['name']) && $_FILES['bn_file1']['name']) {

	$tmp_file  = $_FILES['bn_file1']['tmp_name'];

	$filename  = $_FILES['bn_file1']['name'];
	$filename  = get_safe_filename($filename);

	shuffle($chars_array);
	$shuffle = implode('', $chars_array);

	$filename = preg_replace("/\.(php|pht|phtm|htm|cgi|pl|exe|jsp|asp|inc)/i", "$0-x", $filename);
	$bn_file1 = abs(ip2long($_SERVER['REMOTE_ADDR'])).'_'.substr($shuffle,0,8).'_'.replace_filename($filename);

	$dest_file = G5_DATA_BANNER_PATH.'/'.$bn_file1;

	$error_code = move_uploaded_file($tmp_file, $dest_file) or die($_FILES['bn_file1']['error']);

	chmod($dest_file, G5_FILE_PERMISSION);

	if($bn['bn_file1'])
		@unlink(G5_DATA_BANNER_PATH.'/'.$bn['bn_file1']);
	$sql = " update {$g5['banner_table']} set bn_file1 = '{$bn_file1}' where bn_no = '{$bn_no}' ";

	sql_query($sql, true);

}


if(isset($_FILES['bn_file2']['name']) && $_FILES['bn_file2']['name']) {

	$tmp_file  = $_FILES['bn_file2']['tmp_name'];

	$filename  = $_FILES['bn_file2']['name'];
	$filename  = get_safe_filename($filename);

	shuffle($chars_array);
	$shuffle = implode('', $chars_array);

	$filename = preg_replace("/\.(php|pht|phtm|htm|cgi|pl|exe|jsp|asp|inc)/i", "$0-x", $filename);
	$bn_file2 = abs(ip2long($_SERVER['REMOTE_ADDR'])).'_'.substr($shuffle,0,8).'_'.replace_filename($filename);

	$dest_file = G5_DATA_BANNER_PATH.'/'.$bn_file2;


	$error_code = move_uploaded_file($tmp_file, $dest_file) or die($_FILES['bn_file1']['error']);


	chmod($dest_file, G5_FILE_PERMISSION);

	if($bn['bn_file2'])
		@unlink(G5_DATA_BANNER_PATH.'/'.$bn['bn_file2']);
	$sql = " update {$g5['banner_table']} set bn_file2 = '{$bn_file2}' where bn_no = '{$bn_no}' ";

	sql_query($sql, true);

}


alert('정상적으로 저장되었습니다.',  "./mainbanner.php?{$qstr}");
?>
