<?php
$sub_menu = '100330';
include_once('./_common.php');

if($msg = auth_check_menu($auth, $sub_menu, "w", ture)) die($msg);

if($bn_no){
	$bn = sql_fetch(" select * from {$g5['banner_table']} where bn_no = '{$bn_no}' ", true);

	if(!isset($bn['bn_no']))
		$bn_no = '';
}
?>

<div class="tbl_frm01 tbl_wrap">
	<form id="frm_banner" name="frm_banner" action="./mainbannerformupdate.php" method="post" enctype="multipart/form-data">
	<input type="hidden" name="bn_no" value="<?php echo $bn_no ?>">
	<input type="hidden" name="sst" value="<?php echo $sst ?>">
	<input type="hidden" name="sod" value="<?php echo $sod ?>">
	<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
	<input type="hidden" name="stx" value="<?php echo $stx ?>">
	<input type="hidden" name="page" value="<?php echo $page ?>">
	<input type="hidden" name="token" value="">

    <table>
	    <caption>메인배너</caption>
	    <colgroup>
	        <col class="grid_4">
	        <col>
	    </colgroup>
	    <tbody>
	    <tr>
	        <th scope="row"><label for="bn_name">배너명</label></th>
			<td><input type="text" name="bn_name" id="bn_name" class="frm_input" style="width:100%" value="<?php echo $bn['bn_name']?>"></td>
		</tr>
	    <tr>
	        <th scope="row"><label for="bn_sdate">시작일</label></th>
			<td>
				<input type="text" name="bn_sdate" id="bn_sdate" class="frm_input" value="<?php echo $bn['bn_sdate']?>">
				<input type="checkbox" name="bn_sdate_chk" value="<?php echo date("Y-m-d 00:00:00", G5_SERVER_TIME); ?>" id="bn_sdate_chk" onclick="if (this.checked == true) this.form.bn_sdate.value=this.form.bn_sdate_chk.value; else this.form.bn_sdate.value = this.form.bn_sdate.defaultValue;">
	            <label for="bn_sdate_chk">시작일시를 오늘로</label>
			</td>
		</tr>
	    <tr>
	        <th scope="row"><label for="bn_edate">종료일</label></th>
			<td>
				<input type="text" name="bn_edate" id="bn_edate" class="frm_input" value="<?php echo $bn['bn_edate']?>">
				<input type="checkbox" name="bn_edate_chk" value="<?php echo date("Y-m-d 23:59:59", G5_SERVER_TIME+(60*60*24*7)); ?>" id="bn_edate_chk" onclick="if (this.checked == true) this.form.bn_edate.value=this.form.bn_edate_chk.value; else this.form.bn_edate.value = this.form.bn_edate.defaultValue;">
	            <label for="bn_edate_chk">종료일시를 오늘로부터 7일 후로</label>
			</td>
		</tr>
	    <tr>
	        <th scope="row"><label for="bn_file1">파일(pc)<br>930x355</label></th>
			<td><input type="file" name="bn_file1" id="bn_file1">
			<?php
			if(is_file(G5_DATA_BANNER_PATH."/{$bn['bn_file1']}"))
				echo '<img src="'.G5_DATA_BANNER_URL.'/'.$bn['bn_file1'].'" style="max-width:300px;display:block;margin-top:10px">';
			?>
			</td>
		</tr>
	    <tr>
	        <th scope="row"><label for="bn_file2">파일(모바일)<br>678x430</label></th>
			<td><input type="file" name="bn_file2" id="bn_file2">
			<?php
			if(is_file(G5_DATA_BANNER_PATH."/{$bn['bn_file2']}"))
				echo '<img src="'.G5_DATA_BANNER_URL.'/'.$bn['bn_file2'].'" style="max-width:300px;display:block;margin-top:10px">';
			?>
			</td>
		</tr>
	    <tr>
	        <th scope="row"><label for="bn_url">URL</label></th>
			<td>
				<input type="text" name="bn_url" id="bn_url" class="frm_input" style="width:60%" value="<?php echo $bn['bn_url']?>">
				<label><input type="checkbox" name="bn_target" value="1"<?php echo get_checked("1", $bn['bn_target'])?>> 새창</label>
			</td>
		</tr>
	    <tr>
	        <th scope="row"><label for="bn_order">우선순위</label></th>
			<td>
				<input type="text" name="bn_order" id="bn_order" class="frm_input" style="width:200px" value="<?php echo $bn['bn_order']?>"> 
			</td>
		</tr>
	</table>
	<button type="submit" style="margin-top:10px" class="btn_frmline">저장</button>
	</form>
</div>
