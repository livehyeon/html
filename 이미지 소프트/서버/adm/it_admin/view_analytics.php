<?php
$sub_menu = "500400";
include_once('./_common.php');

$g5['title'] = '많이 본 상품';
include_once(G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

if(!$fr_date) $fr_date = date("Y-m-01", G5_SERVER_TIME);
if(!$to_date) $to_date = date("Y-m-d", G5_SERVER_TIME);


if($type=='year'){
	$txt_date = '년도';

	if(!$fr_date) $fr_date = date("Y-01-01", G5_SERVER_TIME);
	if(!$to_date) $to_date = date("Y-m-d", G5_SERVER_TIME);
	$date 	= '2021';//date("Y", strtotime($fr_date));
	$end 	= date("Y", strtotime($fr_date)); ;

}elseif($type=='month'){
	$txt_date = '년월';

	if(!$fr_date) $fr_date = date("Y-m-01", G5_SERVER_TIME);
	if(!$to_date) $to_date = date("Y-m-d", G5_SERVER_TIME);

	$date = date("Y-m", strtotime($to_date));
	$end = date("Y-m", strtotime($fr_date));

}else{
	$type = "day";
	$txt_date = '일자';
	if(!$fr_date) $fr_date = date("Y-m-d", G5_SERVER_TIME);
	if(!$to_date) $to_date = date("Y-m-d", G5_SERVER_TIME);

	$date = date("Y-m-d", strtotime($to_date));
	$end = $fr_date;
}


?>
<style>
	.anchor a.on{font-weight:bold;background:#fff;}

</style>
<script>
$(function(){
    $("#fr_date, #to_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99", maxDate: "+0d" });
});
</script>

<form name="fvisit" id="fvisit" class="local_sch03 local_sch" method="get" autocomplete="off">
<input type="hidden" name="type" value='<?php echo $type; ?>'>
<div class="sch_last">
    <strong>기간별검색</strong>
    <input type="text" name="fr_date" value="<?php echo $fr_date ?>" id="fr_date" class="frm_input" size="11" maxlength="10">
    <label for="fr_date" class="sound_only">시작일</label>
    ~
    <input type="text" name="to_date" value="<?php echo $to_date ?>" id="to_date" class="frm_input" size="11" maxlength="10">
    <label for="to_date" class="sound_only">종료일</label>
    <input type="submit" value="검색" class="btn_submit">
</div>
</form>

<ul class="anchor">
    <li><a href="./view_analytics.php?type=day&fr_date=<?php echo date("Y-m-d", G5_SERVER_TIME); ?>&to_date=<?php echo date("Y-m-d", G5_SERVER_TIME); ?>"<?php if($type=='day') echo ' class="on"'; ?>>일</a></li>
    <li><a href="./view_analytics.php?type=month&fr_date=<?php echo date("Y-m-01", G5_SERVER_TIME); ?>&to_date=<?php echo date("Y-m-d", G5_SERVER_TIME); ?>"<?php if($type=='month') echo ' class="on"'; ?>>월</a></li>
    <li><a href="./view_analytics.php?type=year&fr_date=<?php echo date("Y-01-01", G5_SERVER_TIME); ?>&to_date=<?php echo date("Y-m-d", G5_SERVER_TIME); ?>"<?php if($type=='year') echo ' class="on"'; ?>>년</a></li>
</ul>

<div class="tbl_head01 tbl_wrap">

    <table>
    <caption>보관함</caption>
	    <thead>
		    <tr>
		        <th scope="col"><?php echo $txt_date; ?></th>
		        <th scope="col">본수</th>
		        <th scope="col">상세내역</th>
		    </tr>
	    </thead>
    	<tbody>
		<?php
		while($date>=$end){
			$length = strlen($date);
			$sql = " select count(a.iv_no) as cnt from {$g5['item_view_table']} a, {$g5['item_table']} b where a.it_id = b.it_id and b.it_use = '1' and substr(a.iv_datetime, 1, {$length}) = '{$date}' ";

			$row = sql_fetch($sql, true );

				if($type=='year'){
					$qstr = "type=month&fr_date=".date("Y-01-01", strtotime($date))."&to_date=".date("Y-12-31", strtotime($date));


				}elseif($type=='month'){
					$qstr = "type=day&fr_date=".date("Y-m-01", strtotime($date))."&to_date=".date("Y-01-t", strtotime($date));


				}else{
					$qstr = "type=day&fr_date={$date}&to_date={$date}";


				}
		?>
			<tr>
				<td><?php echo $date; ?></td>
				<td><?php echo number_format($row['cnt']); ?></td>
				<td><a href="<?php echo G5_ADMIN_URL; ?>/it_admin/view_analytics.php?<?php echo $qstr; ?>" class='btn btn_03'>상세보기</a></td>
			</tr>
		<?php
			if($type=='year'){

				$date = date("Y", strtotime("{$date}-01-01 -1year"));

			}elseif($type=='month'){

				$date = date("Y-m", strtotime("{$date}-01 -1month"));

			}else{
			 
				$date = date("Y-m-d", strtotime("{$date} -1day"));

			}
		}
		?>
        </tbody>
    </table>
</div>

<div class="tbl_head01 tbl_wrap">
	<table>
		<caption>본</caption>
		<thead>
			<tr>
				<th scope="col">No</th>
				<th scope="col">상품이미지</th>
				<th scope="col">상품명</th>
				<th scope="col">본수</th>
			</tr>
		</thead>
		<tbody>
		<?php
		$sql = " select count(a.iv_no) cnt, a.it_id from {$g5['item_view_table']} a, {$g5['item_table']} b where a.it_id = b.it_id and b.it_use = '1' and substr(a.iv_datetime, 1, 10) between '{$fr_date}' and '{$to_date}'   order by cnt desc  ";

		$result = sql_query($sql);
		$idx = 1;
		while($row = sql_fetch_array($result)){
			if(!$row['cnt']) break;
			$it = sql_fetch(" select it_id, it_name, it_thumb from {$g5['item_table']} where it_id = '{$row['it_id']}'", true);
		?>
			<tr>
				<td><?php echo $idx++;?></td>
				<td><?php
				if(is_file(G5_DATA_ITEM_PATH."/{$it['it_thumb']}")){
						echo '<a href="'.G5_ITEM_URL.'/view.php?it_id='.$it['it_id'].'" target="_blank"><img src="'.G5_DATA_ITEM_URL."/{$it['it_thumb']}".'" style="width:60px;"></a>';
				}
				?></td>
				<td><?php echo '<a href="'.G5_ITEM_URL.'/view.php?it_id='.$it['it_id'].'" target="_blank">'.$it['it_name'].'</a>'?></td>
				<td><?php echo number_format($row['cnt'])?></td>
			</tr>
		<?php

		}

		if($idx=='1') echo '<tr><td class="empty" colspan="4">데이터가 없습니다.</td></tr>'.PHP_EOL;
		?>
		</tbody>
		</table>
	</div>
<?php
include_once(G5_ADMIN_PATH.'/admin.tail.php');
?>
