<?php
$sub_menu = "500300";
include_once('./_common.php');

$g5['title'] = '매출';
include_once(G5_ADMIN_PATH.'/admin.head.php');

?>


<div class="tbl_head01 tbl_wrap">

    <table>
    <caption>매출</caption>
	    <thead>
		    <tr>
		        <th scope="col">년도</th>
		        <th scope="col">건수</th>
				<?php for($i=0;$i<count($g5['payment_array']);$i++) { ?>
		        <th scope="col"><?php echo $g5['payment_array'][$i]?></th>
				<?php } ?>
		        <th scope="col">합계</th>
		        <th scope="col">순이익(VAT-3.3%)</th>
		    </tr>
	    </thead>
    	<tbody>
		<?php
		$end = '2021';
		$date = date("Y", G5_SERVER_TIME);
		$total = 0;
		while($date>=$end){
			$row = sql_fetch(" select count(*) cnt, sum(od_amount) amount from {$g5['order_table']} where od_free <> '1' and od_status = '2' and substr(od_paytime, 1, 4) = '{$date}' ", true);

			for($i=0;$i<count($g5['payment_array']);$i++) {
				$payment = $g5['payment_array'][$i];
				$row1 = sql_fetch(" select count(*) cnt, sum(od_amount) amount from {$g5['order_table']} where od_free <> '1' and od_status = '2' and od_payment = '{$payment}' and substr(od_paytime, 1, 4) = '{$date}' ", true);

				$row[$payment] = $row1['amount'];
				$amount = $row1['amount'] - (int)($row1['amount']/11);

				if($payment=='신용카드')
					$amount = $amount - (int)($amount*0.033);

				$total += $amount;
			}
		?>
			<tr>
				<td><?php echo $date; ?></td>
				<td><?php echo number_format($row['cnt']); ?></td>
				<?php for($i=0;$i<count($g5['payment_array']);$i++) { ?>
				<td><?php echo number_format($row[$g5['payment_array'][$i]]); ?>원</td>
				<?php } ?>
				<td><?php echo number_format($row['amount']); ?>원</td>
				<td><?php echo number_format($total); ?>원</td>

			</tr>
		<?php
			$date = date("Y", strtotime("{$date}-01-01 -1year"));
		}
		?>
        </tbody>
    </table>
</div>
<?php
include_once(G5_ADMIN_PATH.'/admin.tail.php');
?>
