<?php
$sub_menu = '400200';
include_once('./_common.php');
include_once(G5_EDITOR_LIB);
auth_check($auth[$sub_menu], "w", true);

$it = array();
$tags = array();
$category = array();
if ($w == "u" && $it_id){
	$it = sql_fetch(" select * from {$g5['item_table']} where it_id = '{$it_id}'");
	if($it['it_tag'])
		$tags = explode("|", $it['it_tag']);

	if($it['it_id']){
		$result = sql_query(" select * from {$g5['item_category_table']} where it_id = '{$it_id}' ");
		while($row=sql_fetch_array($result))
			$category[] = $row['category'];
	}
}

if($it['it_use'] != '0') $it['it_use'] = '1';
$g5['title'] = '상품관리';
include_once(G5_ADMIN_PATH.'/admin.head.php');

add_stylesheet('<link rel="stylesheet" href="'.G5_JS_URL.'/remodal/remodal.css">', 11);
add_stylesheet('<link rel="stylesheet" href="'.G5_JS_URL.'/remodal/remodal-default-theme.css">', 12);
add_javascript('<script src="'.G5_JS_URL.'/remodal/remodal.js"></script>', 10);

?>
<script>

$(function(){
	$("#add-tag").click(function(){
		$tag = $("#it_tag").val().replace(/[^ㄱ-ㅎ|가-힣|a-z|A-Z|0-9|]+$/g,'');
		$("#it_tag").val("");
		if(!$tag) return false;
		$html = '<span class="list-tag">';
		$html += '<input type="hidden" name="tag[]" value="'+$tag+'">';
		$html += $tag
		$html += '<button class="del-tag" type="button">X</button>';
		$html += '</span>';
		$(".tag-line").append($html);
	});
	$(".tag-line").on('click', '.del-tag', function(){
		$(this).parent('.list-tag').remove();
	})
})
</script>
<style>
	.category b{display:inline-block;width:60px;}
	.category label{display:inline-block;padding:0px 10px;}
</style>
<form name="fitem" id="fitem" action="./itemformupdate.php" onsubmit="return fitem_submit(this);" method="post" enctype="multipart/form-data">
<input type="hidden" name="w" value="<?php echo $w ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
<input type="hidden" name="stx" value="<?php echo $stx ?>">
<input type="hidden" name="sst" value="<?php echo $sst ?>">
<input type="hidden" name="sod" value="<?php echo $sod ?>">
<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="it_id" value="<?php echo $it_id ?>">
<input type="hidden" name="token" value="">

<div class="tbl_frm01 tbl_wrap">
	<table>
		<caption><?php echo $g5['title']; ?></caption>
		<colgroup>
			<col class="grid_4">
			<col>

		</colgroup>
		<tbody>
			<tr>
				<th scope="row"><label for="category">분류<strong class="sound_only">필수</strong></label></th>
				<td class="category">
					<?php
					foreach($g5['category_tree_array'] as $ca_id => $array){
						echo '<p>'.PHP_EOL;
						echo '<b>'.$array['name'].'</b>';
						foreach($array['sub'] as $ca_id1 => $array1){
							echo '<label><input type="checkbox" name="category[]" value="'.$ca_id1.'"'.(in_array($ca_id1, $category)?' checked="checked"':'').'> '.$array1['name'].'</label>';
						}
						echo '</p>'.PHP_EOL;
					}
					?>

				</tr>
				<tr>
					<th scope="row"><label for="it_name">상품명<strong class="sound_only">필수</strong></label></th>
					<td><input type="text" name="it_name" value="<?php echo $it['it_name'] ?>" id="it_name" required class="frm_input required" size='50'  maxlength="100">	</td>
				</tr>
				<tr>
					<th scope="row"><label for="it_use">사용여부</label></th>
					<td>
						<label><input type="radio" name="it_use" value="1"<?php echo get_checked('1', $it['it_use'])?>>사용</label>
						<label><input type="radio" name="it_use" value="0"<?php echo get_checked('0', $it['it_use'])?>>미사용</label>
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="it_use">추천</label></th>
					<td>
						<label><input type="checkbox" name="it_main" value="1"<?php echo get_checked('1', $it['it_main'])?>>메인추천</label>
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="it_tag">태그<strong class="sound_only">필수</strong></label></th>
					<td>
						<input type="text" name="it_tag" id="it_tag"  class="frm_input " size='20'  maxlength="20"><button type="button" id="add-tag" class="btn_frmline">추가</button>
						<div class="tag-line">
						<?php for($i=0;$i<count($tags);$i++){ ?>
						<span class="list-tag">
						<input type="hidden" name="tag[]" value="<?php echo $tags[$i]?>">
						<?php echo $tags[$i]?>
						<button class="del-tag" type="button">X</button>
						</span>
						<?php } ?>
						</div>
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="it_content">상세<strong class="sound_only">필수</strong></label></th>
					<td>
						<?php echo editor_html('it_content', get_text(html_purifier($it['it_content']), 0)); ?>
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="it_thumb">썸네일<br>(590x492)</label></th>
					<td>
						<input type="file" name="it_thumb">
						<?php if(is_file(G5_DATA_ITEM_PATH."/{$it['it_thumb']}")){ ?>
						<img src="<?php echo G5_DATA_ITEM_URL."/{$it['it_thumb']}" ?>" style="display:block;width:200px;">
						<label><input type="checkbox" name="del_thumb" value="1"> 썸네일 삭제</label>
						<?php } ?>
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="it_thumb2">썸네일(gif)<br>(590x492)</label></th>
					<td>
						<input type="file" name="it_thumb2">
						<?php if(is_file(G5_DATA_ITEM_PATH."/{$it['it_thumb2']}")){ ?>
							<img src="<?php echo G5_DATA_ITEM_URL."/{$it['it_thumb2']}" ?>" style="display:block;width:200px;">
							<label><input type="checkbox" name="del_thumb2" value="1"> 썸네일(gif) 삭제</label>
						<?} ?>
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="it_file">다운로드 파일</label></th>
					<td>
						<input type="file" name="it_file">
						<?php

						if(is_file(G5_DOWNLOAD_PATH."/{$it['it_file']}"))
							echo '<div><a href="'.G5_ITEM_URL.'/download.php?it_no='.$it['it_no'].'" style="display:inline-block;margin-top:5px;border:1px solid #333;padding:5px 10px;">'.$it['it_filename'].' 다운로드</a></div>';
						?>
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="it_file">관련 상품</label></th>
					<td>
						<div id="relation-list">
						<?php
						$sql = "
							select a.it_id, a.it_name, a.it_thumb
							from {$g5['item_table']} a, {$g5['item_related_table']} b
							where a.it_id = b.it_id2
							and b.it_id1 = '{$it_id}'
							order by ir_no asc
						";
						$result = sql_query($sql, true);
						while($row=sql_fetch_array($result)){

							$html = '<p class="relation-item it'.$row['it_id'].'" style="padding-bottom:10px;line-height:40px;">';
							$html .= '<input type="hidden" name="relation_it_id[]" value="'.$row['it_id'].'"> ';
							$html .= '<img src="'.G5_DATA_ITEM_URL.'/'.$row['it_thumb'].'" style="height:40px;"> '.$row['it_name'];
							$html .= ' <span class="relation-del" style="font-size:10px;color:#ccc;cursor:pointer">삭제</span>';
							$html .= '</p>';
							echo $html;
						}
						?>
						</div>
						<input type="text" name="search" id="search" class="frm_input" placeholder="상품명 혹은 태그" ><button type="button" class="btn_frmline add-relation" data-remodal-target="modal_item">검색</button>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="btn_fixed_top">
		<a href="./itemlist.php?<?php echo $qstr ?>" class="btn btn_02">목록</a>
		<input type="submit" value="확인" class="btn_submit btn" accesskey='s'>
	</div>
</form>

<div class="item remodal" data-remodal-id="modal_item" data-remodal-options="hashTracking: false">
    <button type="button" class="connect-close" data-remodal-action="close">
        <i class="fa fa-close"></i>
        <span class="txt">닫기</span>
    </button>

	<div id="item-list" style="text-align:left;max-height:300px;overflow:scroll">
	</div>
	<button type="button" id="add-item" class="btn btn_03 ">추가</button>
</div>


<script>
function fitem_submit(f)
{
	<?php echo get_editor_js('it_content'); ?>
	check_field(f.it_content, "상세를 입력하세요.");
	return true;
}
$(function(){
	$(".add-relation").click(function(){
		$search = $("#search").val();
		if(!$search){
			alert('검색어를 입력해주세요.');
			$("#search").select();
			return false;
		}
		$.get("./ajax.item.list.php?it_id=<?php echo $it_id; ?>&search="+$search, function($list){
			$("#item-list").html($list);
		});
	});

	$("#add-item").click(function(){
		$("#item-list input:checked").each(function(){
			$this = $(this).parents("label.chk");
			$id 	= $this.attr("data-id");
			$name 	= $this.attr("data-name");
			$image 	= $this.attr("data-image");
			$(".relation-item.it"+$id).remove();
			$html = '<p class="relation-item it'+$id+'" style="padding-bottom:10px;line-height:40px;">';
			$html += '<input type="hidden" name="relation_it_id[]" value="'+$id+'"> ';
			$html += '<img src="'+$image+'" style="height:40px;"> ';
			$html += $name;
			$html += ' <span class="relation-del" style="font-size:10px;color:#ccc;cursor:pointer">삭제</span>';
			$html += '</p>';
			$("#relation-list").append($html);
		});
	})

	$("#relation-list").on("click", ".relation-del",function(){
		if(!confirm('정말로 삭제하시겠습니까?\n삭제 후 저장하셔야 정상처리됩니다.')) return false;
		$(this).parents(".relation-item").remove();
	})
})
</script>
<?php
include_once(G5_ADMIN_PATH.'/admin.tail.php');
?>
