<?php
$sub_menu = "400400";
include_once('./_common.php');

if(!$fld) $fld = 'od_id';

auth_check($auth[$sub_menu], 'r');

$sql_common = " from {$g5['order_table']}  ";
$sql_search = " where od_expiry_date >= '".G5_TIME_YMD."' ";


if($stx){
  	switch ($sfl) {
		case "mb_name" :
		case "mb_email" :
			$sql_search .= " and mb_id in ( select mb_id from {$g5['member_table']} where {$sfl} like '%{$stx}%' ) ";
			break;
		default:
			$sql_search .= " and {$sfl} like '%{$stx}%' ";
			break;
	}
}

if($status)
	$sql_search .= " and od_status = '{$status}' ";

if($payment)
	$sql_search .= " and od_payment  = '{$payment}' ";

if($coupon)
	$sql_search .= " and cm_no > 0 ";


if ($fr_date && $to_date) {
    $sql_search .= " and od_datetime between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
}


if (!$sst) {
    $sst  = "od_no";
    $sod = "desc";
}

$sql_order .= " order by $sst $sod ";

$sql = " select count(*) as cnt
            {$sql_common}
            {$sql_search}
            {$sql_order} ";

$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$sql = " select *
            {$sql_common}
            {$sql_search}
            {$sql_order}
            limit {$from_record}, {$rows} ";

$result = sql_query($sql, true);

$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall btn_ov02">전체목록</a>';

$g5['title'] = '라이선스 주문 관리';
include_once(G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');
?>

<div class="local_ov01 local_ov">
    <?php echo $listall ?>
    <span class="btn_ov01"><span class="ov_txt"> 생성된 주문 수 </span><span class="ov_num"><?php echo number_format($total_count) ?>개</span></span>
</div>

<form id="fsearch" name="fsearch" class="local_sch01 local_sch" method="get" autocomplete="off">
<label for="sfl" class="sound_only">검색대상</label>
<select name="sfl" id="sfl">
    <option value="od_id"<?php echo get_selected($sfl, 'od_id')?>>주문번호</option>
    <option value="mb_name"<?php echo get_selected($sfl, 'mb_name')?>>회원명</option>
    <option value="mb_email"<?php echo get_selected($sfl, 'mb_email')?>>이메일</option>
    <option value="od_title"<?php echo get_selected($sfl, 'od_title')?>>이용권명</option>
</select>
<label for="stx" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
<input type="text" name="stx" value="<?php echo $stx; ?>" id="stx" required="" class="required frm_input">
<input type="submit" class="btn_submit" value="검색">
</form>

<form class="local_sch03 local_sch" autocomplete="off">
	<div>
	    <strong>주문상태</strong>
	    <input type="radio" name="status" value="" id="status_all"<?php echo get_checked($status, "")?>>
	    <label for="status_all">전체</label>
		<?php foreach($g5['order_status_array'] as $k=>$v){ ?>
	    <input type="radio" name="status" value="<?php echo $k; ?>" id="status_<?php echo $k; ?>" <?php echo get_checked("{$status}", "{$k}"); ?>>
	    <label for="status_<?php echo $k; ?>"><?php echo $v; ?></label>
		<?php } ?>
	</div>

	<div>
	    <strong>결제수단</strong>
	    <input type="radio" name="payment" value="" id="payment"<?php echo get_checked($payment, "")?>>
	    <label for="payment">전체</label>
		<?php foreach($g5['payment_array'] as $k=>$v){ ?>
	    <input type="radio" name="payment" value="<?php echo $v ?>" id="payment0<?php echo $k ?>"<?php echo get_checked("{$payment}", "{$v}")?>>
	    <label for="payment0<?php echo $k ?>"><?php echo $v ?></label>
		<?php }
		$k++;
		?>
	    <input type="radio" name="payment" value="무료이용권" id="payment0<?php echo $k; ?>" <?php echo get_checked("{$payment}", "무료이용권")?>>
	    <label for="payment0<?php echo $k; ?>">무료이용권</label>
	</div>

	<div>
	    <strong>기타선택</strong>
	    <input type="checkbox" name="coupon" value="Y" id="coupon"<?php echo get_checked('Y', $coupon); ?>>
	    <label for="od_coupon">쿠폰</label>
	   </div>

	<div class="sch_last">
	    <strong>주문일자</strong>
		<input type="text" id="fr_date"  name="fr_date" value="<?php echo $fr_date; ?>" class="frm_input" size="10" maxlength="10"> ~
	    <input type="text" id="to_date"  name="to_date" value="<?php echo $to_date; ?>" class="frm_input" size="10" maxlength="10">
	    <button type="button" onclick="javascript:set_date('오늘');">오늘</button>
	    <button type="button" onclick="javascript:set_date('어제');">어제</button>
	    <button type="button" onclick="javascript:set_date('이번주');">이번주</button>
	    <button type="button" onclick="javascript:set_date('이번달');">이번달</button>
	    <button type="button" onclick="javascript:set_date('지난주');">지난주</button>
	    <button type="button" onclick="javascript:set_date('지난달');">지난달</button>
	    <button type="button" onclick="javascript:set_date('전체');">전체</button>
	    <input type="submit" value="검색" class="btn_submit">
	</div>
</form>

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col"><?php echo subject_sort_link('od_id') ?>주문번호</a></th>
	    <th scope="col">회원명</a></th>
        <th scope="col">이메일</a></th>
        <th scope="col"><?php echo subject_sort_link('od_title') ?>이용권명</a></th>
		<th scope="col">이용권금액</th>
		<th scope="col">쿠폰사용</th>
		<th scope="col">결제방식</th>
		<th scope="col">결제금액</th>
		<th scope="col"><?php echo subject_sort_link('od_expiry_date') ?>만료일</a></th>
        <th scope="col"><?php echo subject_sort_link('od_paytime') ?>결제일</a></th>
        <th scope="col"><?php echo subject_sort_link('od_download') ?>다운로드</a></th>
        <th scope="col">취소</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $count = 0;
	$t_date = date('Y-m-d',time());
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {

        $bg = 'bg'.($i%2);
		$mb = get_member($row['mb_id']);


		$expiry_date = $row['od_expiry_date'];
		$dday = intval( (strtotime($expiry_date) - strtotime($t_date)) / 86400);

    ?>
    <tr class="<?php echo $bg; ?>">
	    <td><?php echo $row['od_id']?></td>
	    <td><?php echo $mb['mb_name']?></td>
		<td><?php echo $mb['mb_email']?></td>
		<td><?php echo $row['od_title']?></td>
		<td><?php echo number_format($row['od_price']); ?>원</td>
		<td><?php echo number_format($row['od_coupon']); ?>원</td>
		<td><?php echo $row['od_payment']?></td>
		<td><?php echo number_format($row['od_amount']); ?>원</td>
		<td><?php echo $row['od_expiry_date']; ?><p style="color:#f00"><?php echo "{$dday}일 남음"?></p></td>
		<td><?php echo $row['od_paytime']; ?></td>
		<td><?php echo $row['od_download']; ?>건</td>
		<td class="td_mng td_mng_m">
		<?php
		if($row['od_status'] == '9'){
			echo '주문취소';
		}else{
			echo '<select name="od_status" class="od_status" data-no="'.$row['od_no'].'">';
			foreach($g5['order_status_array']as $k=>$v)
				echo '<option value="'.$k.'"'.get_selected("{$k}" , "{$row['od_status']}").'>'.$v.'</option>';
			echo '</select>';
		}

		?>
		</td>
    </tr>
    <?php
        $count++;
    }

    if ($count == 0)
        echo '<tr><td colspan="11" class="empty_table">자료가 없습니다.</td></tr>';
    ?>
    </tbody>
    </table>
</div>

<?php
$pagelist = get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, $_SERVER['SCRIPT_NAME'].'?'.$qstr.'&amp;page=');
echo $pagelist;
?>



<script>
$(function(){
    $("#fr_date, #to_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99", maxDate: "+0d" });

	$(".od_status").change(function(){

		$data = {
			'od_no':$(this).attr('data-no')
			, 'od_status':$(this).val()
		};

		$.post("./orderupdate.php", $data, function($msg){
			alert($msg);
		}).fail(function() {
			alert( "상태값 변경에 실패하였습니다.\n잠시 후 다시 이용해주세요." );
		});
	});
});

function set_date(today)
{

    <?php
    $date_term = date('w', G5_SERVER_TIME);
    $week_term = $date_term + 7;
    $last_term = strtotime(date('Y-m-01', G5_SERVER_TIME));
    ?>
    if (today == "오늘") {
        document.getElementById("fr_date").value = "<?php echo G5_TIME_YMD; ?>";
        document.getElementById("to_date").value = "<?php echo G5_TIME_YMD; ?>";
    } else if (today == "어제") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME - 86400); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME - 86400); ?>";
    } else if (today == "이번주") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-'.$date_term.' days', G5_SERVER_TIME)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "이번달") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-01', G5_SERVER_TIME); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "지난주") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-'.$week_term.' days', G5_SERVER_TIME)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', strtotime('-'.($week_term - 6).' days', G5_SERVER_TIME)); ?>";
    } else if (today == "지난달") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-01', strtotime('-1 Month', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-t', strtotime('-1 Month', $last_term)); ?>";
    } else if (today == "전체") {
        document.getElementById("fr_date").value = "";
        document.getElementById("to_date").value = "";
    }
}
function fitemlist_submit(f)
{
    if (!is_checked("chk[]")) {
        alert(document.pressed+" 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

    if(document.pressed == "선택삭제") {
        if(!confirm("선택한 자료를 정말 삭제하시겠습니까?")) {
            return false;
        }
    }

    return true;
}
</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>
