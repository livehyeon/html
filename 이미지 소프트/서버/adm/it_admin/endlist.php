<?php
$sub_menu = "400500";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$sql_common = " from {$g5['order_table']}  ";
$sql_search = " where od_status = '2' ";

if($stx){
  	switch ($sfl) {
		case "mb_name" :
		case "mb_email" :
			$sql_search .= " and mb_id in ( select mb_id from {$g5['member_table']} where {$sfl} like '%{$stx}%' ) ";
			break;
		default:
			$sql_search .= " and {$sfl} like '%{$stx}%' ";
			break;
	}
}

if ($fr_date && $to_date) {
    $sql_search .= " and od_datetime between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
}else{
	$sql_search .= " and od_expiry_date between '".G5_TIME_YMD."' and '".date("Y-m-d", strtotime("+30days") )."' ";
}

if (!$sst) {
    $sst  = "od_expiry_date";
    $sod = "desc";
}
$sql_order = " order by $sst $sod ";

$sql = " select count(*) as cnt
            {$sql_common}
            {$sql_search}
            {$sql_order} ";

//echo $sql;
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$sql = " select *
            {$sql_common}
            {$sql_search}
            {$sql_order}
            limit {$from_record}, {$rows} ";

$result = sql_query($sql, true);

$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall btn_ov02">전체목록</a>';

$g5['title'] = '라이선스 만료 관리';
include_once(G5_ADMIN_PATH.'/admin.head.php');

$colspan = 10;
?>

<div class="local_ov01 local_ov">
    <?php echo $listall ?>
    <span class="btn_ov01"><span class="ov_txt"> 주문 수 </span><span class="ov_num"><?php echo number_format($total_count) ?>개</span></span>
</div>

<form id="fsearch" name="fsearch" class="local_sch01 local_sch" method="get" autocomplete="off">
<label for="sfl" class="sound_only">검색대상</label>
<select name="sfl" id="sfl">
    <option value="od_id"<?php echo get_selected($sfl, 'od_id')?>>주문번호</option>
    <option value="mb_name"<?php echo get_selected($sfl, 'mb_name')?>>회원명</option>
    <option value="mb_email"<?php echo get_selected($sfl, 'mb_email')?>>이메일</option>
</select>
<label for="stx" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
<input type="text" name="stx" value="<?php echo $stx; ?>" id="stx" required="" class="required frm_input">
<input type="submit" class="btn_submit" value="검색">
</form>


<form class="local_sch03 local_sch" autocomplete="off">
	<input type="hidden" name="sod" value="desc">
	<div>
	    <strong>정렬</strong>
		<input type="radio" name="sst" value="od_expiry_date" id="sst_expiry"<?php echo get_checked($sst, "od_expiry_date")?>>
	    <label>만료 순</label>
		<input type="radio" name="sst" value="od_paytime" id="sst_paytime"<?php echo get_checked($sst, "od_paytime")?>>
	    <label>결제 순</label>

	</div>


	<div class="sch_last">
	    <strong>만료일자</strong>
		<input type="text" id="fr_date"  name="fr_date" value="<?php echo $fr_date; ?>" class="frm_input" size="10" maxlength="10"> ~
	    <input type="text" id="to_date"  name="to_date" value="<?php echo $to_date; ?>" class="frm_input" size="10" maxlength="10">
	    <button type="button" onclick="javascript:set_date('오늘');">오늘</button>
	    <button type="button" onclick="javascript:set_date('내일');">내일</button>
	    <button type="button" onclick="javascript:set_date('이번주');">이번주</button>
	    <button type="button" onclick="javascript:set_date('이번달');">이번달</button>
	    <button type="button" onclick="javascript:set_date('다음주');">다음주</button>
	    <button type="button" onclick="javascript:set_date('다음달');">다음달</button>
	    <button type="button" onclick="javascript:set_date('전체');">전체</button>
	    <input type="submit" value="검색" class="btn_submit">
	</div>
</form>


<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col">주문번호</th>
	    <th scope="col">회원명</th>
        <th scope="col">이메일</th>
        <th scope="col">이용권명</th>
		<th scope="col">만료일</th>
        <th scope="col">결제일</th>
        <th scope="col">알림</th>
    </tr>
    </tr>
    </thead>
    <tbody>
    <?php
    $count = 0;
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {

        $bg = 'bg'.($i%2);
		$mb = get_member($row['mb_id']);
		$send_email = '<a href="./ordersendemail.php?od_id='.$row['od_id'].'" class="btn btn_03">이메일</a>';
		$send_sms = '<a href="./ordersendsms.php?od_id='.$row['od_id'].'" class="btn btn_03">SMS</a>';

		$expiry_date = $row['od_expiry_date'];
		$dday = intval( (strtotime($expiry_date) - strtotime($t_date)) / 86400);
    ?>
    <tr class="<?php echo $bg; ?>">
	    <td><?php echo $row['od_id']?></td>
	    <td><?php echo $mb['mb_name']?></td>
		<td><?php echo $mb['mb_email']?></td>
		<td><?php echo $row['od_title']?></td>
		<td><?php echo $row['od_expiry_date']; ?><p style="color:#f00"><?php echo "{$dday}일 남음"?></p></td>
		<td><?php echo $row['od_paytime']; ?></td>
		<td class="td_mng td_mng_l">
		<?php echo $send_email.$send_sms;?>
		</td>
    </tr>
    <?php
        $count++;
    }
    if ($count == 0)
        echo '<tr><td colspan="'.$colspan.'" class="empty_table">자료가 없습니다.</td></tr>';
    ?>
    </tbody>
    </table>
</div>




<?php
$pagelist = get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, $_SERVER['SCRIPT_NAME'].'?'.$qstr.'&amp;page=');
echo $pagelist;
?>



<script>


function set_date(today)
{
    <?php
    $date_term = date('w', G5_SERVER_TIME);
    $week_term = $date_term + 7;
    $last_term = strtotime(date('Y-m-01', G5_SERVER_TIME));

    ?>
    if (today == "오늘") {
        document.getElementById("fr_date").value = "<?php echo G5_TIME_YMD; ?>";
        document.getElementById("to_date").value = "<?php echo G5_TIME_YMD; ?>";
    } else if (today == "내일") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME + 86400); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME + 86400); ?>";
    } else if (today == "이번주") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-'.$date_term.' days', G5_SERVER_TIME)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', strtotime('+'.(6-$date_term).' days', G5_SERVER_TIME)); ?>";
    } else if (today == "이번달") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-01', G5_SERVER_TIME); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-t', G5_SERVER_TIME); ?>";
    } else if (today == "다음주") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('+'.($week_term-6).' days', G5_SERVER_TIME)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', strtotime('+'.($week_term).' days', G5_SERVER_TIME)); ?>";
    } else if (today == "다음달") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-01', strtotime('+1 Month', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-t', strtotime('+1 Month', $last_term)); ?>";
    } else if (today == "전체") {
        document.getElementById("fr_date").value = "";
        document.getElementById("to_date").value = "";
    }
}

function fitemlist_submit(f)
{
    if (!is_checked("chk[]")) {
        alert(document.pressed+" 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

    if(document.pressed == "선택삭제") {
        if(!confirm("선택한 자료를 정말 삭제하시겠습니까?")) {
            return false;
        }
    }

    return true;
}
</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>
