<?php
$sub_menu = "400100";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'w');

if(in_array($w, array('', 'u'))){

	if(!$ca_id || !$ca_name)
		alert('필수 입력값이 입력되지 않았습니다.');
}

$ca = sql_fetch(" select * from {$g5['category_table']} where ca_id='{$ca_id}' ");

if($ca_use!='1') $ca_use = '';
if($w==''){
	if(isset($ca['ca_id']))
		alert("분류코드가 존재합니다.");

	$sql = "
		insert into {$g5['category_table']} set
			ca_id = '{$ca_id}'
			, ca_name = '{$ca_name}'
			, ca_order = '{$ca_order}'
			, ca_use = '{$ca_use}'
	";
	sql_query($sql, true);
	$txt = '등록';

}elseif($w=='u'){
	if(!isset($ca['ca_id']))
		alert("분류코드가 존재하지 않습니다.");

	$sql = "
		update {$g5['category_table']} set
			ca_name = '{$ca_name}'
			, ca_order = '{$ca_order}'
			, ca_use = '{$ca_use}'
		where ca_id = '{$ca_id}'
	";
	sql_query($sql, true);
	$txt = '수정';
}elseif($w=='d'){

    for ($i=0; $i<count($_POST['chk']); $i++)
    {
        // 실제 번호를 넘김
        $k = $_POST['chk'][$i];
 		$ca_id = $_POST['ca_id'][$k];
		sql_query(" delete from {$g5['category_table']} where ca_id = '{$ca_id}' ");
    }
 	$txt = '삭제';
}

alert("{$txt}되었습니다.", "./categorylist.php?{$qstr}");

?>
