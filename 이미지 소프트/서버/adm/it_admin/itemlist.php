<?php
$sub_menu = "400200";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$sql_common = " from {$g5['item_table']}  ";
$sql_search = " where (1) ";
if (!$sst) {
    $sst  = "it_id";
    $sod = "asc";
}
$sql_order = " order by $sst $sod ";

$sql = " select count(*) as cnt
            {$sql_common}
            {$sql_search}
            {$sql_order} ";
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$sql = " select *
            {$sql_common}
            {$sql_search}
            {$sql_order}
            limit {$from_record}, {$rows} ";

$result = sql_query($sql, true);

$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall btn_ov02">전체목록</a>';

$g5['title'] = '상품관리';
include_once(G5_ADMIN_PATH.'/admin.head.php');
$colspan = 5;
?>

<div class="local_ov01 local_ov">
    <?php echo $listall ?>
    <span class="btn_ov01"><span class="ov_txt"> 생성된 상품 수 </span><span class="ov_num"><?php echo number_format($total_count) ?>개</span></span>
</div>


<form name="fitemlist" id="fitemlist" method="post" action="./itemupdate.php" onsubmit="return fitemlist_submit(this);">
<input type="hidden" name="sst" value="<?php echo $sst ?>">
<input type="hidden" name="sod" value="<?php echo $sod ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
<input type="hidden" name="stx" value="<?php echo $stx ?>">
<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="w" value="d">
<input type="hidden" name="token" value="">

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col">
            <label for="chkall" class="sound_only">현재 페이지 상품 전체</label>
            <input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)">
        </th>
        <th scope="col"><?php echo subject_sort_link('it_id') ?>상품코드</a></th>
        <th scope="col">썸네일</th>
        <th scope="col"><?php echo subject_sort_link('it_name') ?>상품명</a></th>
        <th scope="col"><?php echo subject_sort_link('it_hot') ?>인기도</a></th>
        <th scope="col"><?php echo subject_sort_link('it_main') ?>메인추천</a></th>
        <th scope="col">관리</th>

    </tr>
    </thead>
    <tbody>
    <?php
    $count = 0;
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {
		$one_update = '<a href="./itemform.php?w=u&it_id='.$row['it_id'].'" class="btn btn_03">수정</a>';
        $bg = 'bg'.($i%2);
    ?>
    <tr class="<?php echo $bg; ?>">
        <td class="td_chk">
            <input type="hidden" name="it_id[<?php echo $i ?>]" value="<?php echo $row['it_id'] ?>">
            <input type="checkbox" name="chk[]" value="<?php echo $i ?>" id="chk_<?php echo $i ?>">
        </td>
        <td><?php echo $row['it_id']?></td>
		<td><?php
		if($row['it_thumb'] && is_file(G5_DATA_ITEM_PATH."/{$row['it_thumb']}"))
			echo '<a href="'.G5_ITEM_URL.'/view.php?it_id='.$row['it_id'].'" target="_blank"><img src="'.G5_DATA_ITEM_URL."/{$row['it_thumb']}".'" style="width:60px;"></a>';
		?></td>
		<td class="td_left"><a href="<?PHP echo G5_ITEM_URL; ?>/view.php?it_id=<?php echo $row['it_id']; ?>" target="_blank"><?php echo $row['it_name']?></a></td>
		<td><?php echo $row['it_hot']?></td>
		<td><?php echo $row['it_main']=='1'?'추천':'';?></td>
        <td class="td_mng td_mng_m">
            <?php echo $one_update ?>
        </td>
    </tr>
    <?php
        $count++;
    }

    if ($count == 0)
        echo '<tr><td colspan="'.$colspan.'" class="empty_table">자료가 없습니다.</td></tr>';
    ?>
    </tbody>
    </table>
</div>



<div class="btn_fixed_top">
    <input type="submit" name="act_button" value="선택삭제" onclick="document.pressed=this.value" class="btn_02 btn">
    <a href="./itemform.php" id="item_add" class="btn_01 btn btn-add">상품 추가</a>
</div>

<?php
//if (isset($stx))
//    echo '<script>document.fsearch.sfl.value = "'.$sfl.'";</script>'."\n";

if (strstr($sfl, 'mb_id'))
    $mb_id = $stx;
else
    $mb_id = '';
?>
</form>

<?php
$pagelist = get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, $_SERVER['SCRIPT_NAME'].'?'.$qstr.'&amp;page=');
echo $pagelist;
?>



<script>


function fitemlist_submit(f)
{
    if (!is_checked("chk[]")) {
        alert(document.pressed+" 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

    if(document.pressed == "선택삭제") {
        if(!confirm("선택한 자료를 정말 삭제하시겠습니까?")) {
            return false;
        }
    }

    return true;
}
</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>
