<?php
$sub_menu = "400100";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$sql_common = " from {$g5['category_table']}  ";

$sql_search = " where (1) ";


if (!$sst) {
    $sst  = "ca_id";
    $sod = "asc";
}
$sql_order = " order by $sst $sod ";

$sql = " select count(*) as cnt
            {$sql_common}
            {$sql_search}
            {$sql_order} ";
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$sql = " select *
            {$sql_common}
            {$sql_search}
            {$sql_order}
            limit {$from_record}, {$rows} ";

$result = sql_query($sql, true);

$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall btn_ov02">전체목록</a>';

$g5['title'] = "분류관리";
include_once(G5_ADMIN_PATH.'/admin.head.php');


add_stylesheet('<link rel="stylesheet" href="'.G5_JS_URL.'/remodal/remodal.css">', 11);
add_stylesheet('<link rel="stylesheet" href="'.G5_JS_URL.'/remodal/remodal-default-theme.css">', 12);
add_javascript('<script src="'.G5_JS_URL.'/remodal/remodal.js"></script>', 10);
$colspan = 5;
?>

<div class="local_ov01 local_ov">
    <?php echo $listall ?>
    <span class="btn_ov01"><span class="ov_txt"> 생성된 분류 수 </span><span class="ov_num"><?php echo number_format($total_count) ?>개</span></span>
</div>


<form name="fcategorylist" id="fcategorylist" method="post" action="./categoryupdate.php" onsubmit="return fcategorylist_submit(this);">
<input type="hidden" name="sst" value="<?php echo $sst ?>">
<input type="hidden" name="sod" value="<?php echo $sod ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
<input type="hidden" name="stx" value="<?php echo $stx ?>">
<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="w" value="d">
<input type="hidden" name="token" value="">

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col">
            <label for="chkall" class="sound_only">현재 페이지 카테고리 전체</label>
            <input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)">
        </th>
        <th scope="col"><?php echo subject_sort_link('ca_id') ?>분류코드</a></th>
        <th scope="col"><?php echo subject_sort_link('ca_name') ?>분류명</a></th>
        <th scope="col"><?php echo subject_sort_link('ca_order') ?>우선순위</a></th>
        <th scope="col"><?php echo subject_sort_link('ca_use') ?>사용</a></th>
        <th scope="col">수정</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $count = 0;
	$unused = array();
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {
		 
        $bg = 'bg'.($i%2);
    ?>
    <tr class="<?php echo $bg; ?>">
        <td class="td_chk">
            <input type="hidden" name="ca_id[<?php echo $i ?>]" value="<?php echo $row['ca_id'] ?>">
            <input type="checkbox" name="chk[]" value="<?php echo $i ?>" id="chk_<?php echo $i ?>">
        </td>
        <td><?php echo $row['ca_id']?> </td>
		<td><?php echo $row['ca_name']?> </td>
		<td><?php echo $row['ca_order']?> </td>
		<td><?php echo $row['ca_use']=='1'?'사용':'미사용'?> </td>
		<td class="td_mng td_mng_m">
			<a href="./categoryform.php?w=u&ca_id=<?php echo $row['ca_id']; ?>&<?php echo $qstr; ?>" data-remodal-target="modal_form" class="btn btn_03 btn-add">수정</a>
			<?php if(strlen($row['ca_id'])=='2'){ ?><a href="./categoryform.php?ca_id=<?php echo $row['ca_id']; ?>" data-remodal-target="modal_form" class="btn btn_01 btn-add">추가</a><?php } ?>
		</td>
    </tr>
    <?php
        $count++;
    }

    if ($count == 0)
        echo '<tr><td colspan="'.$colspan.'" class="empty_table">자료가 없습니다.</td></tr>';
    ?>
    </tbody>
    </table>
</div>



<div class="btn_fixed_top">
    <input type="submit" name="act_button" value="선택삭제" onclick="document.pressed=this.value" class="btn_02 btn">
    <a href="./categoryform.php" id="category_add" data-remodal-target="modal_form" class="btn_01 btn btn-add">분류 추가</a>
</div>

<?php
//if (isset($stx))
//    echo '<script>document.fsearch.sfl.value = "'.$sfl.'";</script>'."\n";

if (strstr($sfl, 'mb_id'))
    $mb_id = $stx;
else
    $mb_id = '';
?>
</form>

<?php
$pagelist = get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, $_SERVER['SCRIPT_NAME'].'?'.$qstr.'&amp;page=');
echo $pagelist;
?>

<div class="form remodal" data-remodal-id="modal_form" data-remodal-options="hashTracking: false">
    <button type="button" class="connect-close" data-remodal-action="close">
        <i class="fa fa-close"></i>
        <span class="txt">닫기</span>
    </button>
	<div id="categoryform">
	</div>
</div>



<script>

$(function(){
	$(".btn-add").click(function(){
		$href = $(this).attr('href');
		$.get($href, function(html){
			$("#categoryform").html(html);
		});

	})
})
function fcategory_add_submit(f){

    <?php echo $captcha_js; // 캡챠 사용시 자바스크립트에서 입력된 캡챠를 검사함  ?>

    return true;
}

function fcategorylist_submit(f)
{
    if (!is_checked("chk[]")) {
        alert(document.pressed+" 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

    if(document.pressed == "선택삭제") {
        if(!confirm("선택한 자료를 정말 삭제하시겠습니까?")) {
            return false;
        }
    }

    return true;
}
</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>
