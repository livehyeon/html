<?php
$sub_menu = '400100';
include_once('./_common.php');
auth_check($auth[$sub_menu], "w", true);

if ($w == "")
{

    $len = strlen($ca_id);
    if ($len == 4)
        alert("분류를 더 이상 추가할 수 없습니다.\\n\\2단계 분류까지만 가능합니다.");

    $len2 = $len + 1;

    $sql = " select MAX(SUBSTRING(ca_id,$len2,2)) as max_subid from {$g5['category_table']}
              where SUBSTRING(ca_id,1,$len) = '$ca_id' ";
    $row = sql_fetch($sql);

    $subid = base_convert($row['max_subid'], 36, 10);
    $subid += 36;
    if ($subid >= 36 * 36)
    {
        //alert("분류를 더 이상 추가할 수 없습니다.");
        // 빈상태로
        $subid = "  ";
    }
    $subid = base_convert($subid, 10, 36);
    $subid = substr("00" . $subid, -2);
    $subid = $ca_id . $subid;

    $sublen = strlen($subid);

    if ($ca_id) // 2단계이상 분류
    {
        $sql = " select * from {$g5['category_table']} where ca_id = '$ca_id' ";
        $ca = sql_fetch($sql);
        $html_title = $ca['ca_name'] . " 하위분류추가";
        $ca['ca_name'] = "";
    }
    else // 1단계 분류
    {
		 $html_title = "1단계분류추가";
		 $ca['ca_use'] = '1';
    }
}
else if ($w == "u")
{
    $sql = " select * from {$g5['category_table']} where ca_id = '$ca_id' ";
    $ca = sql_fetch($sql);
    if (!$ca['ca_id'])
        alert("자료가 없습니다.");

    $html_title = $ca['ca_name'] . " 수정";
    $ca['ca_name'] = get_text($ca['ca_name']);
}


?>


<form id="frm" name="frm" action="./categoryupdate.php" method="post">
<input type="hidden" name="sst" value="<?php echo $sst ?>">
<input type="hidden" name="sod" value="<?php echo $sod ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
<input type="hidden" name="stx" value="<?php echo $stx ?>">
<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="w" value="<?php echo $w;?>">
<input type="hidden" name="token" value="">
<div class="tbl_frm01 tbl_wrap">
	<table>
		<tbody>
			<tr>
				<th>분류코드</th>
				<td>
				<?php if ($w == "") { ?>
                <?php echo help("자동으로 보여지는 분류코드를 사용하시길 권해드리지만 직접 입력한 값으로도 사용할 수 있습니다.\n분류코드는 나중에 수정이 되지 않으므로 신중하게 결정하여 사용하십시오.\n\n분류코드는 2자리씩 10자리를 사용하여 5단계를 표현할 수 있습니다.\n0~z까지 입력이 가능하며 한 분류당 최대 1296가지를 표현할 수 있습니다.\n그러므로 총 3656158440062976가지의 분류를 사용할 수 있습니다."); ?>
                <input type="text" name="ca_id" value="<?php echo $subid; ?>" id="ca_id" required class="required frm_input" size="<?php echo $sublen; ?>" maxlength="<?php echo $sublen; ?>">

            	<?php } else { ?>
                <input type="hidden" name="ca_id" value="<?php echo $ca['ca_id']; ?>">
                <span class="frm_ca_id"><?php echo $ca['ca_id']; ?></span>
            	<?php } ?>
				</td>
			</tr>
			<tr>
				<th>분류명</th>
				<td><input type="text" name="ca_name" value="<?php echo $ca['ca_name']; ?>" class="frm_input" required style="width:50%;"> <label><input type="checkbox" name="it_use" value="1"<?php echo get_checked('1', $ca['ca_use'])?>> 사용</label></td>
			</tr>
			<tr>
				<th>노출순위</th>
				<td><input type="text" name="ca_order" value="<?php echo isset($ca['ca_order'])?$ca['ca_order']:'0'; ?>" class="frm_input" required ></td>
			</tr>
		</tbody>
	</table>
</div>
<div class="btn_confirm01 btn_confirm">
	<input type="submit" value="<?php echo $html_title; ?>" class="btn_submit btn">
</div>
</form>
