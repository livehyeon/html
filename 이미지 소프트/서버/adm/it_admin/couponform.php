<?php
$sub_menu = '400300';
include_once('./_common.php');
auth_check($auth[$sub_menu], "w", true);


$html_title = '쿠폰추가';

$cp = array(
	'cp_type'=>'1'
	, 'cp_benefit'=>15
	, 'cp_enddatetype'=>'days'
	, 'cp_enddays'=>'15'
	, 'cp_use'=>'1'
	, 'cp_offline'=>'0'
);


if ($w == "u" && $cp_no)
{
	$w = '';
	$cp = sql_fetch(" select * from {$g5['coupon_table']} where cp_no = '{$cp_no}' ", true);
	if(isset($cp['cp_no'])){
		$html_title = '쿠폰수정';
		$w = 'u';
	}

}
?>


<form id="frm" name="frm" action="./couponupdate.php" method="post">
<input type="hidden" name="sst" value="<?php echo $sst ?>">
<input type="hidden" name="sod" value="<?php echo $sod ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
<input type="hidden" name="stx" value="<?php echo $stx ?>">
<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="w" value="<?php echo $w;?>">
<input type="hidden" name="cp_no" value="<?php echo $cp['cp_no'];?>">
<input type="hidden" name="token" value="">
<div class="tbl_frm01 tbl_wrap">
	<table>
		<tbody>
			<tr>
				<th>쿠폰 종류</th>
				<td>
				<?php
				foreach($g5['coupon_type_array'] as $k=>$v)
					echo '<label><input type="radio" name="cp_type"'.get_checked($cp['cp_type'], $k).' value="'.$k.'" class="type"> '.$v.'</label>&nbsp;&nbsp;'.PHP_EOL;
				?>
				</td>
			</tr>
			<tr>
				<th>할인 혜택</th>
				<td>
					<input type="text" name="cp_benefit" id="cp_benefit" class="frm_input" value="<?php echo $cp['cp_benefit']?>" size="10"> <span id="txt_benefit">일</span>
				</td>
			</tr>
			<tr>
				<th>유효기간</th>
				<td>
					<label><input type="radio" name="cp_enddatetype" class="enddatetype"<?php echo get_checked('days', $cp['cp_enddatetype'])?> value="days"> 사용기간</label>
					<label><input type="radio" name="cp_enddatetype" class="enddatetype"<?php echo get_checked('date', $cp['cp_enddatetype'])?> value="date"> 발행기간</label>
					<p class="enddate days" style="display:none">발급일로부터 <input type="text" name="cp_enddays" value="<?php echo $cp['cp_enddays']?>" class="frm_input" size="10">일간 사용가능</p>
					<p class="enddate date" style="display:none"><input type="text" name="cp_enddate" value="<?php echo $cp['cp_enddate']?>" class="frm_input datepicker" size="10">일까지 사용가능</p>
				</td>
			</tr>
			<tr>
				<th>오프라인쿠폰</th>
				<td>
					<label><input type="radio" name="cp_offline" <?php echo get_checked('1', $cp['cp_offline'])?> value="1"> 오프라인쿠폰</label>
					<label><input type="radio" name="cp_offline" <?php echo get_checked('0', $cp['cp_offline'])?> value="0"> 온라인쿠폰</label>

				</td>
			</tr>
			<?php if($w=='u' && $cp['cp_offline_code']){ ?>
			<tr>
				<th>오프라인쿠폰 코드</th>
				<td><?php echo $cp['cp_offline_code']; ?></td>
			</tr>
			<?php } ?>
			<tr>
				<th>사용유무</th>
				<td>
					<label><input type="radio" name="cp_use" <?php echo get_checked('1', $cp['cp_use'])?> value="1"> 사용</label>
					<label><input type="radio" name="cp_use" <?php echo get_checked('0', $cp['cp_use'])?> value="0"> 미사용</label>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="btn_confirm01 btn_confirm">
	<input type="submit" value="<?php echo $html_title; ?>" class="btn_submit btn">
</div>
</form>
