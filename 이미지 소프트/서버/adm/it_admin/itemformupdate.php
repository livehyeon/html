<?php
$sub_menu = "400200";
include_once('./_common.php');

if ($w == 'u')
    check_demo();

auth_check_menu($auth, $sub_menu, 'w');

//check_admin_token();
$it_tag = implode("|", $_POST['tag']);

$colums = "
	it_name = '{$it_name}'
	, it_use = '{$it_use}'
	, it_tag = '{$it_tag}'
	, it_main = '{$it_main}'
	, it_content = '{$it_content}'
";
if($w==''){
	$it_id = "item".get_uniqid();
	$sql = "
		insert into {$g5['item_table']} set
			it_id = '{$it_id}'
			, {$colums}
	";
	sql_query($sql, true);
}elseif($w=='u'){

	$sql = "
		update {$g5['item_table']} set
			{$colums}
		where it_id = '{$it_id}'
	";
	sql_query($sql, true);
}else{
	alert('잘못된 요청입니다.');
}

$sql = " delete from {$g5['item_category_table']} where it_id = '{$it_id}' ";
sql_query($sql, true);

for($i=0;$i<count($_POST['category']);$i++){
	$category = $_POST['category'][$i];
	$sql = " insert into {$g5['item_category_table']} set it_id = '{$it_id}', category = '{$category}' ";
	sql_query($sql, ture);
}


$sql = " delete from {$g5['item_related_table']} where it_id1 = '{$it_id}' or it_id2 = '{$it_id}' ";
sql_query($sql);

for($i=0;$i<count($_POST['relation_it_id']);$i++){
	$related = $_POST['relation_it_id'][$i];
	$sql = " insert into {$g5['item_related_table']} set it_id1 = '{$it_id}', it_id2 = '{$related}' ";
	sql_query($sql, true);

	$sql = " insert into {$g5['item_related_table']} set it_id2 = '{$it_id}', it_id1 = '{$related}' ";
	sql_query($sql, true);
}


$it = sql_fetch(" select * from {$g5['item_table']} where it_id = '{$it_id}' ", true);
if($del_thumb || $del_thumb2){
	if($del_thumb){
		if($it['it_thumb'])
			@unlink(G5_DATA_ITEM_PATH.'/'.$it['it_thumb']);
		$it['it_thumb'] = '';
	}
	if($del_thumb2){
		if($it['it_thumb2'])
			@unlink(G5_DATA_ITEM_PATH.'/'.$it['it_thumb2']);
		$it['it_thumb2'] = '';
	}

	$sql = " update {$g5['item_table']} set it_thumb = '{$it['it_thumb']}', it_thumb2 = '{$it['it_thumb2']}' where it_id = '{$it_id}' ";
	sql_query($sql, true);
}
$chars_array = array_merge(range(0,9), range('a','z'), range('A','Z'));

if(isset($_FILES['it_thumb']['name']) && $_FILES['it_thumb']['name']) {

	@mkdir(G5_DATA_ITEM_PATH, G5_DIR_PERMISSION);
	@chmod(G5_DATA_ITEM_PATH, G5_DIR_PERMISSION);

	$tmp_file  = $_FILES['it_thumb']['tmp_name'];

	$filename  = $_FILES['it_thumb']['name'];
	$filename  = get_safe_filename($filename);

	shuffle($chars_array);
	$shuffle = implode('', $chars_array);

	$filename = preg_replace("/\.(php|pht|phtm|htm|cgi|pl|exe|jsp|asp|inc)/i", "$0-x", $filename);
	$it_thumb = abs(ip2long($_SERVER['REMOTE_ADDR'])).'_'.substr($shuffle,0,8).'_'.replace_filename($filename);

	$dest_file = G5_DATA_ITEM_PATH.'/'.$it_thumb;


	$error_code = move_uploaded_file($tmp_file, $dest_file) or die($_FILES['it_thumb']['error']);

	chmod($dest_file, G5_FILE_PERMISSION);

	if($it['it_thumb'])
		@unlink(G5_DATA_ITEM_PATH.'/'.$it['it_thumb']);

	sql_query(" update {$g5['item_table']} set it_thumb = '{$it_thumb}' where it_id = '{$it_id}' ", true);
}


if(isset($_FILES['it_thumb2']['name']) && $_FILES['it_thumb2']['name']) {

	@mkdir(G5_DATA_ITEM_PATH, G5_DIR_PERMISSION);
	@chmod(G5_DATA_ITEM_PATH, G5_DIR_PERMISSION);

	$tmp_file  = $_FILES['it_thumb2']['tmp_name'];

	$filename  = $_FILES['it_thumb2']['name'];
	$filename  = get_safe_filename($filename);

	shuffle($chars_array);
	$shuffle = implode('', $chars_array);

	$filename = preg_replace("/\.(php|pht|phtm|htm|cgi|pl|exe|jsp|asp|inc)/i", "$0-x", $filename);
	$it_thumb2 = abs(ip2long($_SERVER['REMOTE_ADDR'])).'_'.substr($shuffle,0,8).'_'.replace_filename($filename);

	$dest_file = G5_DATA_ITEM_PATH.'/'.$it_thumb2;


	$error_code = move_uploaded_file($tmp_file, $dest_file) or die($_FILES['it_thumb2']['error']);

	chmod($dest_file, G5_FILE_PERMISSION);

	if($it['it_thumb2'])
		@unlink(G5_DATA_ITEM_PATH.'/'.$it['it_thumb2']);

	sql_query(" update {$g5['item_table']} set it_thumb2 = '{$it_thumb2}' where it_id = '{$it_id}' ", true);

}


if(isset($_FILES['it_file']['name']) && $_FILES['it_file']['name']) {

	@mkdir(G5_DOWNLOAD_PATH, G5_DIR_PERMISSION);
	@chmod(G5_DOWNLOAD_PATH, G5_DIR_PERMISSION);

	$tmp_file  = $_FILES['it_file']['tmp_name'];

	$filename  = $_FILES['it_file']['name'];
	$filename  = get_safe_filename($filename);

	shuffle($chars_array);
	$shuffle = implode('', $chars_array);

	$filename = preg_replace("/\.(php|pht|phtm|htm|cgi|pl|exe|jsp|asp|inc)/i", "$0-x", $filename);
	$it_file = abs(ip2long($_SERVER['REMOTE_ADDR'])).'_'.substr($shuffle,0,8).'_'.replace_filename($filename);

	$dest_file = G5_DOWNLOAD_PATH.'/'.$it_file;


	$error_code = move_uploaded_file($tmp_file, $dest_file) or die($_FILES['it_file']['error']);

	chmod($dest_file, G5_FILE_PERMISSION);

	if($it['it_file'])
		@unlink(G5_DOWNLOAD_PATH.'/'.$it['it_file']);

	sql_query(" update {$g5['item_table']} set it_file = '{$it_file}', it_filename = '{$filename}' where it_id = '{$it_id}' ", true);

}

alert('정상적으로 등록되었습니다.', './itemform.php?w=u&it_id='.$it_id);
?>
