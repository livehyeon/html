<?php
$sub_menu = "500100";
include_once('./_common.php');

$g5['title'] = '회원가입/탈퇴';
include_once(G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

if($type=='year'){
	$txt_date = '년도';
	$date = date("Y", G5_SERVER_TIME);
	$end = '2021' ;

}elseif($type=='month'){
	$txt_date = '년월';

	$date =  date("Y-m", G5_SERVER_TIME);
	$end = date("Y-01", G5_SERVER_TIME);
}else{
	$txt_date = '일자';

	$date = $to_date ? $to_date : date("Y-m-d", G5_SERVER_TIME);
	$end = $fr_date ? $fr_date : date("Y-m-01", G5_SERVER_TIME);
}


$sql = " select count(*) as cnt from {$g5['member_table']}  where  mb_leave_date = '' ";
$row = sql_fetch($sql, true);
$total_count = $row['cnt'];

$sql = " select count(*) as cnt from {$g5['member_table']}  where  mb_leave_date = '' and substr(mb_datetime, 1, 7) = '".date("Y-m", G5_SERVER_TIME)."' ";
$row = sql_fetch($sql, true);
$month_count = $row['cnt'];


?>

<script>
$(function(){
    $("#fr_date, #to_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99", maxDate: "+0d" });
});
</script>
<div class="tbl_frm01 tbl_wrap">
	<table>
    <caption>누적수</caption>

    	<tbody>
			<th>누적회원수</th>
			<td><?php echo number_format($total_count); ?></td>
			<th><?php echo date("Y.m").'가입수'; ?></th>
			<td><?php echo number_format($month_count); ?></td>
        </tbody>
    </table>

</div>

<form name="fvisit" id="fvisit" class="local_sch03 local_sch" method="get" autocomplete="off">
<div class="sch_last">
    <strong>기간별검색</strong>
    <input type="text" name="fr_date" value="<?php echo $fr_date ?>" id="fr_date" class="frm_input" size="11" maxlength="10">
    <label for="fr_date" class="sound_only">시작일</label>
    ~
    <input type="text" name="to_date" value="<?php echo $to_date ?>" id="to_date" class="frm_input" size="11" maxlength="10">
    <label for="to_date" class="sound_only">종료일</label>
    <input type="submit" value="검색" class="btn_submit">
</div>
</form>

<ul class="anchor">
    <li><a href="./member_analytics.php?type=day">일</a></li>
    <li><a href="./member_analytics.php?type=month">월</a></li>
    <li><a href="./member_analytics.php?type=year">년</a></li>
</ul>

<div class="tbl_head01 tbl_wrap">

    <table>
    <caption>회원가입/탈퇴</caption>
	    <thead>
		    <tr>
		        <th scope="col"><?php echo $txt_date; ?></th>
		        <th scope="col">가입수</th>
		        <th scope="col">탈퇴수</th>
		        <th scope="col">상세내역</th>
		    </tr>
	    </thead>
    	<tbody>
		<?php
		while($date>=$end){

			$date2 = str_replace('-', '', $date);
			$length = strlen($date);
			$length2 = strlen($date2);
			$row1 = sql_fetch( " select count(*) as cnt from {$g5['member_table']}  where  mb_leave_date = ''  and substr(mb_datetime, 1, {$length}) = '{$date}' ");
			$row2 = sql_fetch( " select count(*) as cnt from {$g5['member_table']}  where  substr(mb_leave_date, 1, {$length2}) = '{$date2}' ");
		?>
			<tr>
				<td><?php echo $date; ?></td>
				<td><?php echo number_format($row1['cnt']); ?></td>
				<td><?php echo number_format($row2['cnt']); ?></td>
				<td><a href="<?php echo G5_ADMIN_URL; ?>/member_list.php?date=<?php echo $date; ?>" class='btn btn_03'>상세보기</a></td>
			</tr>
		<?php

			if($type=='year'){
				$date = date("Y", strtotime("{$date}-01-01 -1year"));
			}elseif($type=='month'){
				$date = date("Y-m", strtotime("{$date}-01 -1month"));
			}else{
				$date = date("Y-m-d", strtotime("{$date} -1day"));
			}

		}
		?>
        </tbody>
    </table>
</div>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>
