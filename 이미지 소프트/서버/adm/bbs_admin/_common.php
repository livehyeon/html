<?php
define('G5_IS_ADMIN', true);
include_once ('../../common.php');
include_once(G5_ADMIN_PATH.'/admin.lib.php');

if( isset($token) ){
    $token = @htmlspecialchars(strip_tags($token), ENT_QUOTES);
}

add_event('bbs_move_update', 'bbs_move_update', 10, 4);

function bbs_move_update(){
	alert_close('정상적으로 처리되었습니다.');
	exit;
}

$board_skin_path = get_skin_path('board', 'theme/admin');
$board_skin_url =	get_skin_url('board', 'theme/admin');

$sub_menu_array = array(
	'notice'=>'300910'
	, 'qa'=>'300920'
	, 'talktalk'=>'300930'
);
run_event('admin_common');

?>
