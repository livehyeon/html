<?php
$menu['menu500'] = array (
    array('500000', '통계관리', G5_ADMIN_URL.'/it_admin/member_analytics.php', 'member'),
    array('500100', '회원가입/탈퇴', G5_ADMIN_URL.'/it_admin/member_analytics.php', 'member'),
    array('500200', '보관함', G5_ADMIN_URL.'/it_admin/zzim_analytics.php', 'zzim'),
    array('500300', '매출', G5_ADMIN_URL.'/it_admin/order_analytics.php', 'order'),
    array('500400', '많이본상품', G5_ADMIN_URL.'/it_admin/view_analytics.php', 'view'),
    array('500500', '다운로드', G5_ADMIN_URL.'/it_admin/download_analytics.php', 'download') ,
    array('500600', '구글애널리틱스', 'https://www.google.com/analytics', 'google') ,
);
