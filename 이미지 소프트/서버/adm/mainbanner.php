<?php
$sub_menu = '100330';
include_once('./_common.php');

auth_check_menu($auth, $sub_menu, "r");

$g5['title'] = '메인배너 관리';
include_once (G5_ADMIN_PATH.'/admin.head.php');

add_stylesheet('<link rel="stylesheet" href="'.G5_JS_URL.'/remodal/remodal.css">', 11);
add_stylesheet('<link rel="stylesheet" href="'.G5_JS_URL.'/remodal/remodal-default-theme.css">', 12);
add_javascript('<script src="'.G5_JS_URL.'/remodal/remodal.js"></script>', 10);

$sql_common = " from {$g5['banner_table']} ";

// 테이블의 전체 레코드수만 얻음
$sql = " select count(*) as cnt " . $sql_common;
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함


$sql = "select * $sql_common order by bn_order desc, bn_no desc ";
$result = sql_query($sql);

// 전체 스페이스 구하기
$total_disk = disk_total_space(".");
$total_disk=intval($total_disk/1024/1024/1024);

// 사용가능한 공간 구하기
$FreeDiskSpace=Disk_Free_space(".");
$FreeDiskSpace=intval($FreeDiskSpace/1024/1024/1024);

// 사용중인 공간 구하기
$NoFreeDiskSpace=$total_disk-$FreeDiskSpace;

Print "<p align=right><font style=font-family:tahoma;font-size:7pt;color:#999999;line-height:120%>";
Print "<br>Total Space : $total_disk GB <br> Free Space : $FreeDiskSpace GB / Used Space : $NoFreeDiskSpace GB</font><br><br></p>";

?>
<script>
$(function(){
	$(".btn-form").click(function(){
		$href = $(this).attr('href');

		$.post($href, function($html){
			$(".is_form .wrap").html($html)
		})
	})
})
</script>
<div class="local_ov01 local_ov"><span class="btn_ov01"><span class="ov_txt">전체 </span><span class="ov_num">  <?php echo $total_count; ?>건</span></span></div>

<div class="btn_fixed_top ">
    <a href="./mainbannerform.php" data-remodal-target="modal_form" class="btn btn_01 btn-form">메인배너추가</a>
</div>

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col">이미지</th>
	    <th scope="col">제목</th>
        <th scope="col">시작일시</th>
        <th scope="col">종료일시</th>
        <th scope="col">우선순위</th>
        <th scope="col">관리</th>
    </tr>
    </thead>
    <tbody>
    <?php
    for ($i=0; $row=sql_fetch_array($result); $i++) {
        $bg = 'bg'.($i%2);

    ?>
    <tr class="<?php echo $bg; ?>">
        <td class="td_left" style="width:200px">
		<?php
		if(is_file(G5_DATA_BANNER_PATH."/{$row['bn_file1']}"))
			echo '<img src="'.G5_DATA_BANNER_URL.'/'.$row['bn_file1'].'" style="width:100%">'
		?>
		</td>
	    <td class="td_left"><?php echo $row['bn_name']; ?></td>
        <td class="td_datetime"><?php echo substr($row['bn_sdate'],2,14); ?></td>
        <td class="td_datetime"><?php echo substr($row['bn_edate'],2,14); ?></td>
        <td class="td_mng"><?php echo $row['bn_order']; ?></td>
        <td class="td_mng td_mng_m">
            <a href="./mainbannerform.php?bn_no=<?php echo $row['bn_no'].($qstr?"&{$qstr}":''); ?>" data-remodal-target="modal_form" class="btn btn_03 btn-form"><span class="sound_only"><?php echo $row['bn_name']; ?> </span>수정</a>
            <a href="./mainbannerdelete.php?w=d&amp;bn_no=<?php echo $row['bn_no'].($qstr?"&{$qstr}":''); ?>" onclick="return delete_confirm(this);" class="btn btn_02 "><span class="sound_only"><?php echo $row['bn_subject']; ?> </span>삭제</a>
        </td>
    </tr>
    <?php
    }

    if ($i == 0) {
        echo '<tr><td colspan="11" class="empty_table">자료가 한건도 없습니다.</td></tr>';
    }
    ?>
    </tbody>
    </table>
</div>


<div class="is_form remodal" data-remodal-id="modal_form" role="dialog" aria-labelledby="modalform" aria-describedby="modal2Desc" data-remodal-options="hashTracking: false">

    <button type="button" class="connect-close" data-remodal-action="close">
        <i class="fa fa-close"></i>
        <span class="txt">닫기</span>
    </button>
	<div class="wrap"></div>
</div>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
