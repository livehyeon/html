<?php
$menu['menu400'] = array (
    array('400000', '주문관리', ''.G5_ADMIN_URL.'/it_admin/orderlist.php', 'category'),
    array('400400', '라이선스 주문 관리', ''.G5_ADMIN_URL.'/it_admin/orderlist.php', 'order'),
    array('400500', '라이선스 만료 관리', ''.G5_ADMIN_URL.'/it_admin/endlist.php', 'ending'),
    array('400100', '분류관리', ''.G5_ADMIN_URL.'/it_admin/categorylist.php', 'category'),
    array('400200', '상품관리', ''.G5_ADMIN_URL.'/it_admin/itemlist.php', 'item'),
    array('400300', '쿠폰관리', ''.G5_ADMIN_URL.'/it_admin/couponlist.php', 'coupon') ,
);
