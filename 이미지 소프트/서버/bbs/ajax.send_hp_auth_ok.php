<?php
include_once('./_common.php');
include_once(G5_LIB_PATH."/register.lib.php");

$mb_hp =  preg_replace("/[^0-9]/", "", $mb_hp);

if($mb_hp != get_session('ss_hp')){
	$result = array(
		'msg'=>'휴대폰 번호가 일치하지 않습니다.'
		, 'status'=>''
	);
	die(json_encode($result));
}

if($mb_hp_auth != get_session('ss_hp_auth')){
	$result = array(
		'msg'=>'인증번호가 일치하지 않습니다.'
		, 'status'=>''
	);
	die(json_encode($result));
}


$result = array(
	'msg'=>''
	, 'status'=>'OK'
);
die(json_encode($result));

?>
