<?php
include_once('./_common.php');
include_once(G5_LIB_PATH."/register.lib.php");

if(!$mb_name)	{
	$result = array(
		'msg'=>'이름을 입력해주세요.'
		, 'status'=>''
	);
	die(json_encode($result));
}
$mb_hp =  preg_replace("/[^0-9]/", "", $mb_hp);

$msg = valid_mb_hp($mb_hp) ;

if($msg){
	$result = array(
		'msg'=>$msg
		, 'status'=>''
	);
	die(json_encode($result));
}
$sql = " select mb_email from {$g5['member_table']} where mb_name  = '{$mb_name}' and REPLACE( mb_hp, '-', '' ) = '{$mb_hp}' ";

$mb = sql_fetch($sql, true);

if(!isset($mb)){
	$result = array(
		'msg'=>'일치하는 정보가 없습니다.'
		, 'status'=>''
	);
	die(json_encode($result));
}

if($auth=='1'){

	if($_SESSION['ss_hp'] !=  $mb_hp || $_SESSION['ss_hp_auth'] != $mb_auth){
		$result = array(
			'msg'=>'인증번호가 일치하지 않습니다.'
			, 'status'=>''
		);

	}
	$result = array(
		'msg'=>"아이디는 {$mb['mb_email']} 입니다."
		, 'status'=>'OK'
	);

	die(json_encode($result));
	exit;
}

include_once(G5_LIB_PATH.'/icode.sms.lib.php');

$ss_hp_auth = rand(000000,999999);
set_session('ss_hp', $mb_hp);
set_session('ss_hp_auth', $ss_hp_auth);
// 인증번호 발송.

$send_number = preg_replace('/[^0-9]/', '', $sms5['cf_phone']);
$recv_number = $mb_hp;
$sms_content = "[{$config['cf_title']}] 인증번호 [{$ss_hp_auth}]를 입력해주세요.";
$SMS = new SMS; // SMS 연결
$SMS->SMS_con($config['cf_icode_server_ip'], $config['cf_icode_id'], $config['cf_icode_pw'], $config['cf_icode_server_port']);
$SMS->Add($recv_number, $send_number, $config['cf_icode_id'], iconv("utf-8", "euc-kr", stripslashes($sms_content)), "");
$SMS->Send();

// 인증번호 발송..
$result = array(
	'msg'=>''
	, 'status'=>'OK'
);
die(json_encode($result));

?>
