<?php
include_once('./_common.php');

$sql  = "
	select *
	from {$g5['coupon_table']}
	where cp_use = '1'
	and ( cp_enddatetype = 'days' or ( cp_enddatetype = 'date' and cp_enddate >= '".G5_TIME_YMD."' ))
";
if($cp_no){
	$sql .= "
		and cp_offline = '0'
		and cp_no = '{$cp_no}'
	";
}elseif($code){
	$sql .= "
		and cp_offline = '1'
		and cp_offline_code = '{$code}'
	";
}else{
	alert('잘못된 요청입니다.');
}

$cp = sql_fetch($sql, true);

if(!isset($cp['cp_no']))
	alert('이용가능한 쿠폰이 아닙니다.');

$row = sql_fetch(" select cm_no from {$g5['coupon_member_table']} where cp_no = '{$cp['cp_no']}' and mb_id = '{$member['mb_id']}'  ");
if(isset($row['cm_no']))
	alert('이미 등록하신 쿠폰입니다.');


if($cp['cp_type'] == '1' && $is_membership)
	alet('이미 이용권을 이용중입니다.');


$enddate = $cp['cp_enddatetype'] == 'date' ? $cp['cp_enddate'] : date("Y-m-d", strtotime("+{$cp['cp_enddays']}days"));

// 쿠폰 등록
$sql = "
	insert into {$g5['coupon_member_table']} set
		cp_no = '{$cp['cp_no']}'
		, mb_id = '{$member['mb_id']}'
		, cp_type = '{$cp['cp_type']}'
		, cp_benefit = '{$cp['cp_benefit']}'
		, cp_enddate = '{$enddate}'
		, cp_offline = '{$cp['cp_offline']}'
		, cm_status = '1'
";
sql_query($sql);
$cm_no = sql_insert_id();
// 무료 이용권쿠폰
if($cp['cp_type'] == '1'){
	$od_id = "od".get_uniqid();
	$title = "{$cp['cp_benefit']}일 무료이용권";
	$sql = "
		insert into {$g5['order_table']} set
			mb_id = '{$member['mb_id']}'
			, od_id  = '{$od_id}'
			, od_title  = '{$title}'
			, od_period  = '{$cp['cp_benefit']}'
			, od_unit  = 'days'
			, od_price  = '0'
			, cm_no  = '{$cm_no}'
			, od_coupon  = '0'
			, od_free = '1'
			, od_payment  = '무료이용권'
			, od_amount  = '0'
			, od_expiry_date  = '$enddate'
			, od_status  = '2'
	";
	sql_query($sql);
}


$sql = "
	update {$g5['coupon_table']} set
		cp_download = ( select count(cp_no) from {$g5['coupon_member_table']} where cp_no = '{$cp['cp_no']}' )
	where cp_no = '{$cp['cp_no']}'
";
sql_query($sql, true);

alert('정상적으로 다운로되었습니다.', G5_MYPAGE_URL."/coupon.php");
?>
