<?php
include_once('./_common.php');

if(!isset($g5['membership_order'][$od_membership]) || !$od_payment){

	alert('잘못된 요청입니다.', G5_MYPAGE_URL."/order.php");

}


$ms = $g5['membership_order'][$od_membership];
$amount = $price = $ms['price'];
$coupon = 0;
$expiry = date("Y-m-d", strtotime("+{$ms['period']}{$ms['unit']}"));
$status = in_array($od_payment , array('신용카드', '계좌이체', '휴대폰'))?'2':'1';
if($cm_no){
	$sql = "
		select *
		from {$g5['coupon_member_table']}
		where cm_status = '1'
		and cp_enddate >= '".G5_TIME_YMD."'
		and mb_id ='{$member['mb_id']}'
		and cp_type = '2'
		and cm_no ='{$cm_no}'
	";

	$cm = sql_fetch($sql);
	if(isset($cm['cm_no'])){
		$coupon = (int)($price * $cm['cp_benefit'] / 100);
		$amount -= $coupon;

 	}else{
		$cm_no = 0;
	}
}
$od_id = "od".get_uniqid();
$sql = "
	insert into {$g5['order_table']} set
		mb_id = '{$member['mb_id']}'
		, od_id  = '{$od_id}'
		, od_title  = '{$ms['title']}'
		, od_period  = '{$ms['period']}'
		, od_unit  = '{$ms['unit']}'
		, od_price  = '{$price}'
		, cm_no  = '{$cm_no}'
		, od_coupon  = '{$coupon}'
		, od_payment  = '{$od_payment}'
		, od_amount  = '{$amount}'
		, od_expiry_date  = '{$expiry}'
		, od_status  = '{$status}'
";
sql_query($sql, true);
if($cm_no){
	$sql = " update {$g5['coupon_member_table']} set cm_status = '2' where cm_no = '{$cm_no}' ";
	sql_query($sql, true);
}


alert('정상적으로 처리되었습니다.', G5_MYPAGE_URL."/membership.php");
?>
