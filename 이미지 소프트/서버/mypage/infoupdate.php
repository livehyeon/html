<?php
include_once("./_common.php");
include_once(G5_LIB_PATH.'/register.lib.php');

//print_r($member);

//print_r($_POST);

$mb_email 			= isset($_POST['mb_email']) ? trim($_POST['mb_email']) : '';
$mb_hp 				= isset($_POST['mb_hp']) ? trim($_POST['mb_hp']) : '';
$mb_hp_auth			= isset($_POST['mb_hp_auth']) ? trim($_POST['mb_hp_auth']) : '';
$mb_password_old    = isset($_POST['mb_password_old']) ? trim($_POST['mb_password_old']) : '';
$mb_password    	= isset($_POST['mb_password']) ? trim($_POST['mb_password']) : '';
$mb_password_re 	= isset($_POST['mb_password_re']) ? trim($_POST['mb_password_re']) : '';


$columns = array();
if($mb_email != $member['mb_email']){
	if ($msg = valid_mb_email($mb_email))   alert($msg, "", true, true);
	if ($msg = prohibit_mb_email($mb_email))alert($msg, "", true, true);
	if ($msg = exist_mb_email($mb_email, $member['mb_id']))   alert($msg, "", true, true);

	$columns[] = " mb_email = '{$mb_email}' ";
}

$mb_hp = preg_replace("/[^0-9]/", "", $mb_hp);
$member['mb_hp'] = preg_replace("/[^0-9]/", "", $member['mb_hp']);
if($mb_hp != $member['mb_hp']){

	if($mb_hp != get_session('ss_hp'))
		alert('휴대폰 번호가 일치하지 않습니다.', G5_MYPAGE_URL."/info.php");

	if($mb_hp_auth != get_session('ss_hp_auth'))
		alert('인증번호가 일치하지 않습니다.', G5_MYPAGE_URL."/info.php");

	$mb_hp = hyphen_hp_number($mb_hp);
	$columns[] = " mb_hp = '{$mb_hp}' ";
}

if($mb_password != ''){
	if(!login_password_check($member, $mb_password_old, $member['mb_password']) )
		alert('기존비밀번호가 일치하지 않습니다.', G5_MYPAGE_URL."/info.php");

	if($mb_password != $mb_password_re)
		alert('비밀번호가 일치하지 않습니다.', G5_MYPAGE_URL."/info.php");

	$columns[] = " mb_password = '".get_encrypt_string($mb_password)."' ";
}

if(count($columns)){
	$column = implode(", ", $columns);
	$sql = "
		update {$g5['member_table']} set
			{$column}
		where mb_id = '{$member['mb_id']}'
	";
	sql_query($sql, true);
}

alert('정상적으로 처리되었습니다.', G5_MYPAGE_URL."/info.php");
?>
