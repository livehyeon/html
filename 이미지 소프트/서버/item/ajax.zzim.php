<?php
include_once("./_common.php");

$response = array('msg'=>'','status'=>'0');

if(!$is_member){
	$response['msg'] = '로그인 후 이용하실 수 있습니다.';
	die(json_encode($response));
}

$it = sql_fetch(" select it_id from {$g5['item_table']} where it_id = '{$it_id}' and it_use = '1' ");
if(!isset($it['it_id'])){
	$response['msg'] = '잘못된 요청입니다.';
	die(json_encode($response));
}

$zz = sql_fetch(" select zz_no from {$g5['zzim_table']} where it_id = '{$it_id}' and mb_id = '{$member['mb_id']}' ");

if(!isset($zz['zz_no'])){
	$sql = " insert into {$g5['zzim_table']} set it_id = '{$it_id}', mb_id = '{$member['mb_id']}' ";
	$response['status'] = '1';
}else{
	$sql = " delete from {$g5['zzim_table']} where it_id = '{$it_id}' and mb_id = '{$member['mb_id']}' ";
	$response['status'] = '0';
}
sql_query($sql, true);

set_item_hot($it_id);

die(json_encode($response));
?>
