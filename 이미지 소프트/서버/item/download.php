<?php
include_once('./_common.php');

$it_no = isset($_REQUEST['it_no']) ? (int) $_REQUEST['it_no'] : 0;
$it = sql_fetch(" select it_id, it_file, it_filename from {$g5['item_table']} where it_no = '{$it_no}' ");

if(!$is_member)
	alert('로그인 후 이용해주세요.', G5_BBS_URL."/login.php?url=".urlencode("/item/view.php?it_id={$it['it_id']}"));
if(!$is_membership && !$is_admin)
	alert('권한이 없습니다.\n이용권 구매 후 이용해주세요.', G5_MYPAGE_URL."/order.php");

// clean the output buffer
ob_end_clean();



$filepath = G5_DATA_ITEM_PATH.'/'.$it['it_file'];

$filepath = addslashes($filepath);
$file_exist_check = (!is_file($filepath) || !file_exists($filepath)) ? false : true;

if ( false === $file_exist_check )
    alert('파일이 존재하지 않습니다.');

//$original = urlencode($file['bf_source']);
$original = rawurlencode($it['it_filename']);

if(preg_match("/msie/i", $_SERVER['HTTP_USER_AGENT']) && preg_match("/5\.5/", $_SERVER['HTTP_USER_AGENT'])) {
    header("content-type: doesn/matter");
    header("content-length: ".filesize($filepath));
    header("content-disposition: attachment; filename=\"$original\"");
    header("content-transfer-encoding: binary");
} else if (preg_match("/Firefox/i", $_SERVER['HTTP_USER_AGENT'])){
    header("content-type: file/unknown");
    header("content-length: ".filesize($filepath));
    //header("content-disposition: attachment; filename=\"".basename($file['bf_source'])."\"");
    header("content-disposition: attachment; filename=\"".$original."\"");
    header("content-description: php generated data");
} else {
    header("content-type: file/unknown");
    header("content-length: ".filesize($filepath));
    header("content-disposition: attachment; filename=\"$original\"");
    header("content-description: php generated data");
}
header("pragma: no-cache");
header("expires: 0");
flush();

$fp = fopen($filepath, 'rb');

// 4.00 대체
// 서버부하를 줄이려면 print 나 echo 또는 while 문을 이용한 방법보다는 이방법이...
//if (!fpassthru($fp)) {
//    fclose($fp);
//}

$download_rate = 10;

while(!feof($fp)) {
    //echo fread($fp, 100*1024);
    /*
    echo fread($fp, 100*1024);
    flush();
    */

    print fread($fp, round($download_rate * 1024));
    flush();
    usleep(1000);
}
fclose ($fp);
flush();

if(!$is_admin){

	$sql = "
		insert into {$g5['item_download_table']} set
			it_id = '{$it['it_id']}'
			, mb_id = '{$member['mb_id']}'
			, ip = '{$_SERVER['REMOTE_ADDR']}'
	";
	sql_query($sql);
	$sql = " update {$g5['order_table']} set od_download = od_download + 1 where od_no = '{$g5['order']['od_no']}' "; 
	sql_query($sql, ture);

	set_item_hot($it['it_id']);
}


?>
