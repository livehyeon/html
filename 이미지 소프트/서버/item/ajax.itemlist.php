<?php
include_once("./_common.php");

$sql_common = " from {$g5['item_table']}  ";
$sql_search = " where it_use = '1' ";
if (!$sst) {
    $sst  = "it_no";
    $sod = "desc";
}

if($tag){
	$sql_search .= " and  instr(it_tag, '{$tag}') ";
	$field = array('tag');
	insert_popular($field, $tag);
}


if(count($category)){
	for($i=0;$i<count($category);$i++)
		$category[$i] = "'{$category[$i]}'";

	$sql_search .= " and  it_id in( select distinct(it_id) from {$g5['item_category_table']} where category in (".implode(",", $category).") ) ";
}

$sql_order = " order by $sst $sod ";


$rows = 15;//$config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$sql = " select *
            {$sql_common}
            {$sql_search}
            {$sql_order}
            limit {$from_record}, {$rows} ";

$result = sql_query($sql, true);
?>
<ul>
<?php
while($row = sql_fetch_array($result)){
	$thumb = G5_DATA_ITEM_URL."/{$row['it_thumb']}";


	$thumb2 = ($row['it_thumb'] && is_file(G5_DATA_ITEM_PATH."/{$row['it_thumb2']}"))?G5_DATA_ITEM_URL."/{$row['it_thumb2']}":false;
	$subject = $row['it_name'];

	$row['it_tag'] = "#".str_replace("|", "|#", $row['it_tag']);
	$tags = explode("|", $row['it_tag']);
	$tags = implode(" ", $tags);

	$zzim = is_zzim($row['it_id'], $member['mb_id'])?' on': '';
?>
<li>
	<a href="<?php echo G5_ITEM_URL; ?>/view.php?it_id=<?php echo $row['it_id']; ?>" target="_blank">
		<div class="data_imgBox">
			<?php if($thumb2){ ?>
			<img src="<?php echo $thumb; ?>" alt="<?php echo $subject; ?>1" class="base">
			<img src="<?php echo $thumb2; ?>" alt="<?php echo $subject; ?>2" class="emot">
			<?php }else{ ?>
			<img src="<?php echo $thumb; ?>" alt="<?php echo $subject; ?>">
			<?php } ?>

			<em class="ico_rabbit<?php echo $zzim; ?>" data-id="<?php echo $row['it_id']; ?>"></em>
		</div>
		<div class="btm_txtBox">
			<strong><?php echo $subject; ?></strong>
			<span class="tag_txt"><?php?><?php echo $tags ?></span>
			<?if(it_new($row)){ ?><div class="n_txtBox"><em class="n_txt">NEW</em></div><?php } ?>
		</div>
	</a>
</li>
<?php
}
?>
</ul>
