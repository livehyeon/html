<?php
include_once('./_common.php');

$it = sql_fetch(" select * from {$g5['item_table']} where it_id = '{$it_id}' ");

if(!$it['it_id'])
	alert('잘못된 요청입니다.');

if(!$is_member)
	goto_url(G5_BBS_URL."/login.php?url={$urlencode}");
	
$it['it_content'] = str_replace("http://imagepage.openfield.co.kr", G5_URL, $it['it_content']);

if(!isset($_SESSION["ss_item_{$it_id}_view"])){
	$sql = "
		insert into {$g5['item_view_table']} set
			mb_id = '{$member['mb_id']}'
			, it_id = '{$it['it_id']}'
			, it_name = '{$it['it_name']}'
			, iv_ip = '{$_SERVER['REMOTE_ADDR']}'
	";
	sql_query($sql, true);
	set_session("ss_item_{$it_id}_view", '1');
}

if($it['it_tag'])
	$tags = explode("|", $it['it_tag']);


$wrap_class = 'detail_wrap';
$g5['title'] = $it['it_name'];

$zzim = is_zzim($it['it_id'], $member['mb_id'])?' on': '';

$it['it_tag'] = explode("|", $it['it_tag']);

include_once(G5_PATH.'/_head.php');
include_once($item_skin_path.'/view.skin.php');
include_once(G5_PATH.'/_tail.php');
?>
