jQuery(document).ready(function () {

	/*스크롤 탑 버튼*/
	$(window).scroll(function () {
		if ($(this).scrollTop() > 200) {
			$('.top').fadeIn();
		} else {
			$('.top').fadeOut();
		}
	});
	$('.top a').click(function (e) {
		e.preventDefault();
		$('html,body').animate({ scrollTop: 0 }, 300);
	});

	/*메뉴 스크롤 버튼*/
	$(window).scroll(function () {
		if ($(this).scrollTop() > 60) {
			$('.header_wrap').addClass('on');
		} else {
			$('.header_wrap').removeClass('on');
		}
	});

	/*모바일 메뉴 버튼*/
	$('body').append('<span class=\"dimd\"></span>');

	$('.m_filter_btn').click(function () {
		$('.m_filter_btnBox').toggleClass('on');
		$('.filter_boxArea').toggleClass('on');
	})
	$('.m_menuBtn').click(function () {
		$('.m_menuArea').addClass('on');
		$(".m_menuArea").css({"right":"0%"},100);
		$(".m_menuArea .dim").delay(0).fadeIn(0);
		$('body').addClass('on');
		$('.dimd').fadeIn();

	})
	$('.m_closeBtn').click(function () {
		$('.m_menuArea').removeClass('on');
		$(".m_menuArea").css({"right":"-100%"},100);
		$(".m_menuArea .dim").fadeOut(100);
		$('body').removeClass('on')
		$('.dimd').fadeOut();
	})
	$('.m_customerBtn').click(function () {
		$(this).toggleClass('on');
		$('.m_customerBox').toggleClass('on');
	})
	$('.ico_rabbit').click(function () {
		$(this).toggleClass('on');
	})

	//IMG를 Backgrond 이미지로 변환
	$('.slide_backImg').imgLiquid({fill:true, horizontalAlign:"top",  verticalAlign:"center"});
	// 카테고리 해더 FIX
	//ready

	/* 21.02.24 jhj 수정 */
	/*gif 이미지 노출*/
	$("#item-list").on("mouseenter", '.data_imgBox', function(){
	//$('.data_imgBox').mouseenter(function(){
		$(this).addClass('on');
	})
	$("#item-list").on("mouseleave", '.data_imgBox', function(){
	//$('.data_imgBox').mouseleave(function(){
		$(this).removeClass('on');

	});


});
