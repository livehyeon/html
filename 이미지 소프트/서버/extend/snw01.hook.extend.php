<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

add_event('register_form_before', 'register_form_before', 10, 0);
add_event('member_delete_after', 'member_delete_after', 10, 1);
add_event('register_form_update_after', 'register_form_update_after', 10, 2);
add_replace('is_admin', 'is_admin_hook', 10, 2);

function register_form_before(){
	if(!isset($_POST['w'])){
		$_POST['agree'] = $_POST['agree2'] = '1';
	}
}

function is_admin_hook($is_authority, $mb_id){
	global $g5;

	if($is_authority)
		return $is_authority;

	$auth = sql_fetch(" select count(*) cnt from {$g5['auth_table']} where mb_id = '{$mb_id}'", true );
	if($auth['cnt']>0)
		$is_authority = 'auth';

	return $is_authority;

}

function member_delete_after($mb_id){
	global $g5;
	$mb = get_member($mb_id);
	if(!$mb['mb_leave_date']){
		$sql = " update {$g5['member_table']} set mb_leave_date = '".date("Ymd", G5_SERVER_TIME)."' where mb_id = '{$mb_id}' ";
		sql_query($sql);
	}
}

function register_form_update_after($mb_id, $w){
	if($w=='u'){
		goto_url(G5_MYPAGE_URL.'/info.php');
	}else{
		goto_url(G5_MYPAGE_URL);
	}

}

?>
