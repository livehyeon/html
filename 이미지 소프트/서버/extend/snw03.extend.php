<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가


define('G5_RESOURCE_URL',     G5_URL.'/resource');

define('G5_MYPAGE_URL',     G5_URL.'/mypage');
define('G5_MYPAGE_PATH',     G5_PATH.'/mypage');

define('G5_SERVICE_URL',     G5_URL.'/service');
define('G5_SERVICE_PATH',     G5_PATH.'/service');


define('G5_BACKOFFICE_URL',     G5_URL.'/backoffice');
define('G5_BACKOFFICE_PATH',     G5_PATH.'/backoffice');

define('G5_ITEM_URL',     G5_URL.'/item');
define('G5_ITEM_PATH',     G5_PATH.'/item');

define('G5_DATA_ITEM_URL',     G5_DATA_URL.'/item');
define('G5_DATA_ITEM_PATH',     G5_DATA_PATH.'/item');

//define('G5_DOWNLOAD_PATH',     realpath(G5_PATH.'/../download'));
define('G5_DOWNLOAD_PATH',     G5_DATA_PATH.'/item');

define('G5_DATA_BANNER_URL',     G5_DATA_URL.'/banner');
define('G5_DATA_BANNER_PATH',     G5_DATA_PATH.'/banner');


$mypage_skin_path    = get_skin_path('mypage', 'theme/basic');
$mypage_skin_url    = get_skin_url('mypage', 'theme/basic');

$item_skin_path    = get_skin_path('item', 'theme/basic');
$item_skin_url    = get_skin_url('item', 'theme/basic');


$service_skin_path    = get_skin_path('service', 'theme/basic');
$service_skin_url    = get_skin_url('service', 'theme/basic');

$g5['category_table'] = G5_TABLE_PREFIX.'category';
$g5['item_table'] = G5_TABLE_PREFIX.'item';
$g5['item_category_table'] = G5_TABLE_PREFIX.'item_category';
$g5['item_download_table'] = G5_TABLE_PREFIX.'item_download';
$g5['item_related_table'] = G5_TABLE_PREFIX.'item_related';
$g5['item_view_table'] = G5_TABLE_PREFIX.'item_view';
$g5['zzim_table'] = G5_TABLE_PREFIX.'zzim';
$g5['order_table'] = G5_TABLE_PREFIX.'order';
$g5['coupon_table'] = G5_TABLE_PREFIX.'coupon';
$g5['coupon_member_table'] = G5_TABLE_PREFIX.'coupon_member';
$g5['banner_table'] = G5_TABLE_PREFIX.'banner';

$g5['item_new'] = date("Y-m-d H:i:s", strtotime("-3day"));

$g5['category_tree_array'] = array();
$g5['category_array'] = array();
$g5['category2_array'] = array();
$sql = " select ca_id, ca_name, length(ca_id) as len, ca_use from {$g5['category_table']} order by len asc, ca_order desc, ca_id asc ";
$result = sql_query($sql, true);
while($row=sql_fetch_array($result)){
	if($row['len'] == 2){
		$g5['category_tree_array'][$row['ca_id']]['name'] = $row['ca_name'];
		$g5['category_tree_array'][$row['ca_id']]['use'] = $row['ca_use'];
		$g5['category2_array'][$row['ca_id']] = $row['ca_name'];
	}elseif($row['len'] == 4){
		$ca_id1 = substr($row['ca_id'], 0, 2);
		$g5['category_tree_array'][$ca_id1]['sub'][$row['ca_id']]['name'] = $row['ca_name'];
		$g5['category_tree_array'][$ca_id1]['sub'][$row['ca_id']]['use'] = $row['ca_use'];
		$g5['category2_array'][$row['ca_id']] = "{$g5['category_tree_array'][$ca_id1]['name']}>{$row['ca_name']}";
	}
	$g5['category_array'][$row['ca_id']] = $row['ca_name'];
} 
$coupon_membership = $order_membership = $is_membership = false;
$membership_day = '-';
$t_date = date('Y-m-d',time());
if($is_member){
	$od = sql_fetch(" select * from {$g5['order_table']} where od_status = '2' and mb_id = '{$member['mb_id']}' and od_expiry_date >= '".G5_TIME_YMD."' order by od_no desc limit 1 ");
	if(isset($od['od_no'])){

		$g5['order'] = $od;
		$is_membership = true;
		if($od['od_payment'] == '무료이용권'){
			$coupon_membership = true;
		}else{
			$order_membership = true;
		}

		$membership_start_date = $od['od_paytime'];
		$membership_expiry_date = $od['od_expiry_date'];
		$membership_day = intval((strtotime($t_date) - strtotime($membership_expiry_date)) / 86400);

		$membership_day = $membership_day<0?"D{$membership_day}":'D-Day';

	}
	/*
	if(!$is_membership){ // 쿠폰
		$cm = sql_fetch( " select * from {$g5['coupon_member_table']} where mb_id = '{$member['mb_id']}' and cp_type = '1' and cp_enddate >= '".G5_TIME_YMD."' and cm_status = '1' order by cp_enddate desc limit 1");

		if(isset($cm['cm_no'])){
			$g5['coupon'] = $cm;

			$coupon_membership = $is_membership = true;
			$membership_start_date = $cm['cm_datetime'];
			$membership_expiry_date = $cm['cp_enddate'];
			$membership_day = intval((strtotime($t_date) - strtotime($membership_expiry_date)) / 86400);

			$membership_day = $membership_day<0?"D{$membership_day}":'D-Day';

		}

	}
	*/

}


$g5['membership_order'] = array(
	'1'=>array(
		'title'=>'유료회원 이용권 1년'
		, 'period'=>'1'
		, 'unit'=>'years'
		, 'price'=>66000
	)
);

$g5['email_domain_array'] = array(
	'hanmail.net'
	, 'naver.com'
	, 'empal.com'
	, 'paran.com'
	, 'nate.com'
	, 'korea.com'
	, 'hitel.net'
	, 'unitel.co.kr'
	, 'kornet.net'
	, 'nownuri.net'
	, 'gmail.com'
	, 'lycos.co.kr'
	, 'hanafos.com'
	, 'dreamwiz.com'
	, 'hanmir.com'
	, 'freechal.com'
	, 'chollian.net'
	, 'yahoo.co.kr'
	, 'hotmail.com'
);


$g5['coupon_type_array'] = array(
	'1'=>'무료 이용권'
	, '2'=>'할인 쿠폰'
);

$g5['payment_array'] = array('신용카드', '계좌이체', '휴대폰', '무통장 입금');

$g5['order_status_array'] = array(
	'1' => '주문접수'
	, '2'=>'결제완료'
	, '9'=>'주문취소'
);

if($is_member && !$is_admin){
	$is_admin = is_admin_hook($is_admin, $member['mb_id']);
}

if($is_member && !$is_admin && !$is_membership && $bo_table=='talktalk'){
	alert('이용권 구매 후 이용하실 수 있습니다.', G5_MYPAGE_URL."/order.php");
}
?>
