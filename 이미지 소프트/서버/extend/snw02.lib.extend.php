<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

function is_zzim($it_id, $mb_id){
	global $g5;
	if(!$mb_id) return false;
	$zz = sql_fetch(" select zz_no from {$g5['zzim_table']} where it_id = '{$it_id}' and mb_id = '{$mb_id}' ");
	return isset($zz['zz_no']);
}

function it_new($it){
	global $g5;
	return $it['it_datetime'] > $g5['item_new'] ? true : false;
}



// 한페이지에 보여줄 행, 현재페이지, 총페이지수, URL
function get_paging_new($write_pages, $cur_page, $total_page, $url, $add="")
{
    //$url = preg_replace('#&amp;page=[0-9]*(&amp;page=)$#', '$1', $url);
    $url = preg_replace('#(&amp;)?page=[0-9]*#', '', $url);
	$url .= substr($url, -1) === '?' ? 'page=' : '&amp;page=';

    $str = '';
    if ($cur_page > 1)
		$str .= '<a href="'.$url.'1'.$add.'" class="page_fisrt"><span>처음</span></a>'.PHP_EOL;

    $start_page = ( ( (int)( ($cur_page - 1 ) / $write_pages ) ) * $write_pages ) + 1;
    $end_page = $start_page + $write_pages - 1;

    if ($end_page >= $total_page) $end_page = $total_page;

	if ($start_page > 1) $str .= '<a href="'.$url.($start_page-1).$add.'" class="page_prev"><span>이전</span></a>'.PHP_EOL;

    if ($total_page > 1) {
        for ($k=$start_page;$k<=$end_page;$k++) {
            if ($cur_page != $k)
				$str .= '<a href="'.$url.$k.$add.'">'.$k.'</a>'.PHP_EOL;
            else
				$str .= '<strong class="current_page">'.$k.'</strong>'.PHP_EOL;
        }
    }

    if ($total_page > $end_page) $str .= '<a href="'.$url.($end_page+1).$add.'" class="page_next"><span>다음</span></a>'.PHP_EOL;

    if ($cur_page < $total_page) {
        $str .= '<a href="'.$url.$total_page.$add.'" class="pg_page pg_end">맨끝</a>'.PHP_EOL;
		$str .= '<a href="'.$url.$total_page.$add.'" class="page_last"><span>끝</span></a>'.PHP_EOL;
    }

    if ($str)
        return '<div class="paging">'.$str.'</div>';
    else
        return "";
}


function get_offline_code($cp_no){
	global $g5;
	$chars_array = array_merge(range(0,9), range('A','Z'));

	sql_query(" LOCK TABLE {$g5['coupon_table']} WRITE ");

	while(1){
		shuffle($chars_array);
		$shuffle = implode('', $chars_array);
		$code = substr($shuffle, 0, 12);
		echo "{$code}<br>";
		$row = sql_fetch(" select cp_no from {$g5['coupon_table']} where cp_offline_code = '{$code}' ", true);
		if(!isset($row['cp_no'])){
			sql_query(" update {$g5['coupon_table']} set cp_offline_code = '{$code}' where cp_no = '{$cp_no}' ", true);
			break;
		}
		usleep(10000);
	}
	sql_query(" UNLOCK TABLES ");
}

function set_item_hot($it_id){
	global $g5;


	$sql = "
		update {$g5['item_table']} set
			it_download = ( select count(distinct mb_id) from {$g5['item_download_table']} where it_id = '{$it_id}' )
		where it_id = '{$it_id}'
	";
	sql_query($sql, true);

	$sql = "
		update {$g5['item_table']} set
			it_zzim = ( select count(*) from {$g5['zzim_table']} where it_id = '{$it_id}')
		where it_id = '{$it_id}';
	";
	sql_query($sql, true);



	$sql = " update {$g5['item_table']} set it_hot = it_zzim + it_download where it_id = '{$it_id}' ";
	sql_query($sql, true);

}
?>
