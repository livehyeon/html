<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

$sql = "
	select *
	from {$g5['coupon_member_table']}
	where cm_status = '1'
	and cp_enddate >= '".G5_TIME_YMD."'
	and mb_id ='{$member['mb_id']}'
	order by cm_no desc
";
$result = sql_query($sql, true);

$rows = sql_num_rows($result);
?>
<script>
$(function(){
	/* poup */
	$.fn.popOpen = function(){
		$(this).bind('click', function(e){
			$title 	= $(this).attr('data-title');
			$name 	= $(this).attr('data-name');
			$no 	= $(this).attr('data-no');
			$type 	= $(this).attr('data-type');

			$("#txt-title").text($title);
			$(".txt-name").text($name);
			$(".pop_cont").hide();
			$(".pop_cont.pop_cont"+$type).show();
			$("#btn-download").attr('href' ,"<?php echo G5_MYPAGE_URL?>/coupon_update.php?cp_no="+$no);

			var s 	= $(this).attr("href");
			$(s).popup('show');
		});
		return false;
	};
	$('.btn_pop').popOpen();

	function pop_close(){
		$('.modal').popup('hide');
	}
})
</script>
<div id="container">
	<div class="wrapper">
		<div class="mypage_boxArea">
			<h2>내 쿠폰함</h2>
			<?php
			$on = 'coupon';
			include_once("{$mypage_skin_path}/top.skin.php");
			?>
			<div class="pick_listBox">
				<div class="cont_topArea">
					<p class="s_pTxt">총 <em class="yellow"><?php echo $rows; ?></em>개의 쿠폰이 있습니다. </p>
					<div class="right">
						<form name="frmcoupon" name="frmcoupon" action="<?php echo G5_MYPAGE_URL?>/coupon_update.php" method="post">
						<input type="text" name="code" class="btn_gray_w" placeholder="할인 쿠폰코드 입력해주세요">
						<button type="submit" class="btn_orange">쿠폰등록</button>
					</form>
					</div>
				</div>
				<div class="main_dataList">
					<div class="main_contentArea couponList">
						<ul>
						<?php

						$idx = 0;
						while($row=sql_fetch_array($result)){
						?>
							<li>
								<div class="c_box">
									<div class="coupon_topBox">
										<span class="<?php echo $row['cp_type']=='1'?'red roundTit':'yellow roundSale'?> "><?php echo $g5['coupon_type_array'][$row['cp_type']]?></span>
										<strong class="titTxt"><?php echo $row['cp_benefit'].($row['cp_type']=='1'?'일 ':'% ').$g5['coupon_type_array'][$row['cp_type']]; ?></strong>
										<em class="c_day"><?php echo date("Y.m.d", strtotime($row['cp_enddate'])); ?>일까지</em>
									</div>
									<div class="coupon_bottomBox">
										<a href="" class="btn_gray"><em></em>발급완료</a>
									</div>
								</div>
							</li>
						<?php
							$idx++;
						}
						if($idx==0) echo '<li class="empty">다운로드한 쿠폰이 없습니다.</li>'.PHP_EOL;
						?>
						</ul>
					</div>
					<div class="main_contentArea couponList">
						<ul>
							<?php
							$sql = "
								select *
								from {$g5['coupon_table']}
								where cp_use = '1'
								and cp_offline = '0'
								and cp_no not in (
									select cp_no
									from {$g5['coupon_member_table']}
									where mb_id = '{$member['mb_id']}'
								)
								and ( cp_enddatetype = 'days' or ( cp_enddatetype = 'date' and cp_enddate >= '".G5_TIME_YMD."' ))
								order by cp_no desc
							";
							$result = sql_query($sql, true);
							$idx = 0;
							while($row=sql_fetch_array($result)){
							?>
							<li>
								<div class="c_box">
									<div class="coupon_topBox">
										<span class="<?php echo $row['cp_type']=='1'?'red roundTit':'yellow roundSale'?> "><?php echo $g5['coupon_type_array'][$row['cp_type']]?></span>
										<strong class="titTxt"><?php echo $row['cp_benefit'].($row['cp_type']=='1'?'일 ':'% ').$g5['coupon_type_array'][$row['cp_type']]; ?></strong>
										<em class="c_day"><?php echo $row['cp_enddatetype']=='date'?date("Y.m.d", strtotime($row['cp_enddate'])).'일까지':"발급일로부터 {$row['cp_enddays']}일간 사용가능"; ?></em>
									</div>
									<div class="coupon_bottomBox">
										<a href="#pop01" class="btn_orange btn_pop"  data-title="<?php echo $row['cp_benefit'].($row['cp_type']=='1'?'일 ':'% ').$g5['coupon_type_array'][$row['cp_type']]; ?>" data-name="<?php echo $row['cp_type']=='1'?"무료이용권":"{$row['cp_benefit']}% 쿠폰"; ?>" data-no="<?php echo $row['cp_no']?>" data-type='<?php echo $row['cp_type']; ?>'><em></em>쿠폰받기</a>
									</div>
								</div>
							</li>
							<?php
								$idx++;
							}
							if($idx==0) echo '<li class="empty">다운로드 가능한 쿠폰이 없습니다.</li>'.PHP_EOL;
							?>
						</ul>
					</div>
				</div>
			</div>
			<!-- pick_listBox  -->
		</div>
	</div>
	<!--  wrapper -->
</div>
<!--  //container      -->

<section class="modal" id="pop01">
	<div class="modal_wrap">
		<!-- modal_header -->
		<div class="modal_header">
			<h2><span id="txt-title">7일 무료 이용권</span> 발급받기</h2>
		</div>
		<!-- //modal_header -->

		<!-- modal_body -->
		<div class="modal_body">
			<!-- pop_cont -->
			<div class="pop_cont pop_cont2">
				<p class="q_pick"><span class="txt-name">50% 쿠폰</span>을 발급 받겠습니까? <br>발급 받은 쿠폰은 이용권 결제시 사용이 가능합니다. </p>
			</div>

			<div class="pop_cont pop_cont1">
				<p class="q_pick"><span class="txt-name">무료이용권</span>을 발급 받겠습니까? <br>무료 이용권은 발급 받는 날로 바로 사용이 가능합니다.</p>
			</div>

			<!-- //pop_cont -->
			<!-- btn_area -->
			<div class="btn_area">
				<a href="#" class="btn_gray_short pop01_close">취소</a>
				<a href="#" class="btn_orange_short " id="btn-download">발급받기</a>
			</div>
			<!-- //btn_area -->
		</div>
		<!-- //modal_body -->
		<a href="#" class="btn_pop_close pop01_close"><em>팝업 닫기</em></a>
	</div>
</section>
