<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

$email = explode("@", $member['mb_email']);

$in_domain = in_array($email[1], $g5['email_domain_array']);

?>
<script>
$(function(){
	$("#email_domain1").change(function(){
		$val = $(this).val();
		if($val=='custom'){
			$(".email-custom").show();
			$(".email-custom").find('input[type="text"]').attr('prop', true);
		}else{
			$(".email-custom").hide();
			$(".email-custom").find('input[type="text"]').attr('prop', false);
		}
	});



	$("#btn_hp_auth").click(function(){
		$(".discord").remove();
		$(".red").removeClass('red');
		$val = $("#reg_mb_hp").val();

		$("#reg_mb_hp_auth").prop('disabled',true).val('');
		is_auth_com = is_auth = false
		if($val==''){
			$("#reg_mb_hp").addClass('red');
			$("#reg_mb_hp").select();
			$(".inp_box.phone").after('<span class="discord">휴대폰번호를 입력해주세요.</span>');
			return false;
		}
		$.get("<?php echo G5_BBS_URL; ?>/ajax.send_hp_auth.php?mb_hp="+$val, function($response){
			if($response.status != 'OK'){
				$("#reg_mb_hp").addClass('red');
				$(".inp_box.phone").after('<span class="discord">'+$response.msg+'</span>');
			}else{
				alert('전송되었습니다.');
				$("#reg_mb_hp_auth").prop('disabled',false).select()
				is_auth = true
			}
		}, 'json');
	});

	$("#btn_hp_auth_com").click(function(){

		if(!is_auth) return;

		is_auth_com = false;
		$(".discord").remove();
		$(".red").removeClass('red');
		$mb_hp = $("#reg_mb_hp").val();
		$mb_hp_auth = $("#reg_mb_hp_auth").val();

		if($mb_hp_auth==''){
			$("#reg_mb_hp_auth").addClass('red');
			$("#reg_mb_hp_auth").select();
			$(".inp_box.number").after('<span class="discord">인증번호를 입력해주세요.</span>');
			return false;
		}
		$.get("<?php echo G5_BBS_URL; ?>/ajax.send_hp_auth_ok.php?mb_hp="+$mb_hp+"&mb_hp_auth="+$mb_hp_auth, function($response){
			if($response.status != 'OK'){
				$("#reg_mb_hp_auth").addClass('red');
				$(".inp_box.number").after('<span class="discord">'+$response.msg+'</span>');
			}else{
				alert('인증되었습니다.');
				is_auth_com = true;

			}
		}, 'json');
	});


	$("#btn-update-email").click(function(){
		$(".email").prop('disabled', false);
		$(".email:eq(0)").select();
	});

	$("#btn-update-hp").click(function(){
		$("#update-hp").show();
		$("#now-hp").hide();
	});

	$("#email_domain1").trigger('change');
});


function finfo_submit(f){
	$(".red").removeClass('red');
	$(".discord").remove();

	/// 이메일체크;
	$email = $(".email:eq(0)").val();

	if($("#email_domain1").val()==''){
		$(".inp-email .btn_gray_w").addClass('red');
		$(".inp-email").eq(0).append('<span class="discord">이메일을 입력해주세요.</span>');
		$("#email_domain1").select();
		return false;
	} else if($("#email_domain1").val()=='custom'){
		if($("#email_domain2").val()==''){
			$(".inp-email .btn_gray_w").addClass('red');
			$(".inp-email").eq(1).append('<span class="discord">이메일을 입력해주세요.</span>');
			$("#email_domain2").select();
			return false;
		}else{
			$email += "@"+$("#email_domain2").val();
		}
	} else {
		$email += "@"+$("#email_domain1").val();
	}

	$("#reg_mb_email").val($email);

	if ( f.mb_email.defaultValue != f.mb_email.value) {
        var msg = reg_mb_email_check();
        if (msg) {
            alert(msg);
            f.reg_mb_email.select();
            return false;
        }
    }

    if (f.mb_password_old.value == '' && f.mb_password.value != '') {
		$("#reg_password_old").addClass('red');
		$("#reg_password_old").after('<span class="discord">기존 비밀번호를 입력해주세요.</span>');
        f.mb_password.focus();
        return false;
    }

    if (f.mb_password.value != '' && f.mb_password.value.length < 8) {
		$("#reg_mb_password").addClass('red');
		$("#reg_mb_password").after('<span class="discord">비밀번호를 8글자 이상 입력하십시오.</span>');
        f.mb_password.focus();
        return false;
    }


    if (f.mb_password.value != f.mb_password_re.value) {
		$("#reg_mb_password_re").addClass('red');
		$("#reg_mb_password_re").after('<span class="discord">비밀번호가 같지 않습니다.</span>');

        alert("비밀번호가 같지 않습니다.");
        f.mb_password_re.focus();
        return false;
    }

 	return true;

}


</script>
<div id="container">
	<div class="wrapper">
		<div class="mypage_boxArea ">
			<h2>회원정보</h2>
			<?php
			$on = 'info';
			include_once("{$mypage_skin_path}/top.skin.php");
			?>
			<div class="cont_topArea">
				<strong class="s_titleTxt">내 정보 수정</strong>
			</div>
			<form id="frminfo" name="frminfo" onsubmit="return finfo_submit(this);" method="post" action="<?php echo G5_MYPAGE_URL; ?>/infoupdate.php">
			<input type="hidden" name="mb_email" id="reg_mb_email" value="<?php echo $member['mb_email']?>">
			<div class="mypage_boardBox">
				<div class="board_conBox">
					<div class="info_leftTxt">
						<p class="p_bt13">이름</p>
					</div>
					<div class="info_contBox">
						<p class="p_bt13"><?php echo $member['mb_name']?></p>
					</div>
				</div>
				<div class="board_conBox">
					<div class="info_leftTxt">
						<p class="p_bt13">이메일</p>
					</div>
					<div class="info_contBox">
						<div class="inp_box">
							<div class="left middle">
								<label for="join_email" class="inp_titTxt">이메일</label>
								<input type="text" name="email[]" id="join_email"  value="<?php echo $email[0]?>" class="btn_gray_w email" placeholder="이메일" disabled>
							</div>
							<span class="center_mark">@</span>
							<div class="left middle">
								<select name="email[]" id="email_domain1" class="btn_gray_w email" disabled>
									<option value="">이메일주소</option>
									<option value="custom"<?php if(!$in_domain) echo ' selected="selected"'?>>직접입력</option>
									<?php
									foreach($g5['email_domain_array'] as $k=>$v)
										echo '<option value="'.$v.'"'.get_selected($email[1], $v).'>'.$v.'</option>'.PHP_EOL;
									?>
								</select>
							</div>
							<a href="#" class="btn_black_short" id="btn-update-email">수정</a>
						</div>

						<div class="inp_box email-custom inp-email" style="margin-top: 10px;">
							<input type="text" name="email[]" id="email_domain2" value="<?php echo $email[1]; ?>"  disabled class="btn_gray_w email" placeholder="도메인 직접입력">
						</div>
					</div>
				</div>
				<div class="board_conBox">
					<div class="info_leftTxt">
						<p class="p_bt62">휴대폰</p>
					</div>
					<!-- 회원정보 가입되어 있으면 info_membership 미가입이면 info_noMember -->
					<?php if($member['mb_hp']){ ?>
					<div class="info_contBox info_noMember" id="now-hp">
						<div class="inp_box">
							<span class="info_number"><?php echo $member['mb_hp']?></span>
							<button type="button" class="btn_black_short member_numBtn" id="btn-update-hp">수정</button>
						</div>
					</div>
					<?php } ?>
					<div class="info_contBox <?php echo $member['mb_hp']?'info_membership':'info_noMember'?>" id="update-hp">
						<div class="inp_box">
							<div class="left p_number">
								<label for="reg_mb_hp" class="inp_titTxt">휴대폰</label>
								<input type="tel" id="reg_mb_hp" name="mb_hp" value="<?php echo $member['mb_hp']?>" class="btn_gray_w " placeholder="휴대폰 번호 입력">
							</div>
							<div class="left btn_conBox">
								<button type="button" class="btn_orange_short" id="btn_hp_auth">인증</button>
							</div>
						</div>
						<div class="inp_box ">
							<div class="left p_number">
								<label for="join_email" class="inp_titTxt"> 인증번호 입력</label>
								<input type="tel" class="btn_gray_w" name="mb_hp_auth" id="reg_mb_hp_auth" disabled placeholder="인증번호 입력">
							</div>
							<div class="left btn_conBox">
								<button type="button" class="btn_gray_short" id="btn_hp_auth_com">인증</button>
							</div>
						</div>
						<div class="inp_box sms">
							<input type="checkbox" class="inp_cont" id="snsAlign" name="mb_sms" value="1"<?php echo get_checked('1', $member['mb_sms'])?>>
							<label for="snsAlign">
								<span>SMS 수신동의</span>
							</label>
						</div>
					</div>
				</div>
				<div class="board_conBox">
					<div class="info_leftTxt">
						<p class="p_bt86">비밀번호</p>
					</div>
					<!-- 회원정보 가입되어 있으면 info_membership 미가입이면 info_noMember -->
					<!-- member_pwBtn클릭시  pwBox info_noMember 노출-->
					<div class="info_contBox info_membership">
						<div class="inp_box">
							<a href="#" class="btn_black_short member_pwBtn">수정</a>
						</div>
					</div>
					<div class=" info_contBox pwBox info_noMember">
						<div class="inp_box">
							<label for="reg_password_old" class="inp_titTxt">현재 비밀번호</label>
							<input type="password" class="btn_gray_w " name="mb_password_old" id="reg_password_old"  placeholder="현재 비밀번호">
						</div>
						<div class="inp_box">
							<label for="reg_mb_password" class="inp_titTxt">변경 비밀번호</label>
							<input type="password" class="btn_gray_w " name="mb_password" id="reg_mb_password"  placeholder="변경 비밀번호">
						</div>
						<div class="inp_box">
							<label for="reg_mb_password_re" class="inp_titTxt">변경 비밀번호 확인</label>
							<input type="password" class="btn_gray_w" name="mb_password_re" id="reg_mb_password_re"  placeholder="변경 비밀번호 확인">
						</div>

					</div>
				</div>

				<?php
				if( function_exists('social_member_provider_manage') ){
					social_member_provider_manage();
				}
				?>
				<div class="center_area">
					<a href="<?php echo G5_MYPAGE_URL; ?>" class="btn_gray_middle">취소</a>

					<button class="btn_orange_middle">수정</button>
				</div>
			</div>
		</div>
		<!--// service_boxArea -->


	</div>
	<!--  wrapper -->
</div>
<!--  //container      -->
