<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가


$sql_common = " from {$g5['write_prefix']}qa  ";
$sql_search = " where mb_id = '{$member['mb_id']}' and wr_is_comment = '0' ";
if(!$sfl) $sfl = 'wr_subject';
if($stx){
	$sql_search .= " and {$sfl} like '%{$stx}%' ";
}
$sql_order = " order by wr_num asc ";

$sql = " select count(*) as cnt {$sql_common} {$sql_search} {$sql_order} ";
$row = sql_fetch($sql, true);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$sql = " select * {$sql_common} {$sql_search} {$sql_order} limit {$from_record}, {$rows} ";
$result = sql_query($sql);
$rows = sql_num_rows($result);
$write_pages = get_paging_new(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, get_pretty_url('qa', '', $qstr.'&amp;page='));
?>
<div id="container">
	<div class="wrapper">
		<div class="mypage_boxArea ">
			<h2>내 문의함</h2>
			<?php
			$on = 'qa';
			include_once("{$mypage_skin_path}/top.skin.php");
			?>
			<div class="cont_topArea">
				<p class="s_pTxt">총 <em class="yellow"><?php echo $total_count; ?></em>개의 문의가 있습니다. </p>
				<div class="right">
					<form action="#" id="tab_search">
						<fieldset>
							<legend>검색어 입력폼</legend>
							<select name="sfl" id="sfl" class="btn_gray_w select01">
								<option value="wr_subject"<?php echo get_selected($sfl, 'wr_subject', true); ?>>제목</option>
								<option value="wr_content"<?php echo get_selected($sfl, 'wr_content', true); ?>>내용</option>
							</select>
							<div class="searchArea">

								<div class="search_box">
									<input type="text" class="search_txt" id="stx" name="stx" value="<?php echo stripslashes($stx) ?>" required placeholder="검색어 입력해주세요.">
									<button id="img_search" type="submit">
										<span class="s_txt">검색</span>
									</button>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
			<?php if($rows>0){ ?>
			<div class=" service_tblBox ask">
				<table class="tableBox">
					<caption>1대1 문의</caption>
					<col style="width:*">
					<col style="width:*">
					<tbody>
					<?php while($row=sql_fetch_array($result)){	?>
						<tr>
							<td class="td_txt">
								<a href="<?php echo get_pretty_url('qa', $row['wr_id']); ?>">
								<?php
								if($row['wr_comment'])
									echo '<strong class="notice_tag fw400">완료</strong>';
								else
									echo '<strong class="notice_tag gray fw400">대기</strong>';

									echo $row['wr_subject'];
								?>
								</a>
							</td>
							<td class="td_date"><?php echo date("Y.m.d", strtotime($row['wr_datetime'])); ?></td>
						</tr>
					<?php
					}
					?>
					</tbody>
				</table>

			</div>
			<?php } else { ?>
			<div class="mypage_boardBox ask">
				<div class="ask_content">
					<img src="<?php echo G5_RESOURCE_URL; ?>/images/contents/ico_noContent.png" alt="">
					<p><?php echo $stx?'검색된':'작성된'?> 문의가 없습니다.</p>
				</div>
			</div>
			<?php } ?>
		</div>
		<!--// mypage_boxArea -->
		<?php echo $write_pages; ?>
		<div class="center_area">
			<a href="<?php echo short_url_clean(G5_BBS_URL.'/write.php?bo_table=qa') ?>" class="btn_orange_middle">문의하기</a>
		</div>
	</div>
	<!--  wrapper -->
</div>
<!--  //container      -->
