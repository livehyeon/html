<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
?>

<div id="container">
	<div class="wrapper">
		<div class="mypage_boxArea ">
			<h2>내 이용권</h2>
			<?php
			$on = 'membership';
			include_once("{$mypage_skin_path}/top.skin.php");
			?>
			<div class="cont_topArea">
				<strong class="s_titleTxt">내 이용권</strong>
			</div>
			<!-- 유료회원일때 노출항목  -->
			<div class="mypage_boardBox padeMember">
				<table class="tableBox">
					<caption>내 이용권</caption>
					<colgroup>
						<col style="width:*">
						<col style="width:*">
						<col style="width:*">
						<col style="width:*">
						<col style="width:*">
					</colgroup>
					<thead>
						<tr>
							<th class="td_info">회원 정보</th>
							<th class="td_voucher">내 이용권</th>
							<th class="td_usePeriod">이용기간</th>
							<th class="td_overPeriod">잔여기간</th>
							<th class="td_day">결제일</th>
						</tr>
					</thead>
					<tbody>
						<?php if($is_membership){ ?>
						<tr>
							<td class="td_txt"><?php echo $member['mb_name']?> (<?php echo $member['mb_email']?>)</td>
							<td>
							<?php
							if($order_membership){
								echo '유료회원 이용권';
								if($g5['order']['od_download'] == 0 && G5_TIME_YMDHIS < date("Y-m-d H:i:s", strtotime("{$g5['order']['od_paytime']}+7day")) )
									echo '<a href="'.G5_MYPAGE_URL.'/order_cancel.php" onclick="return confirm(\'정말로 취소하시겠습니까?\')" class="btn_black_middle">내 이용권 취소하기</a>';

							}else{
								echo '무료회원 이용권';
							?>
							<a href="<?php echo G5_MYPAGE_URL; ?>/order.php" class="btn_orange_middle">유료회원 이용권 구매</a>
							<?php
							}
							?>
							</td>
							<td><?php echo date("Y. m. d.", strtotime($membership_start_date))." ~ ".date("Y. m. d.", strtotime($membership_expiry_date))?></td>
							<td><?php echo $membership_day ?></td>
							<td><?php echo date("Y. m. d.", strtotime($membership_start_date)) ?></td>
						</tr>
						<?php }else{?>
						<tr>
							<td class="empty" colspan="5">이용중인 이용권이 없습니다.</td>
						</tr>

						<?php } ?>
					</tbody>
				</table>
			</div>


	</div>
	<!--  wrapper -->
</div>
<!--  //container      -->
