<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

$sql_common = " from {$g5['zzim_table']} a, {$g5['item_table']} b ";
$sql_search = " where  a.mb_id = '{$member['mb_id']}' and a.it_id = b.it_id and b.it_use = '1' ";
$sql_order = " order by a.zz_no desc  ";
$sql = " select count(*) as cnt
            {$sql_common}
            {$sql_search}
            {$sql_order} ";
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = 25;//$config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$sql = " select *
            {$sql_common}
            {$sql_search}
            {$sql_order}
            limit {$from_record}, {$rows} ";
$result = sql_query($sql);

?>
<script>
$(function(){
	$(".chk-no").click(function(){
		$length = $(".chk-no:checked").length ;
		$("#chk-count").text($length);
	});

	$("#btn-chk-cancel").click(function(){
		$(".chk-no").prop('checked', false)
		$("#chk-count").text(0);
	});

	$("#frmzzim").submit(function(){
		$length = $(".chk-no:checked").length ;
		if($length==0){
			alert('삭제하실 상품을 선택해주세요.');
			return false;
		}

		return confirm('정말로 삭제하시겠습니까?');
	});
})
</script>
<div id="container">
	<div class="wrapper">
		<div class="mypage_boxArea">
			<h2>내 찜 목록</h2>
			<?php
			$on = 'zzim';
			include_once("{$mypage_skin_path}/top.skin.php");
			?>

			<div class="pick_listBox">
				<form id="frmzzim" name="frmzzim" action="./zzimlistupdate.php" method="post">
				<input type="hidden" name="page" value="<?php echo $page ?>">
				<div class="cont_topArea">
					<p class="s_pTxt"> <em class="yellow" id="chk-count">0</em>/<?php echo number_format($total_count); ?>개 선택</p>
					<div class="right">
						<button type="button" class="btn_gray_short" id="btn-chk-cancel">취소</button>
						<button type="submit" class="btn_black_short" id="btn-chk-del">삭제</button>
					</div>
				</div>
				<div class="main_dataList">
					<div class="main_contentArea">
						<ul>
							<?php
							$idx = 0;
							while($row=sql_fetch_array($result)){
								$thumb = G5_DATA_ITEM_URL."/{$row['it_thumb']}";
							?>
							<li>
								<a href="<?php echo G5_ITEM_URL; ?>/view.php?it_id=<?php echo $row['it_id']?>">
									<div class="data_imgBox">
										<img src="<?php echo $thumb; ?>" alt="">
										<div class="list_cont">
											<input type="checkbox" class="inp_cont chk-no" name="zz_no[]" value="<?php echo $row['zz_no']?>" id="img<?php echo $idx; ?>">
											<label for="img<?php echo $idx; ?>"></label>
										</div>
										<div class="shadow"></div>
									</div>
								</a>
							</li>
							<?php
								$idx++;
							}
							if($idx==0) echo '<li style="width:100%;line-height:50px;text-align:center">찜목록이 없습니다.</li>';
							?>

						</ul>
					</div>
				</div>
				</form>
			</div>
			<!-- pick_listBox  -->
			<?php
			$pagelist = get_paging_new(5, $page, $total_page, $_SERVER['SCRIPT_NAME'].'?'.$qstr.'&amp;page=');
			echo $pagelist;
			?>

		</div>
	</div>
	<!--  wrapper -->
</div>
<!--  //container      -->
