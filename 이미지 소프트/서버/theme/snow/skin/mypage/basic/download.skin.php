<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가


$sql_common = " from {$g5['item_download_table']} a, {$g5['item_table']} b ";
$sql_search = " where  a.mb_id = '{$member['mb_id']}' and a.it_id = b.it_id  ";
$sql_order = " group by a.it_id order by a.id_no desc  ";

if($year){
	$year = preg_replace('/[^0-9_]/i', '', trim($year));
	$sql_search .= " and substring(a.id_datetime, 1, 4) = '{$year}' ";
	$qstr .= '&amp;year='.$year;
}


if($month){
	$month = preg_replace('/[^0-9_]/i', '', trim($month));
	$sql_search .= " and substring(a.id_datetime, 6, 2) = '{$month}' ";
	$qstr .= '&amp;month='.$month;
}

if($stx){
	$sql_search .= " and instr(it_tag, '{$stx}') ";
}


$sql = " select count(distinct a.it_id) as cnt
            {$sql_common}
            {$sql_search}
            {$sql_order} ";

$row = sql_fetch($sql, true);
$total_count = $row['cnt'];

$rows = 25;//$config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$sql = " select max(a.id_datetime) id_datetime, a.ip, b.it_thumb, b.it_id
            {$sql_common}
            {$sql_search}
            {$sql_order}
            limit {$from_record}, {$rows} ";

$result = sql_query($sql, true);


?>
<div id="container">
	<div class="wrapper">
		<div class="mypage_boxArea">
			<h2>다운로드 내역</h2>
			<?php
			$on = 'download';
			include_once("{$mypage_skin_path}/top.skin.php");
			?>

			<div class="pick_listBox downCon">
				<div class="cont_topArea">
					<p class="s_pTxt">총 <em class="yellow"><?php echo number_format($total_count)?></em>개의 소식이 있습니다. </p>
					<div class="right">
						<form action="<?php echo G5_MYPAGE_URL ?>/download.php" id="tab_search">
						<select name="year" id="year" class="btn_gray_w select04">
							<option value="">년</option>
							<?php
							for($i=2021;$i<=(int)date("Y");$i++)
								echo '<option value="'.$i.'"'.get_selected("{$i}", "{$year}").'>'.$i.'</option>'.PHP_EOL;
							?>
						</select>
						<select name="month" id="month" class="btn_gray_w select04">
							<option value="">월</option>
							<?php
							for($i=1;$i<=12;$i++){
								$mon = substr("0{$i}", -2);
								echo '<option value="'.$mon.'"'.get_selected("{$mon}", "{$month}").'>'.$mon.'</option>'.PHP_EOL;
							}
							?>
						</select>
						<div class="searchArea">
								<fieldset>
									<legend>검색어 입력폼</legend>
									<div class="search_box">
										<input type="text" class="search_txt" name="stx" id="stx" value="<?php echo $stx; ?>" placeholder="검색어 입력">
										<button id="img_search" type="submit">
											<span class="s_txt">검색</span>
										</button>
									</div>
								</fieldset>

						</div>
						</form>
					</div>
				</div>
				<div class="main_dataList">
					<div class="main_contentArea">
						<ul>
							<?php
							$idx = 0;
							while($row=sql_fetch_array($result)){
								$thumb = G5_DATA_ITEM_URL."/{$row['it_thumb']}";
							?>
							<li>
								<a href="<?php echo G5_ITEM_URL; ?>/view.php?it_id=<?php echo $row['it_id']?>">
									<div class="data_imgBox">
										<img src="<?php echo $thumb; ?>" alt="">

									</div>
									<div class="btm_txtBox">
										<ul>
											<li>
												<p>다운일시</p>
												<span><?php echo $row['id_datetime']?></span>
											</li>
											<li>
												<p>다운IP</p>
												<span><?php echo $row['ip']?></span>
											</li>
										</ul>
									</div>
								</a>
							</li>
							<?php
								$idx++;
							}
							if($idx==0) echo '<li style="width:100%;line-height:50px;text-align:center">다운로드 내역이 없습니다.</li>';
							?>
						</ul>
					</div>
				</div>
			</div>
			<!-- pick_listBox  -->
			<?php
			$pagelist = get_paging_new(5, $page, $total_page, $_SERVER['SCRIPT_NAME'].'?'.$qstr.'&amp;page=');
			echo $pagelist;
			?>
		</div>
	</div>
	<!--  wrapper -->
</div>
<!--  //container      -->
