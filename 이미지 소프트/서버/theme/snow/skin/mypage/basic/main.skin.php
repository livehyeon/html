<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
?>
<div id="container">
	<div class="wrapper">
		<div class="mypage_boxArea">
			<h2>마이페이지</h2>
			<?php
			$on = 'main';
			include_once("{$mypage_skin_path}/top.skin.php");
			?>
			<ul class="profile_boxArea">
				<li class="prof_nameBox">
					<div class="prof_imgBox">
						<img src="<?php echo G5_RESOURCE_URL; ?>/images/data/img_dummy.jpg" alt="">
					</div>
					<div class="prof_infoBox">
						<strong><?php echo $member['mb_name']?>님</strong>
						<p><?php echo $member['mb_email']?></p>
						<a href="<?php echo G5_MYPAGE_URL; ?>/info.php" class="modify_btn">회원정보수정</a>
					</div>
				</li>
				<li class="prof_detailBox">
					<div class="top_btnbox">
						<strong>내 이용권</strong>
						<a href="<?php echo G5_MYPAGE_URL ?>/membership.php" class="more_btn">더보기</a>
					</div>
					<a href="<?php echo G5_MYPAGE_URL ?>/order.php" class="btn_orange a_pc">이용권 구매</a>
					<ul class="coupon_count a_mobile">
						<li>
							<p>이용권 날짜</p>
							<span><?php echo $is_membership?date("Y.m.d", strtotime($membership_expiry_date)):'-'?></span>
						</li>
						<li>
							<p>잔여일</p>
							<span><em class="yellow"><?php echo $membership_day; ?> </em></span>
						</li>
					</ul>
				</li>
				<li class="prof_detailBox">
					<div class="top_btnbox">
						<strong>내 쿠폰함</strong>
						<a href="<?php echo G5_MYPAGE_URL ?>/coupon.php" class="more_btn">더보기</a>
					</div>
					<?php

					$sql = "
						select count(*) cnt
						from {$g5['coupon_member_table']}
						where cm_status = '1'
						and cp_enddate >= '".G5_TIME_YMD."'
						and mb_id ='{$member['mb_id']}'

					";
					$cm = sql_fetch($sql);

					$sql = "
						select count(*) cnt
						from {$g5['coupon_table']}
						where cp_use = '1'
						and cp_offline = '0'
						and cp_no not in (
							select cp_no
							from {$g5['coupon_member_table']}
							where mb_id = '{$member['mb_id']}'
						)
						and ( cp_enddatetype = 'days' or ( cp_enddatetype = 'date' and cp_enddate >= '".G5_TIME_YMD."' ))

					";
					$cp = sql_fetch($sql);
					?>
					<ul class="coupon_count">
						<li>
							<p>사용가능 쿠폰</p>
							<span><em class="yellow"><?php echo $cm['cnt']?></em>건</span>
						</li>
						<li>
							<p>다운로드 가능 쿠폰</p>
							<span><em class="yellow"><?php echo $cp['cnt']?></em>건</span>
						</li>
					</ul>
				</li>
			</ul>
			<!--//  profile_boxArea -->
			<div class="pick_listBox">
				<div class="cont_topArea">
					<strong class="s_titleTxt">내 찜목록</strong>
					<a href="<?php echo G5_MYPAGE_URL; ?>/zzim.php" class="more_btn">더보기</a>
				</div>
				<div class="main_dataList">
					<div class="main_contentArea">
						<ul>
						<?php
						$sql = " select b.it_id, b.it_thumb from {$g5['zzim_table']} a, {$g5['item_table']} b where  a.mb_id = '{$member['mb_id']}' and a.it_id = b.it_id and b.it_use = '1' order by a.zz_no desc limit 15 ";
						$result = sql_query($sql, true);
						$idx = 0;
						while($row = sql_fetch_array($result)){
							$thumb = G5_DATA_ITEM_URL."/{$row['it_thumb']}";

						?>
							<li>
								<a href="<?php echo G5_ITEM_URL; ?>/view.php?it_id=<?php echo $row['it_id']?>">
									<div class="data_imgBox">
										<img src="<?php echo $thumb; ?>" alt="">
									</div>
								</a>
							</li>
						<?php
							$idx++;
						}

						if($idx==0) echo '<li class="empty">등록한 찜이 없습니다.<li>';
						?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--  wrapper -->
</div>
<!--  //container      -->
