<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
?>
<div class="top_listBox">
	<ul>
		<!-- on클래스는 선택된 페이지 l_lineNone클래스는 모바일에서 border-left선 지우는것         -->
		<li><a href="<?php echo G5_MYPAGE_URL; ?>" class="<?php echo ($on=='main'?'on':''); ?>">마이페이지</a></li>
		<li><a href="<?php echo G5_MYPAGE_URL; ?>/info.php" class="<?php echo ($on=='info'?'on':''); ?>">회원정보</a></li>
		<li><a href="<?php echo G5_MYPAGE_URL; ?>/membership.php" class="<?php echo ($on=='membership'?'on':''); ?>">내 이용권</a></li>
		<li class="l_lineNone"><a href="<?php echo G5_MYPAGE_URL; ?>/coupon.php" class="<?php echo ($on=='coupon'?'on':''); ?>">내 쿠폰함</a></li>
		<li><a href="<?php echo G5_MYPAGE_URL; ?>/zzim.php" class="<?php echo ($on=='zzim'?'on':''); ?>">내 찜목록</a></li>
		<li><a href="<?php echo G5_MYPAGE_URL; ?>/download.php" class="<?php echo ($on=='download'?'on':''); ?>">다운로드내역</a></li>
		<li class="l_lineNone"><a href="<?php echo G5_MYPAGE_URL; ?>/order.php" class="<?php echo ($on=='order'?'on':''); ?>">구매/결제내역</a></li>
		<li><a href="<?php echo G5_MYPAGE_URL; ?>/qa.php" class="<?php echo ($on=='qa'?'on':''); ?>">내 문의함</a></li>
	</ul>
</div>
