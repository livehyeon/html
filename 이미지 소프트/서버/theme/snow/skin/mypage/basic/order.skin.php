<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
?>
<script>
$(function(){
	$("#frmorder").submit(function(){
		return true;
	});
	$("#btn-order").click(function(){
		if($(".membership:checked").length==0){
			alert('상품을 선택해주세요.');
			return false;
		}
		return true;
	})
	$(".membership").click(function(){
		get_price();
	});

	$("#cm_no").change(function(){
		get_price();
	})
});

function get_price(){
	if($(".membership:checked").length==0)
		return false;
	$price = parseInt($(".membership:checked").attr('data-price'));
	$(".txt-price").text(number_format($price)+"원");
	$discount = 0;
	if($("#cm_no").length>0){
		$benefit = parseInt($("#cm_no option:selected").attr('data-benefit'));

		$discount = parseInt($price * $benefit / 100);

	}
	$(".txt-dicount").text(number_format($discount)+"원");
	$(".txt-amount").text(number_format($price-$discount)+"원");
}
</script>
<div id="container">
	<div class="wrapper">
		<form name="frmorder" id="frmorder" method="post" action="./orderupdate.php">
		<div class="mypage_boxArea buying">
			<h2>구매/결제내역</h2>
			<?php
			$on = 'order';
			include_once("{$mypage_skin_path}/top.skin.php");
			?>
			<div class="buy_infoBox">
				<div class="cont_topArea">
					<strong class="s_titleTxt">유료회원이용권 구매</strong>
					<div class="buy_boxArea">
						<strong><i></i>유료회원이용권 기간은 구매하신 날부터 시작됩니다.</strong>
					</div>
				</div>
				<div class="mypage_boardBox">
					<div class="board_conBox">
						<div class="info_leftTxt">
							<p class="p_bt23">상품정보</p>
						</div>
						<div class="info_contBox">
							<?php
							$idx = 0;
							foreach($g5['membership_order'] as $k => $array ){ ?>
							<label>
								<div class="radioList">
									<input type="radio" id="r0" name="od_membership" class="membership" value="<?php echo $k ?>" data-price="<?php echo  $array['price']; ?>"<?php if($idx==0) echo ' required'?> />
									<label for="r0"><span></span></label>
								</div>
								<div class="product_infoBox">
									<img src="<?php echo G5_RESOURCE_URL; ?>/images/contents/img_freepass.png" alt="" class="a_pc">
									<img src="<?php echo G5_RESOURCE_URL; ?>/images/contents/img_freepass_m.png" alt="" class="a_mobile">
									<div class="pirceBox">
										<p><?php echo $array['title']; ?></p>
										<p class="yellow"><?php echo number_format($array['price'])?>원</p>
									</div>
								</div>
							</label>
							<?php
								$idx++;
							}
							?>
						</div>
					</div>
					<div class="board_conBox">
						<div class="info_leftTxt">
							<p class="p_bt13">상품 금액</p>
						</div>
						<div class="info_contBox">
							<p class="txt-price">0</p>
						</div>
					</div>
					<?php
					$sql = "
						select *
						from {$g5['coupon_member_table']}
						where cm_status = '1'
						and cp_enddate >= '".G5_TIME_YMD."'
						and mb_id ='{$member['mb_id']}'
						and cp_type = '2'
						order by cp_benefit desc
					";
					$result = sql_query($sql, true);
						$rows = sql_num_rows($result);
					?>
					<div class="board_conBox">
						<div class="info_leftTxt">
							<p class="p_bt13">쿠폰 사용</p>
						</div>
						<div class="info_contBox">
							<?php
							if($rows){
								echo '<select name="cm_no" id="cm_no" class="select01">'.PHP_EOL;
								echo '<option value="" data-benefit="0">쿠폰을 선택해주세요</option>'.PHP_EOL;
								while($row=sql_fetch_array($result))
									echo '<option value="'.$row['cm_no'].'" data-benefit="'.$row['cp_benefit'].'">'.$row['cp_benefit'].'% 할인쿠폰</option>'.PHP_EOL;
								echo '</select>'.PHP_EOL;
							}else{
								echo '사용가능한 쿠폰이 없습니다.';
							}
							?>
						</div>
					</div>
					<div class="board_conBox b_paidBox">
						<div class="info_leftTxt">
							<p class="p_bt13">결제 방법</p>
						</div>
						<div class="info_contBox ">
							<ul class="radio_info">
								<?php foreach($g5['payment_array'] as $k=>$v){ ?>
								<li class="radioList">
									<input type="radio" id="r<?php echo $k ?>" name="od_payment" value="<?php echo $v; ?>"<?php echo get_checked($k, 0)?> />
									<label for="r<?php echo $k ?>"><span></span><?php echo $v; ?></label>
								</li>
								<?php }?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- buyBox         -->
			<div class="payBox">
				<div class="cont_topArea">
					<strong class="s_titleTxt">결제하기</strong>
				</div>
				<div class="mypage_boardBox">
					<ul>
						<li>
							<p>상품 금액</p>
							<span class="txt-price">0원</span>
						</li>
						<li class="listSecond">
							<p>할인 적용</p>
							<span class="txt-dicount">0원</span>
						</li>
						<li class="listThird">
							<p>최종 결제 금액</p>
							<strong class="finalPrice txt-amount">0</strong>
						</li>
					</ul>

					<button type="submit" class="btn_orange" id="btn-order">결제하기</button>
				</div>
			</div>
		</div>
		</form>
		<!--// service_boxArea -->


	</div>
	<!--  wrapper -->
</div>
<!--  //container      -->
