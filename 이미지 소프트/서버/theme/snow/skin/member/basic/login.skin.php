<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

$mb_id = get_cookie('ck_save_id');

?>

<script>
	jQuery(function($){
		$(".sns_btnArea").on("click", "a.social_link", function(e){
			e.preventDefault();

			var pop_url = $(this).attr("href");
			var newWin = window.open(
				pop_url,
				"social_sing_on",
				"location=0,status=0,scrollbars=1,width=600,height=500"
			);

			if(!newWin || newWin.closed || typeof newWin.closed=='undefined')
				 alert('브라우저에서 팝업이 차단되어 있습니다. 팝업 활성화 후 다시 시도해 주세요.');

			return false;
		});
	});
</script>

<div id="container">
	<div class="wrapper">
		<div class="join_boxArea">
			<h3>로그인</h3>
			<div class="login_contBox">
				<form action="<?php echo $login_action_url ?>" id="loginBox" method="post">
				<input type="hidden" name="url" value="<?php echo $login_url ?>">
					<div class="inp_box">
						<label for="login_id"></label>
						<input type="text" id="login_id" name="mb_id" value="<?php echo $mb_id; ?>" class="btn_gray_w required" required placeholder="아이디(이메일) 입력">
					</div>
					<div class="inp_box">
						<label for="login_pw"></label>
						<input type="password" id="login_pw" name="mb_password" class="btn_gray_w required" required placeholder="비밀번호 입력">
					</div>
					<div class="list_cont">
						<input type="checkbox" class="inp_cont" value="1" name="save_id" id="nyear"<?php if($mb_id) echo ' checked="checked"'; ?>>
						<label for="nyear">
							<span>아이디 저장</span>
						</label>
					</div>
					<div class="inp_box">
						<button type="submit" class="btn_orange"><span>로그인</span></button>
					</div>
				</form>
				<ul class="btn_joinArea">
					<li><a href="<?php echo G5_BBS_URL; ?>/id_find.php" class="id_find">아이디 찾기</a></li>
					<li><a href="<?php echo G5_BBS_URL; ?>/pw_find.php" class="pw_find">비밀번호 찾기</a></li>
					<li><a href="<?php echo G5_BBS_URL; ?>/register_form.php" class="m_join">회원 가입</a></li>
				</ul>
				<div class="sns_btnArea">
					<ul>
						<?php if( social_service_check('kakao') ) {     //카카오 로그인을 사용한다면 ?>
						<li><a href="<?php echo $self_url;?>?provider=kakao&amp;url=<?php echo $urlencode;?>" class="kakao social_link"><span>카카오톡</span></a></li>
						<?php } ?>
						<?php if( social_service_check('naver') ) {     //카카오 로그인을 사용한다면 ?>
						<li><a href="<?php echo $self_url;?>?provider=naver&amp;url=<?php echo $urlencode;?>" class="naver social_link"><span>네이버</span></a></li>
						<?php } ?>
					</ul>
					<p>SNS로 간편하게 로그인하세요.</p>
				</div>
			</div>
		</div>
	</div>
	<!--  wrapper -->
</div>
