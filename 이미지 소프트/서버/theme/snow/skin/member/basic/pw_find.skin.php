<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
?>
<!-- //모바일 메뉴 로그인 후   -->
<div id="container">
	<div class="wrapper">
		<div class="join_boxArea">
			<div>
				<h3>비밀번호 찾기</h3>
			</div>
			<div class="login_findBox " style="max-height:none">
				<strong class="find_txt"><span>이미지페이지 가입정보를 입력하시면</span> 임시 비밀번호를 발급 받으실 수 있습니다.</strong>
				<form action="#" id="loginBox" onsubmit="return false;">
					<input type="hidden" name="auth" id="auth" value="">
					<div class="inp_box">
						<label for="reg_mb_name"></label>
						<input type="text" name="mb_id" id="reg_mb_id" class="btn_gray_w" placeholder="아이디입력">
					</div>

					<div class="inp_box">
						<label for="reg_mb_name"></label>
						<input type="text" name="mb_name" id="reg_mb_name" class="btn_gray_w" placeholder="이름입력">
					</div>
					<div class="inp_box">
						<div class="left">
							<label for="reg_mb_hp"></label>
							<input type="text" name="mb_hp" id="reg_mb_hp" class="btn_gray_w" placeholder="휴대폰 번호 입력">
						</div>
						<div class="right">
							<button type="button" id="btn_hp_auth" class="btn_orange_short">
								<span>인증</span>
							</button>
						</div>
					</div>
					<div class="inp_box">
						<label for=""></label>
						<input type="text" class="btn_gray_w" name="mb_auth" id="reg_mb_auth" placeholder="인증번호 입력" disabled>
					</div>
					<span class="message"></span>
					<div class="inp_box">
						<button type="button" id="btn_hp_auth_com" class="btn_orange">
							<span>임시비밀번호 발급</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!--  wrapper -->
</div>
<script>
$(function(){
	$("#btn_hp_auth").click(function(){
		$("#reg_mb_auth").val('').prop("disabled", true);
		$("#auth").val();
		$serialize = $("#loginBox").serialize();

		$.post("<?php echo G5_BBS_URL; ?>/ajax.find.pw.send_hp_auth.php", $serialize, function($response){
			if($response.status != 'OK'){

				alert($response.msg);

			}else{
				alert('전송되었습니다.');
				$("#reg_mb_auth").prop("disabled", false)

			}
		}, 'json');
	});


	$("#btn_hp_auth_com").click(function(){

		$("#auth").val('1');
		$serialize = $("#loginBox").serialize();
		$.post("<?php echo G5_BBS_URL; ?>/ajax.find.pw.send_hp_auth.php", $serialize, function($response){
			if($response.status != 'OK'){
				alert($response.msg);
			}else{
				$(".message").text($response.msg);
			}
		}, 'json');
	});
})
</script>
