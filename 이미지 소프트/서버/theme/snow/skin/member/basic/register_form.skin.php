<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
?>
<script>
var is_auth = false;
var is_auth_com = false;
$(function(){
	$("#wrap").addClass('join_wrap');

	$("#email_domain1").change(function(){
		$val = $(this).val();
		if($val=='custom'){
			$(".email-custom").show();
			$(".email-custom").find('input[type="text"]').attr('prop', true);
		}else{
			$(".email-custom").hide();
			$(".email-custom").find('input[type="text"]').attr('prop', false);
		}
	});

	$("#btn_hp_auth").click(function(){
		$(".discord").remove();
		$(".red").removeClass('red');
		$val = $("#reg_mb_hp").val();

		$("#reg_mb_hp_auth").prop('disabled',true).val('');
		is_auth_com = is_auth = false
		if($val==''){
			$("#reg_mb_hp").addClass('red');
			$("#reg_mb_hp").select();
			$(".inp_box.phone").after('<span class="discord">휴대폰번호를 입력해주세요.</span>');
			return false;
		}
		$.get("<?php echo G5_BBS_URL; ?>/ajax.send_hp_auth.php?mb_hp="+$val, function($response){
			if($response.status != 'OK'){
				$("#reg_mb_hp").addClass('red');
				$(".inp_box.phone").after('<span class="discord">'+$response.msg+'</span>');
			}else{
				alert('전송되었습니다.');
				$("#reg_mb_hp_auth").prop('disabled',false).select()
				is_auth = true
			}
		}, 'json');
	});

	$("#btn_hp_auth_com").click(function(){

		if(!is_auth) return;

		is_auth_com = false;
		$(".discord").remove();
		$(".red").removeClass('red');
		$mb_hp = $("#reg_mb_hp").val();
		$mb_hp_auth = $("#reg_mb_hp_auth").val();

		if($mb_hp_auth==''){
			$("#reg_mb_hp_auth").addClass('red');
			$("#reg_mb_hp_auth").select();
			$(".inp_box.number").after('<span class="discord">인증번호를 입력해주세요.</span>');
			return false;
		}
		$.get("<?php echo G5_BBS_URL; ?>/ajax.send_hp_auth_ok.php?mb_hp="+$mb_hp+"&mb_hp_auth="+$mb_hp_auth, function($response){
			if($response.status != 'OK'){
				$("#reg_mb_hp_auth").addClass('red');
				$(".inp_box.number").after('<span class="discord">'+$response.msg+'</span>');
			}else{
				alert('인증되었습니다.');
				is_auth_com = true;

			}
		}, 'json');
	});

});

function fregisterform_submit(f){
	$(".red").removeClass('red');
	$(".discord").remove();

	/// 이메일체크;
	$email = $(".email:eq(0)").val();

	if($("#email_domain1").val()==''){
		$(".inp-email .btn_gray_w").addClass('red');
		$(".inp-email").eq(0).append('<span class="discord">이메일을 입력해주세요.</span>');
		$("#email_domain1").select();
		return false;
	} else if($("#email_domain1").val()=='custom'){
		if($("#email_domain2").val()==''){
			$(".inp-email .btn_gray_w").addClass('red');
			$(".inp-email").eq(1).append('<span class="discord">이메일을 입력해주세요.</span>');
			$("#email_domain2").select();
			return false;
		}else{
			$email += "@"+$("#email_domain2").val();
		}
	} else {
		$email += "@"+$("#email_domain1").val();
	}

	$("#reg_mb_email").val($email);
	if ((f.w.value == "") || (f.w.value == "u" && f.mb_email.defaultValue != f.mb_email.value)) {
        var msg = reg_mb_email_check();
        if (msg) {
            alert(msg);
            f.reg_mb_email.select();
            return false;
        }
    }

	<?php if(!$provider_name) { ?>
    if (f.w.value == "") {
        if (f.mb_password.value.length < 8) {
			$("#reg_mb_password").addClass('red');
			$("#reg_mb_password").after('<span class="discord">비밀번호를 8글자 이상 입력하십시오.</span>');
            f.mb_password.focus();
            return false;
        }
    }

    if (f.mb_password.value != f.mb_password_re.value) {
		$("#reg_mb_password_re").addClass('red');
		$("#reg_mb_password_re").after('<span class="discord">비밀번호가 같지 않습니다.</span>');
        f.mb_password_re.focus();
        return false;
    }
	<?php } ?>
	if (f.w.value=="") {
        if (f.mb_name.value.length < 1) {
			$("#reg_mb_name").addClass('red');
			$("#reg_mb_name").after('<span class="discord">이름을 입력하십시오.</span>');
            f.mb_name.focus();
            return false;
        }
    }

	if(!$("#chk1").is(":checked") || !$("#chk2").is(":checked")  ){
		alert('약관에 동의해주세요');
			return false;
	}
 	return true;

}
</script>

<script src="<?php echo G5_JS_URL ?>/jquery.register_form.js"></script>
<div id="container">
	<div class="wrapper">
		<div class="join_boxArea">
			<h3><?php echo $g5['title'] ?></h3>
			<form id="fregisterform" name="fregisterform" action="<?php echo $register_action_url ?>" onsubmit="return fregisterform_submit(this);" method="post" enctype="multipart/form-data" autocomplete="off">
			<input type="hidden" name="w" value="<?php echo $w ?>">
			<input type="hidden" name="url" value="<?php echo $urlencode ?>">
			<input type="hidden" name="agree" value="<?php echo $agree ?>">
			<input type="hidden" name="agree2" value="<?php echo $agree2 ?>">
			<input type="hidden" name="cert_type" value="<?php echo $member['mb_certify']; ?>">
			<input type="hidden" name="cert_no" value="">
			<?php if (isset($member['mb_sex'])) {  ?><input type="hidden" name="mb_sex" value="<?php echo $member['mb_sex'] ?>"><?php }  ?>
			<?php if (isset($member['mb_nick_date']) && $member['mb_nick_date'] > date("Y-m-d", G5_SERVER_TIME - ($config['cf_nick_modify'] * 86400))) { // 닉네임수정일이 지나지 않았다면  ?>
			<input type="hidden" name="mb_nick_default" value="<?php echo get_text($member['mb_nick']) ?>">
			<input type="hidden" name="mb_nick" value="<?php echo get_text($member['mb_nick']) ?>">
			<?php }  ?>
			<input type="hidden" name="mb_id" id="reg_mb_id" value="<?php echo $user_id; ?>">
			<input type="hidden" name="mb_email" id="reg_mb_email" value="">
			<?php if($provider_name){ ?>
		    <input type="hidden" name="provider" value="<?php echo $provider_name;?>" >
			<?php } ?>
			<div class="join_membersBox">
				<div class="membership_topTxt">

				<?php if($provider_name){ ?>
					<span>혹시 기존 회원이신가요?</span>
				<a href="<?php echo G5_BBS_URL; ?>/login.php" class="login_goBtn" data-remodal-target="modal">기존 계정에 연결하기</a>
				<?php } else{ ?>
				<span>이미 가입하셨다면?</span>
				<a href="<?php echo G5_BBS_URL; ?>/login.php" class="login_goBtn">로그인 바로가기</a>
				<?php } ?>
				</div>



				<div class="sns_btnArea"<?php if($provider_name) echo ' style="padding:0px"'?>>
					<?php

					if( (social_service_check('kakao') || social_service_check('naver')) && !$provider_name) {
						$social_pop_once = false;

						$self_url = G5_BBS_URL."/login.php";

						//새창을 사용한다면
						if( G5_SOCIAL_USE_POPUP ) {
					    	$self_url = G5_SOCIAL_LOGIN_URL.'/popup.php';
						}
					?>
					<ul>
						<?php if( social_service_check('kakao') ) {     //카카오 로그인을 사용한다면 ?>
						<li><a href="<?php echo $self_url;?>?provider=kakao&amp;url=<?php echo $urlencode;?>" class="kakao social_link"><span>카카오톡</span></a></li>
						<?php } ?>
						<?php if( social_service_check('naver') ) {     //카카오 로그인을 사용한다면 ?>
						<li><a href="<?php echo $self_url;?>?provider=naver&amp;url=<?php echo $urlencode;?>" class="naver social_link"><span>네이버</span></a></li>
						<?php } ?>
					</ul>
					<p>SNS로 간편하게 로그인하세요.</p>

					<script>
						jQuery(function($){
							$(".sns_btnArea").on("click", "a.social_link", function(e){
								e.preventDefault();

								var pop_url = $(this).attr("href");
								var newWin = window.open(
									pop_url,
									"social_sing_on",
									"location=0,status=0,scrollbars=1,width=600,height=500"
								);

								if(!newWin || newWin.closed || typeof newWin.closed=='undefined')
									 alert('브라우저에서 팝업이 차단되어 있습니다. 팝업 활성화 후 다시 시도해 주세요.');

								return false;
							});
						});
					</script>

					<?php } ?>
				</div>


				<div class="join_boxArea">

					<div class="inp_box inp-email">
						<div class="left middle">
							<label for="join_email" class="inp_titTxt">이메일</label>
							<input type="text" name="email[]" id="join_email" class="btn_gray_w email" placeholder="이메일">
						</div>
						<span class="center_mark">@</span>
						<div class="right middle">
							<select name="email[]" id="email_domain1" class="btn_gray_w email">
								<option value="">이메일주소</option>
								<option value="custom">직접입력</option>
								<?php
								foreach($g5['email_domain_array'] as $k=>$v)
									echo '<option value="'.$v.'">'.$v.'</option>'.PHP_EOL;
								?>
							</select>
						</div>
					</div>
					<div class="inp_box email-custom inp-email" >
						<input type="text" name="email[]" id="email_domain2" class="btn_gray_w email" placeholder="도메인 직접입력">
					</div>
					<?php if(!$provider_name) { ?>
					<div class="inp_box">
						<label for="reg_mb_password" class="inp_titTxt">비밀번호</label>
						<input type="password" name="mb_password" id="reg_mb_password" class="btn_gray_w" placeholder="비밀번호 입력">
					</div>
					<div class="inp_box">
						<label for="reg_mb_password_re" class="inp_titTxt">비밀번호확인</label>
						<input type="password" name="mb_password_re" id="reg_mb_password_re" class="btn_gray_w " placeholder="8자 이상 입력">
					</div>
					<?php } ?>
					<div class="inp_box">
						<label for="reg_mb_id" class="inp_titTxt">이름</label>
						<input type="text" class="btn_gray_w" id="reg_mb_name" name="mb_name" value="<?php echo $user_name ? $user_name : $user_nick ?>" placeholder="이름 입력">
					</div>
					<div class="inp_box phone">
						<div class="left">
							<label for="reg_mb_hp" class="inp_titTxt">휴대폰</label>
							<input type="tel" id="reg_mb_hp" name="mb_hp" class="btn_gray_w " placeholder="휴대폰 번호 입력">
						</div>
						<div class="right"><button type="button" class="btn_orange_short" id="btn_hp_auth">인증</button></div>
					</div>
					<div class="inp_box number">
						<div class="left">
							<label for="reg_mb_hp_auth" class="inp_titTxt"></label>
							<input type="tel" class="btn_gray_w" name="mb_hp_auth" id="reg_mb_hp_auth" disabled placeholder="인증번호 입력">
						</div>
						<div class="right">
							<button type="button" class="btn_gray_short" id="btn_hp_auth_com">인증</button>
						</div>
					</div>

					<div class="list_cont sns_check">
						<input type="checkbox" class="inp_cont" id="reg_mb_sms" name="mb_sms" value="1">
						<label for="reg_mb_sms">
							<span>SMS 수신동의</span>
						</label>
					</div>
				</div>
				<div class="check_boxArea">
					<strong class="list_titTxt">약관 동의</strong>
					<div class="list_contBox">
						<div class="list_cont allCheck">
							<input type="checkbox" class="inp_cont" id="allCheck">
							<label for="allCheck">
								<span>전체 동의</span>
							</label>
						</div>
						<div class="list_cont">
							<input type="checkbox" class="inp_cont chk" id="chk1" name="chk1" >
							<label for="agree">
								<span>이용약관<em class="red">(필수)</em></span>
							</label>
						</div>
						<div class="list_cont">
							<input type="checkbox" class="inp_cont chk" id="chk2" name="chk2"  >
							<label for="private_info">
								<span>개인정보처리방침<em class="red">(필수)</em></span>
							</label>
						</div>
						<div class="list_cont">
							<input type="checkbox" class="inp_cont chk" id="reg_mb_mailling" name="mb_mailling" value="1">
							<label for="marketing_agree">
								<span>마케팅정보수신동의<em class="l_gray">(선택)</em></span>
							</label>
						</div>

					</div>
				</div>
				<div class="center_area">
					<button type="submit" class="btn_orange">회원가입</button>
				</div>
				</div>
			</form>
		</div>
	</div>
	<!--  wrapper -->
</div>
<!--  //container      -->
<?php if($provider_name){ ?>
<div id="sns-link-pnl" class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
	<button type="button" class="connect-close" data-remodal-action="close">
		<i class="fa fa-close"></i>
		<span class="txt">닫기</span>
	</button>
	<div class="connect-fg">
		<form method="post" action="<?php echo $login_action_url ?>" onsubmit="return social_obj.flogin_submit(this);" autocomplete="off">
		<input type="hidden" id="url" name="url" value="<?php echo $login_url ?>">
		<input type="hidden" id="provider" name="provider" value="<?php echo $provider_name ?>">
		<input type="hidden" id="action" name="action" value="social_account_linking">

		<div class="connect-title">기존 계정에 연결하기</div>

		<div class="connect-desc">
			기존 아이디에 SNS 아이디를 연결합니다.<br>
			이 후 SNS 아이디로 로그인 하시면 기존 아이디로 로그인 할 수 있습니다.
		</div>

		<div id="login_fs">
			<label for="login_id" class="login_id">아이디<strong class="sound_only"> 필수</strong></label>
			<span class="lg_id"><input type="text" name="mb_id" id="login_id" class="frm_input required btn_gray_w" size="20" maxLength="20" ></span>
			<label for="login_pw" class="login_pw">비밀번호<strong class="sound_only"> 필수</strong></label>
			<span class="lg_pw"><input type="password" name="mb_password" id="login_pw" class="frm_input required btn_gray_w" size="20" maxLength="20"></span>
			<br>
			<input type="submit" value="연결하기" class="login_submit btn_submit">
		</div>

		</form>
	</div>
</div>
<?php } ?>
