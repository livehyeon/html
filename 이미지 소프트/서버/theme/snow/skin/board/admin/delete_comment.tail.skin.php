<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

delete_cache_latest($bo_table);

run_event('bbs_delete_comment', $comment_id, $board);

goto_url(short_url_clean(G5_ADMIN_URL.'/bbs_admin/board.php?bo_table='.$bo_table.'&amp;wr_id='.$write['wr_parent'].'&amp;page='.$page. $qstr));
 
?>
