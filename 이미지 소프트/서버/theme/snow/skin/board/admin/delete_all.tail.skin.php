<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

delete_cache_latest($bo_table);

run_event('bbs_delete_all', $tmp_array, $board);

goto_url(short_url_clean(G5_ADMIN_URL.'/bbs_admin/board.php?bo_table='.$bo_table.'&amp;page='.$page.$qstr));
?>
