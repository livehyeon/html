<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
delete_cache_latest($bo_table);

$redirect_url = short_url_clean(G5_ADMIN_URL.'/bbs_admin/board.php?bo_table='.$bo_table.'&amp;wr_id='.$wr['wr_parent'].'&amp;'.$qstr.'&amp;#c_'.$comment_id);

run_event('comment_update_after', $board, $wr_id, $w, $qstr, $redirect_url, $comment_id, $reply_array);

goto_url($redirect_url);
?>
