<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
?>
<script>
$(function(){
	$("#wrap").addClass('notice_wrap');
	$('.btn_black_file').click(function(e){
		e.preventDefault();
		$('#service_fileBtn').click();
	});

	$('#service_fileBtn').change(function(){
		$file = $(this).val();
		$("#txt-file").val($file);
	});

});


    function fwrite_submit(f)
    {
        <?php echo $editor_js; // 에디터 사용시 자바스크립트에서 내용을 폼필드로 넣어주며 내용이 입력되었는지 검사함   ?>

        var subject = "";
        var content = "";
        $.ajax({
            url: g5_bbs_url+"/ajax.filter.php",
            type: "POST",
            data: {
                "subject": f.wr_subject.value,
                "content": f.wr_content.value
            },
            dataType: "json",
            async: false,
            cache: false,
            success: function(data, textStatus) {
                subject = data.subject;
                content = data.content;
            }
        });

        if (subject) {
            alert("제목에 금지단어('"+subject+"')가 포함되어있습니다");
            f.wr_subject.focus();
            return false;
        }

        if (content) {
            alert("내용에 금지단어('"+content+"')가 포함되어있습니다");
            if (typeof(ed_wr_content) != "undefined")
                ed_wr_content.returnFalse();
            else
                f.wr_content.focus();
            return false;
        }

        if (document.getElementById("char_count")) {
            if (char_min > 0 || char_max > 0) {
                var cnt = parseInt(check_byte("wr_content", "char_count"));
                if (char_min > 0 && char_min > cnt) {
                    alert("내용은 "+char_min+"글자 이상 쓰셔야 합니다.");
                    return false;
                }
                else if (char_max > 0 && char_max < cnt) {
                    alert("내용은 "+char_max+"글자 이하로 쓰셔야 합니다.");
                    return false;
                }
            }
        }

        <?php echo $captcha_js; // 캡챠 사용시 자바스크립트에서 입력된 캡챠를 검사함  ?>

        document.getElementById("btn_submit").disabled = "disabled";

        return true;
    }
</script>

<div id="container">
	<div class="wrapper">
		<div class="service_boxArea ">
			<h2>아이디어 톡톡</h2>
			<div class="service_boardBox note">
				<!-- 게시물 작성/수정 시작 { -->
			    <form name="fwrite" id="fwrite" action="<?php echo $action_url ?>" onsubmit="return fwrite_submit(this);" method="post" enctype="multipart/form-data" autocomplete="off" style="width:<?php echo $width; ?>">
			    <input type="hidden" name="uid" value="<?php echo get_uniqid(); ?>">
			    <input type="hidden" name="w" value="<?php echo $w ?>">
			    <input type="hidden" name="bo_table" value="<?php echo $bo_table ?>">
			    <input type="hidden" name="wr_id" value="<?php echo $wr_id ?>">
			    <input type="hidden" name="sca" value="<?php echo $sca ?>">
			    <input type="hidden" name="sfl" value="<?php echo $sfl ?>">
			    <input type="hidden" name="stx" value="<?php echo $stx ?>">
			    <input type="hidden" name="spt" value="<?php echo $spt ?>">
			    <input type="hidden" name="sst" value="<?php echo $sst ?>">
			    <input type="hidden" name="sod" value="<?php echo $sod ?>">
			    <input type="hidden" name="page" value="<?php echo $page ?>">
			    <?php
			    $option = '';
			    $option_hidden = '';
			    if ($is_notice || $is_html || $is_secret || $is_mail) {
			        $option = '';
			        if ($is_notice) {
			            $option .= PHP_EOL.'<li class="chk_box"><input type="checkbox" id="notice" name="notice"  class="selec_chk" value="1" '.$notice_checked.'>'.PHP_EOL.'<label for="notice"><span></span>공지</label></li>';
			        }
			        if ($is_html) {
			            if ($is_dhtml_editor) {
			                $option_hidden .= '<input type="hidden" value="html1" name="html">';
			            } else {
			                $option .= PHP_EOL.'<li class="chk_box"><input type="checkbox" id="html" name="html" onclick="html_auto_br(this);" class="selec_chk" value="'.$html_value.'" '.$html_checked.'>'.PHP_EOL.'<label for="html"><span></span>html</label></li>';
			            }
			        }
			        if ($is_secret) {
			            if ($is_admin || $is_secret==1) {
			                $option .= PHP_EOL.'<li class="chk_box"><input type="checkbox" id="secret" name="secret"  class="selec_chk" value="secret" '.$secret_checked.'>'.PHP_EOL.'<label for="secret"><span></span>비밀글</label></li>';
			            } else {
			                $option_hidden .= '<input type="hidden" name="secret" value="secret">';
			            }
			        }
			        if ($is_mail) {
			            $option .= PHP_EOL.'<li class="chk_box"><input type="checkbox" id="mail" name="mail"  class="selec_chk" value="mail" '.$recv_email_checked.'>'.PHP_EOL.'<label for="mail"><span></span>답변메일받기</label></li>';
			        }
			    }
			    echo $option_hidden;
			    ?>
				<div class="board_txtBox">
					<div class="notice_leftTxt">
						<p>제목</p>
					</div>
					<div class="inp_box">
						<label for=""></label>
						<input type="text" name="wr_subject" value="<?php echo $subject ?>" id="wr_subject" required class="btn_gray_w" placeholder="제목을 입력해주세요">
					</div>

				</div>
				<div class="board_center textareaBox">
					<div class="notice_leftTxt">
						<p>내용</p>
					</div>
					<div class="inp_box">
						<textarea name="wr_content" id="wr_content" cols="30" rows="10" placeholder="내용을 입력해주세요."><?php echo $write['wr_content']?></textarea>
					</div>
				</div>
				<div class="board_txtBox ">
					<div class="notice_leftTxt">
						<p>첨부파일</p>
					</div>
					<div class="inp_box file">
						<input type="text" name="file" id="txt-file" class="btn_gray_w " disabled placeholder="용량 <?php echo $upload_max_filesize ?> 이하만 업로드 가능">

					</div>
					<div class="fileBox">
						<input type="file" name="bf_file[]"  title="" id="service_fileBtn">
						<a href="#" class="btn_black_file">파일선택</a>
						<?php
						$i = 0;
						if($w == 'u' && $file[$i]['file']) { ?>
				        <div class="file_del" style="margin:-10px 0px 20px">
				            <input type="checkbox" id="bf_file_del<?php echo $i ?>" name="bf_file_del[<?php echo $i;  ?>]" value="1"> <label for="bf_file_del<?php echo $i ?>"><?php echo $file[$i]['source'].'('.$file[$i]['size'].')';  ?> 파일 삭제</label>
				        </div>
				        <?php } ?>
					</div>
				</div>
				<div class="center_area">
					<a href="<?php echo get_pretty_url($bo_table); ?>" class="btn_gray_middle">취소</a>
					<button type="submit" id="btn_submit" accesskey="s" class="btn_orange_middle">등록</button>
				</div>
			</div>
		</div>
		<!--// service_boxArea -->


	</div>
	<!--  wrapper -->
</div>
<!--  //container      -->
