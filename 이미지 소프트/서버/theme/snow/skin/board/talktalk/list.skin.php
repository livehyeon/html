<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
$write_pages = get_paging_new(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, get_pretty_url($bo_table, '', $qstr.'&amp;page='));
?>
<script>
$(function(){
	$("#wrap").addClass('notice_wrap');
})
</script>

<div id="container">
	<div class="wrapper">
		<div class="service_boxArea">
			<h2>아이디어 톡톡</h2>
			<div class="idea_bannerArea pc">
				<div class="slide_backImg">
					<a href="<?php echo $write_href ?>" class="bannerArea">
						<img src="<?php echo G5_RESOURCE_URL; ?>/images/layout/idea_banner.jpg" alt="">
					</a>
				</div>
			</div>
			<div class="idea_bannerArea mobile">
				<div class="slide_backImg">
					<a href="<?php echo $write_href ?>" class="bannerArea">
						<img src="<?php echo G5_RESOURCE_URL; ?>/images/layout/idea_banner.jpg" alt="">
					</a>
				</div>
			</div>
			<div class="service_tblBox">
				<div class="cont_topArea">
					<p class="s_pTxt">총 <em class="yellow"><?php echo $total_count ?></em>개의 소식이 있습니다. </p>
					<div class="right">

						<form name="fsearch" method="get">
			            <input type="hidden" name="bo_table" value="<?php echo $bo_table ?>">
			            <input type="hidden" name="sca" value="<?php echo $sca ?>">
			            <input type="hidden" name="sop" value="and">
						<select name="sfl" id="sfl" class="btn_gray_w select01">
							<option value="wr_subject"<?php echo get_selected($sfl, 'wr_subject', true); ?>>제목</option>
  					      	<option value="wr_content"<?php echo get_selected($sfl, 'wr_content', true); ?>>내용</option>
						</select>
						<div class="searchArea">
							<fieldset>
								<legend>검색어 입력폼</legend>
								<div class="search_box">
									<input type="text" class="search_txt" id="stx" name="stx" value="<?php echo stripslashes($stx) ?>" required placeholder="검색어 입력">
									<button id="img_search" type="submit">
										<span class="s_txt">검색</span>
									</button>
								</div>
							</fieldset>
						</div>
						</form>

					</div>
				</div>
				<table class="tableBox">
					<caption>공지사항</caption>
					<colgroup>
						<col style="width:*;">
						<col style="width:*">
						<col style="width:*">
					</colgroup>
					<tbody>
						<?php
						for ($i=0; $i<count($list); $i++) {
							$class = $list[$i]['is_notice'] ? 'class="bg_noticeTitle"' : '';

							$mb_admin = is_admin($list[$i]['mb_id']);
							if (!$is_admin && !$mb_admin) {
	    						$list[$i]['name'] = preg_replace('/(?<=.{1})./u','*',$list[$i]['wr_name']);
							}
						?>
						<tr<?php echo $class; ?>>
							<td class="td_txt">
								<a href="<?php echo $list[$i]['href'] ?>"><?php if($list[$i]['icon_secret']) echo '<strong class="tok_tag">잠금</strong>'; ?><?php echo $list[$i]['subject'] ?></a>
								<?php if ($list[$i]['icon_new']){ ?><span><img src="<?php echo G5_RESOURCE_URL; ?>/images/contents/ico_n.png" alt=""></span><?php } ?>
								<?php if ($list[$i]['icon_file']){ ?><span><img src="<?php echo G5_RESOURCE_URL; ?>/images/contents/ico_file.png" alt=""></span><?php } ?>
							</td>
							<td class="td_name"><?php echo $list[$i]['name'] ; ?></td>
							<td class="td_date"><?php echo date("Y.m.d", strtotime($list[$i]['wr_datetime']))?></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
		<!--// service_boxArea -->
		<?php echo $write_pages; ?>

		<?php if ($write_href) { ?>
		<div class="btn_bottom">
			<a href="<?php echo $write_href ?>" type="text" class="btn_orange_middle">작성하기</a>
		</div>
		<?php } ?>
		<!-- //service_banner -->
	</div>
	<!--  wrapper -->
</div>
<!--  //container      -->
