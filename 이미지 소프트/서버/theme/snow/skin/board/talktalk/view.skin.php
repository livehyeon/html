<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가
include_once(G5_LIB_PATH.'/thumbnail.lib.php');
$arr_notice = explode(',', trim($board['bo_notice']));

$is_notice = in_array($arr_notice, $wr_id);



$is_new = ($board['bo_new'] && $view['wr_datetime'] >= date("Y-m-d H:i:s", G5_SERVER_TIME - ($board['bo_new'] * 3600))) ? true : false;
?>

<script src="<?php echo G5_JS_URL; ?>/viewimageresize.js"></script>

<div id="container">
	<div class="wrapper">
		<div class="service_boxArea detail">
			<h2>아이디어 톡톡</h2>
			<div class="service_boardBox">
				<div class="board_txtBox">
					<div class="board_txt">
						<div class="notice_txt">

							<span class="titText" style="padding:0px;"><?php echo $view['wr_subject']; ?>
							<?php if($is_new){ ?><em><img src="<?php echo G5_RESOURCE_URL; ?>/images/contents/ico_n.png" alt=""></em></span><?php } ?></div>
						</div>
						<div class="board_date pc"><?php echo date("Y.m.d", strtotime($view['wr_datetime'])); ?></div>
					</div>
					<div class="board_center">
						<?php echo get_view_thumbnail($view['content']); ?>
						<?php
				        // 가변 파일
				        for ($i=0; $i<count($view['file']); $i++) {
				            if (isset($view['file'][$i]['source']) && $view['file'][$i]['source'] && !$view['file'][$i]['view']) {
				         ?>
						<div class="board_downBox">
							<i><img src="<?php echo G5_RESOURCE_URL; ?>/images/contents/ico_file.png" alt=""></i>
							<span><?php echo $view['file'][$i]['source'] ?></span>
							<em class="downVolume"><?php echo $view['file'][$i]['size'] ?></em>
							<a href="<?php echo $view['file'][$i]['href'];  ?>" class="file_down" target="_blank"><i></i></a>
						</div>
						<?php
							}
						}
						?>
					</div>
					<?php if ($prev_href || $next_href) { ?>
					<div class="board_prevnext">
						 <?php if ($prev_href) { ?>
						<div class="prev"><span>이전글</span>
							<a href="<?php echo $prev_href ?>"><?php echo $prev_wr_subject;?></a>
						</div>
						<?php } ?>
						<?php if ($next_href) { ?>
						<div class="next"><span>다음글</span>
							<a href="<?php echo $next_href ?>"><?php echo $next_wr_subject;?></a>
						</div>
						<?php } ?>
					</div>
					<?php } ?>
					<div class="center_area">
						<a href="<?php echo $list_href ?>" class="btn_orange_middle">목록</a>
					</div>
				</div>
			</div>
			<!--// service_boxArea -->
		</div>
		<!--  wrapper -->
	</div>
	<!--  //container      -->
