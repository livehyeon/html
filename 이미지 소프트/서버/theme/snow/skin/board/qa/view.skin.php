<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가
include_once(G5_LIB_PATH.'/thumbnail.lib.php');

?>
<script>
$("#wrap").addClass('notice_wrap')
</script>

<div id="container">
	<div class="wrapper">
		<div class="service_boxArea detail ">
			<h2>1:1문의</h2>
			<!-- 21.03.02 jhj 수정 oneQna클래스 추가 -->
			<div class="service_boardBox oneQna">
				<div class="board_txtBox">
					<div class="board_txt">
						<div class="notice_txt">
							<strong class="notice_tag<?php if($view['wr_comment']==0) echo ' gray'  ?>"><?php echo $view['wr_comment']==1?'완료':'대기'?></strong>
							<span class="titText"><?php echo $view['wr_subject']?></span>
						</div>
					</div>
					<div class="board_date pc"><?php echo date("Y.m.d", strtotime($view['wr_datetime'])); ?></div>
				</div>
				<div class="board_center"><?php echo get_view_thumbnail($view['content']); ?></div>

				<?php
			    // 코멘트 입출력
			    include_once(G5_BBS_PATH.'/view_comment.php');
				?>
				<div class="center_area">
					<a href="<?php echo $list_href; ?>" class="btn_orange_middle">목록</a>
				</div>
			</div>
		</div>
		<!--// service_boxArea -->
	</div>
	<!--  wrapper -->
</div>
<!--  //container      -->
