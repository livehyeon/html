<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
$write_pages = get_paging_new(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, get_pretty_url($bo_table, '', $qstr.'&amp;page='));
?>
<div id="container">
	<div class="wrapper">
		<div class="service_boxArea">
			<h2>1대 1문의</h2>
			<?php
			$on = 'qa';
			include_once("{$service_skin_path}/top.skin.php");
			?>
			<div class="service_tblBox">
				<div class="cont_topArea">
					<p class="s_pTxt">총 <em class="yellow"><?php echo $total_count; ?></em>개의 소식이 있습니다. </p>
					<div class="right">
						<form name="fsearch" method="get">
			            <input type="hidden" name="bo_table" value="<?php echo $bo_table ?>">
			            <input type="hidden" name="sca" value="<?php echo $sca ?>">
			            <input type="hidden" name="sop" value="and">
						<select name="sfl" id="sfl" class="btn_gray_w select01">
							<option value="wr_subject"<?php echo get_selected($sfl, 'wr_subject', true); ?>>제목</option>
  					      	<option value="wr_content"<?php echo get_selected($sfl, 'wr_content', true); ?>>내용</option>
						</select>
						<div class="searchArea">
							<fieldset>
								<legend>검색어 입력폼</legend>
								<div class="search_box">
									<input type="text" class="search_txt" id="stx" name="stx" value="<?php echo stripslashes($stx) ?>" required placeholder="검색어 입력">
									<button id="img_search" type="submit">
										<span class="s_txt">검색</span>
									</button>
								</div>
							</fieldset>
						</div>
						</form>
					</div>
				</div>
				<table class="tableBox">
					<caption>1대1문의</caption>
					<colgroup class="pc">
						<col style="width:*;">
						<col style="width:*;">
					</colgroup>
					<tbody>
						<?php
						for ($i=0; $i<count($list); $i++) {
							$class = $list[$i]['is_notice'] ? 'class="bg_noticeTitle"' : '';
							if($member['mb_id'] != $list[$i]['mb_id'] && !$is_admin){
								$list[$i]['href'] = '#" onclick="alert(\'권한이 없습니다.\');return false;"';
							}
						?>
						<tr<?php echo $class; ?>>
							<!--공지사항일때 bg_noticeTitle 클래스 추가 -->
							<td class="td_txt">
								<a href="<?php echo $list[$i]['href'] ?>">
								<?php
									if($class) {
										echo '<strong class="notice_tag">공지</strong>';
									}else{
										if($list[$i]['comment_cnt'])
											echo '<strong class="notice_tag fw400">완료</strong>';
										else
											echo '<strong class="notice_tag gray fw400">대기</strong>';
									}
									echo $list[$i]['subject'];
								?>
								</a>
							</td>
							<td class="td_date"> <?php echo date("Y.m.d", strtotime($list[$i]['wr_datetime']))?></td>
						</tr>
						<?php } ?>
					 	<?php if (count($list) == 0) { echo '<tr><td colspan="2" class="empty">게시물이 없습니다.</td></tr>'; } ?>
					</tbody>
				</table>
			</div>
		</div>
		<!--// service_boxArea -->
		<?php echo $write_pages ?>
		<!-- //paging -->
		<?php if ($write_href) { ?>
		<div class="btn_bottom">
			<a href="<?php echo $write_href ?>"   class="btn_orange_middle">1:1 문의하기</a>
		</div>
		<?php } ?>

		<!-- //service_banner -->
	</div>
	<!--  wrapper -->
</div>
<!--  //container      -->
