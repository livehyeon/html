<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
?>
<div id="container">
	 <div class="wrapper">
	   <div class="method_boxArea">
		 <div class="inner_bgBox child">
		   <div class="bg_txtBox">

			 <strong> <em></em><span>아이들과 함께 할 수업과 </span>딱 맞는 이미지를 찾으시나요?</strong>
			 <p>
			   <span>이미지페이지에서 아이들 수업에 함께 할 이미지들을 </span>
			   찾아보세요. 다양한 도안들이 준비 되어 있습니다.
			 </p>
		   </div>
		 </div>
		 <div class="inner_bgBox design a_pc">
		   <div class="bg_txtBox f_right">

			 <strong><em></em><span>환경판 꾸미기, 가정통신문에 </span>넣을 다양한 도안이 필요하신가요?</strong>
			 <p>
			   <span>수업에 딱 맞고 꾸미기용으로 사용하기 좋은 다양한 이미지들이 </span>
			   준비 되어있습니다. 퀄리티 있는 이미지로 수업의 질을 높아보세요!
			 </p>
		   </div>
		 </div>
		 <div class="inner_bgBox design  a_mobile">
		   <div class="bg_txtBox f_right">

			 <strong><em></em><span>환경판 꾸미기, 가정통신문에 넣을</span> 다양한 도안이 필요하신가요?</strong>
			 <p>
			   <span>수업에 딱 맞고 꾸미기용으로 사용하기 좋은 다양한</span>
			   <span> 이미지들이 준비 되어있습니다. 퀄리티 있는 이미지로</span>
			   수업의 질을 높아보세요!
			 </p>
		   </div>
		 </div>
		 <div class="inner_bgBox together">
		   <div class="bg_txtBox">

			 <strong> <em></em><span>그렇다면,</span> 이미지페이지와 함께하세요!</strong>
			 <p>
			   귀엽고 아기자기한 다양하고 많은 이미지가 함께합니다.
			 </p>
		   </div>
		 </div>
		 <div class="inner_bgBox infinite a_pc">
		   <div class="bg_txtBox f_right">

			 <strong><em></em><span>유아교육에서 필요한 각종 도안과</span> 이미지를 무제한으로 제공합니다!</strong>
			 <p>
			   <span>무제한으로 다운 받으실 수 있으니 컴퓨터 용량 차지하지 말고</span> 쓰고 지우고 필요하면 또 받으세요.
			 </p>
		   </div>
		 </div>
		 <div class="inner_bgBox infinite a_mobile">
		   <div class="bg_txtBox f_right">

			 <strong><em></em><span>유아교육에서 필요한 각종 도안과</span> 이미지를 무제한으로 제공합니다!</strong>
			 <p>
			   <span>무제한으로 다운 받으실 수 있으니 컴퓨터 용량 차지하지</span> 말고
			   쓰고 지우고 필요하면 또 받으세요.
			 </p>
		   </div>
		 </div>
		 <div class="inner_bgBox anytime a_pc">
		   <div class="bg_txtBox">

			 <strong><em></em><span>1년 내내! 언제든지! 수업에</span> 필요한 이미지를 다운 받으세요.</strong>
			 <p>
			   1년 내내 유아 교육에서 필요한 각종 도안들을 언제든지 다운 받으세요!
			 </p>
		   </div>
		 </div>
		 <div class="inner_bgBox anytime a_mobile">
		   <div class="bg_txtBox">

			 <strong><em></em><span>1년 내내! 언제든지! 수업에</span> 필요한 이미지를 다운 받으세요.</strong>
			 <p>
			   <span>1년 내내 유아 교육에서 필요한 각종 도안들을 </span> 언제든지 다운 받으세요!
			 </p>
		   </div>
		 </div>
		 <div class="inner_bgBox method_order">
		   <div class="bg_txtBox">

			 <strong><em></em>이용방법도 매우 간단해요</strong>
			 <p>
			   <span>이미지페이지에서 간단한 이용 방법으로 도안을 받아</span> 수업에서 언제든지 활용하세요!
			 </p>
		   </div>
		   <ul>
			 <li>
			   <div>
				 <img src="<?php echo G5_RESOURCE_URL; ?>/images/layout/method_bg06_1.jpg" alt="" class="a_pc">
				 <img src="<?php echo G5_RESOURCE_URL; ?>/images/layout/method_bg06_1_m.jpg" alt="" class="a_mobile">
			   </div>
			 </li>
			 <li>
			   <div>
				 <img src="<?php echo G5_RESOURCE_URL; ?>/images/layout/method_bg06_2.jpg" alt="" class="a_pc">
				 <img src="<?php echo G5_RESOURCE_URL; ?>/images/layout/method_bg06_2_m.jpg" alt="" class="a_mobile">
			   </div>
			 </li>
			 <li>
			   <div>
				 <img src="<?php echo G5_RESOURCE_URL; ?>/images/layout/method_bg06_3.jpg" alt="" class="a_pc">
				 <img src="<?php echo G5_RESOURCE_URL; ?>/images/layout/method_bg06_3_m.jpg" alt="" class="a_mobile">
			   </div>
			 </li>
			 <li>
			   <div>
				 <img src="<?php echo G5_RESOURCE_URL; ?>/images/layout/method_bg06_4.jpg" alt="" class="a_pc">
				 <img src="<?php echo G5_RESOURCE_URL; ?>/images/layout/method_bg06_4_m.jpg" alt="" class="a_mobile">
			   </div>
			 </li>
		   </ul>
		 </div>

	   </div>
	 </div>
	 <!--  wrapper -->
   </div>
   <!--  //container      -->
