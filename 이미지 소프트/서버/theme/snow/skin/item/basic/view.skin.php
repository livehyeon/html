<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가


?>

<script src="//developers.kakao.com/sdk/js/kakao.min.js"></script>
<script type='text/javascript'>
  //<![CDATA[
    // // 사용할 앱의 JavaScript 키를 설정해 주세요.
    Kakao.init('<?php echo $config['cf_kakao_js_apikey']; ?>');
    // // 카카오링크 버튼을 생성합니다. 처음 한번만 호출하면 됩니다.
    function sendLink() {
      Kakao.Link.sendDefault({
    	objectType: 'feed',
    	content: {
    	  title: '<?php echo $it['it_name'] ?>',
    	  description: '<?php echo implode(",", $it['it_tag']) ?>',
    	  imageUrl: '<?php echo G5_DATA_ITEM_URL."/{$it['it_thumb']}" ?>',
    	  link: {
    		mobileWebUrl: document.URL,
    		webUrl: document.URL
    	  }
    	},
      });
    }
  //]]>
</script>


<script>
$(function(){
	$(".linkCon a.saveBtn").click(function(){
		$this = $(this);
		$.get("<?php echo G5_ITEM_URL; ?>/ajax.zzim.php?it_id=<?php echo $it_id; ?>", function($response){
			if($response.msg){
				alert($response.msg);
				$this.removeClass('on');
				return false;
			}
			if($response.status=='1'){
				$this.addClass('on');
			}else{
				$this.removeClass('on');
			}
		}, 'json');

		return false;
	});
})
</script>
	<div id="container">
		<div class="wrapper">
			<div class="detail_titleArea">
				<div class="title_boxArea">
					<div class="title_imgBox slide_backImg" style="background-image:url(<?php echo G5_DATA_ITEM_URL."/{$it['it_thumb']}"; ?>)">
						<a href="#"><img src="<?php echo G5_DATA_ITEM_URL."/{$it['it_thumb']}"; ?>" alt=""></a>
					</div>
					<div class="title_contBox">
						<div class="txt_boxArea">
							<?if(it_new($it)){ ?>
							<div class="n_txtBox">
								<em class="n_txt">NEW</em>
								<!--em class="n_txt bl">NEW</em-->
							</div>
						<?php } ?>
							<strong class="titleText"><?php echo $it['it_name']?></strong>
							<!--  tag_list pc-->
							<?php for($i=0;$i<count($it['it_tag']);$i++){ ?>
							<ul class="tag_list t_pc">
								<?php for($i=0;$i<count($it['it_tag']);$i++){?>
								<li><a href="<?php echo G5_URL; ?>?tag=<?php echo urlencode($it['it_tag'][$i]); ?>">#<?php echo $it['it_tag'][$i]?></a></li>
								<?php } ?>
							</ul>
							<div class=" tab_listBox swiper-container03 t_mobile">
								<ul class="tag_list swiper-wrapper">
									<?php for($i=0;$i<count($it['it_tag']);$i++){?>
									<li class="swiper-slide"><a href="<?php echo G5_URL."?tag=".urlencode($it['it_tag'][$i]); ?>">#<?php echo $it['it_tag'][$i]; ?></a></li>
									<?php } ?>
								</ul>
							</div>
							<?php } ?>
						</div>
						<div class="btn_boxArea">
							<div class="button_conBox linkCon">
								<a href="javascript:void(0);" class="shareBtn"></a>
								<div class="sns_boxArea">
									<div class="box">
										<ul class="snsBox">
											<li><a href="#" onclick="javascript:sendLink();return false" class="sns_kakao">카카오톡</a></li>
											<li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(G5_ITEM_URL."/view.php?it_id={$it_id}"); ?>" target="_blank" class="sns_facebook">페이스북</a></li>
											<li><a href="https://twitter.com/intent/tweet?url=<?php echo urlencode(G5_ITEM_URL."/view.php?it_id={$it_id}")?>&text=<?php echo urlencode($it['it_name'])?>"  target="_blank" class="sns_twitter">트위터</a></li>
										</ul>
										<a href="javascript:void(0);" class="sns_closeBtn">1</a>
									</div>
								</div>
							</div>
							<div class="button_conBox linkCon">
								<a hgref="#" class="saveBtn<?php echo $zzim; ?>"></a>
							</div>
							<div class="button_conBox downCon">
								<a href="<?php echo G5_ITEM_URL; ?>/download.php?it_no=<?php echo $it['it_no']?>" class="downBtn">
									<i></i><span>다운로드</span>
								</a>
							</div>
						</div>
						<div class="notice_boxArea">
							<p>이미지를 무단으로 사용하는 경우 저작권법 등에 따라 처벌 될 수 있습니다. 콘텐츠 사용 전 콘텐츠 사용 안내를 꼭 확인해 주세요.</p>
							<strong><i></i>콘텐츠 사용안내</strong>
						</div>
					</div>
				</div>
				<div class="edit_boxArea">
					<?php echo $it['it_content']; ?>
					<!--img src="<?php echo G5_RESOURCE_URL; ?>/images/contents/img_editcont.jpg" alt=""-->
				</div>
			</div>
			<!-- 디테일 배너  -->
			<div class="detail_bannerArea">
				<div class="banner_boxArea swiper-container01 pc">
					<h3>이런 상품도 있어요!</h3>
					<ul class="swiper-wrapper pc">
						<li class="img_list swiper-slide">
							<ul>
							<?php
							$sql = "
								select a.it_id, a.it_name, a.it_thumb, a.it_tag
								from {$g5['item_table']} a, {$g5['item_related_table']} b
								where a.it_id = b.it_id2
								and b.it_id1 = '{$it_id}'
								order by ir_no asc
							";

							$result = sql_query($sql, true);
							$related = array();
							while($row=sql_fetch_array($result)){

								$row['it_tag'] = explode("|", $row['it_tag'] );

								for($i=0;$i<count($row['it_tag']);$i++)
									$row['it_tag'][$i] = "#{$row['it_tag'][$i]}";

								$row['it_tag'] = implode(" ", $row['it_tag']);
								$related[] = $row;
							?>
								<li>
									<a href="<?php echo G5_ITEM_URL; ?>/view.php?it_id=<?php echo $row['it_id']?>">
										<div class="recom_imgBox">
											<img src="<?php echo G5_DATA_ITEM_URL.'/'.$row['it_thumb']; ?>" alt="<?php echo $row['it_name']?>">
										</div>
										<strong class="list_txt"><?php echo $row['it_name']?></strong>
										<span class="tag_txt"><?php echo $row['it_tag']; ?></span>
									</a>
								</li>
							<?php
							}
							?>
							</ul>
						</li>
					</ul>
					<div class="arrow_btnArea">
						<div class="swiper-button-prev"></div>
						<div class="swiper-button-next"></div>
						<!-- //스와이퍼 네비게이션 버튼-->
					</div>
				</div>
				<!-- 상세 모바일 배너슬라이드 -->
				<div class="banner_boxArea swiper-container02 mobile">
					<h3>이런 상품도 있어요!</h3>
					<ul class="swiper-wrapper">
						<?php for($i=0;$i<count($related);$i++){ ?>
						<li class="img_list swiper-slide">
							<a href="<?php echo G5_ITEM_URL; ?>/view.php?it_id=<?php echo $row['it_id']?>">
								<div class="recom_imgBox">
									<img src="<?php echo G5_DATA_ITEM_URL.'/'.$row['it_thumb']; ?>" alt="<?php echo $row['it_name']?>">
								</div>
								<div class="btm_txtBox">
									<strong class="list_txt"><?php echo $row['it_name']?></strong>
									<span class="tag_txt"><?php echo $row['it_tag']?></span>
								</div>
							</a>
						</li>
						<?php } ?>
					</ul>
					<div class="arrow_btnArea">
						<div class="swiper-button-prev"></div>
						<div class="swiper-button-next"></div>
						<!-- //스와이퍼 네비게이션 버튼-->
					</div>
				</div>
				<div class="customer_boxArea">
					<a href="<?php echo G5_URL; ?>/talktalk" class="customer_appeal">도안요청하기</a>
					<a href="<?php echo G5_ITEM_URL; ?>/guide.php" class="customer_service">고객센터</a>
				</div>

			</div>
			<!-- //상세 모바일 배너슬라이드 -->
		</div>
		<!--  wrapper -->
	</div>
	<!--  //container      -->

	<script>
	  /*sns */
	  $('.saveBtn').click(function() {
		$(this).toggleClass('on');

	  })
	  /*sns 팝업*/
	  $('.shareBtn').click(function() {
		$('.sns_boxArea').addClass('on');
	  })
	  $('.sns_closeBtn').click(function() {
		$('.sns_boxArea').removeClass('on');
	  })



	  /*pc 사이드 배너 슬라이드*/
	  var swiper = new Swiper('.swiper-container01', {
		slidesPerView: 1,
		spaceBetween: 50,
		//            loop: true,
		navigation: {
		  nextEl: '.swiper-button-next',
		  prevEl: '.swiper-button-prev',
		},
		breakpoints: {
		  600: {
			slidesPerView: 2.5,
			spaceBetween: 15,
		  },
		  800: {
			slidesPerView: 3.5,
			spaceBetween: 15,
		  },
		  1200: {
			slidesPerView: 4.5,
			spaceBetween: 15,
		  },
		}
	  });
	  /*mobile 사이드 배너 슬라이드*/
	  var swiper02 = new Swiper('.swiper-container02 ', {
		slidesPerView: 4.5,
		spaceBetween: 15,
		//            loop: true,
		loopedSlides: 5,
		autoplay: false,
		navigation: {
		  nextEl: '.swiper-button-next',
		  prevEl: '.swiper-button-prev',
		},
		breakpoints: {
		  600: {
			slidesPerView: 2.5,
			spaceBetween: 15,
		  },
		  800: {
			slidesPerView: 3.5,
			spaceBetween: 15,
		  },
		}
	  });
	  /*태그 슬라이드*/
	  var swiper03 = new Swiper('.swiper-container03 ', {
		slidesPerView: 6,
		spaceBetween: 5,
		autoplay: false,
		breakpoints: {
		  375: {
			slidesPerView: 6,
			spaceBetween: 5,
		  },
		  450: {
			slidesPerView: 6,
			spaceBetween: 5,
		  },
		  800: {
			slidesPerView: 6.5,
			spaceBetween: 5,
		  },
		}
	  });

	</script>
