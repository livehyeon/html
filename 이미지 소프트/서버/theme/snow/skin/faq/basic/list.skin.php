<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
?>
<script>
$(function(){
	$('.slide_question').click(function() {
		$(this).next(".slide_answer").slideToggle(100);
	})
})
</script>
<!-- //모바일 메뉴 로그인 후   -->
<div id="container">
	<div class="wrapper">
		<div class="service_boxArea">
			<h2>자주묻는 질문</h2>
			<?php
			$on = 'faq';
			include_once("{$service_skin_path}/top.skin.php");
			?>
			<div class="slide_boxArea">
				<div class="cont_topArea">
					<p class="s_pTxt">총 <em class="yellow"><?php echo  $total_count?></em>개의 소식이 있습니다. </p>
					<div class="right" style="max-width:330px">
						<!--
						<select name="" id="" class="btn_gray_w select01">
							<option value="">선택</option>
							<option value="">선택</option>
							<option value="">선택</option>
						</select>
					-->
						<div class="searchArea">
						    <form name="faq_search_form" method="get">
								<fieldset>
									<legend>검색어 입력폼</legend>
									<div class="search_box">

										<input type="text" name="stx" value="<?php echo $stx;?>" required class="search_txt" id="search" placeholder="검색어 입력">
										<button id="img_search" type="submit">
											<span class="s_txt">검색</span>
										</button>
									</div>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
				<div class="slide_listBox">
					<ul>
						<?php foreach($faq_list as $key=>$v){ ?>
						<li>
							<div class="slide_question"><p><em>Q</em><?php echo conv_content($v['fa_subject'], 1); ?></p></div>
							<div class="slide_answer"><p><em>A</em><?php echo conv_content($v['fa_content'], 1); ?></p></div>
						</li>
						<?php } ?>
					</ul>
				</div>
			</div>
			<!-- 아코디언 슬라이드 박스 -->
		</div>
		<?php echo get_paging_new($page_rows, $page, $total_page, $_SERVER['SCRIPT_NAME'].'?'.$qstr.'&amp;page='); ?>
	</div>
	<!--  wrapper -->
</div>
