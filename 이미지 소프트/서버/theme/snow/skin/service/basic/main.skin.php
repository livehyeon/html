<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
?>
<div id="container">
	<div class="wrapper">
		<div class="service_boxArea">
			<h2>고객센터</h2>
			<?php
			$on = 'main';
			include_once("{$service_skin_path}/top.skin.php");
			?>
			<div class="service_tblBox">
				<div class="cont_topArea">
					<strong class="s_titleTxt">공지사항</strong>
					<a href="<?php echo get_pretty_url('notice');  ?>" class="more_btn">더보기</a>
				</div>
				<table class="tableBox">
					<caption>공지사항</caption>
					<colgroup>
						<col style="width:*">
						<col style="width:*">
					</colgroup>
					<tbody>
					<?php

					$list = array();
					$i = 0;
					$notice_count = 0;
					$bo_table = 'notice';
					$notice_array = array();
					$board = get_board_db($bo_table, true);
					$arr_notice = explode(',', trim($board['bo_notice']));
					$board_notice_count = count($arr_notice);
					$write_table = $g5['write_prefix'] . $bo_table; // 게시판 테이블 전체이름

					$list_page_rows = 5;
					for ($k=0; $k<$board_notice_count; $k++) {
						if (trim($arr_notice[$k]) == '') continue;
					    $row = sql_fetch(" select * from {$write_table} where wr_id = '{$arr_notice[$k]}' ");

						if (!$row['wr_id']) continue;

				        $notice_array[] = $row['wr_id'];

				        if($k < $from_notice_idx) continue;

				        $list[$i] = get_list($row, $board, $board_skin_url, G5_IS_MOBILE ? $board['bo_mobile_subject_len'] : $board['bo_subject_len']);
				        $list[$i]['is_notice'] = true;
				        $list[$i]['num'] = 0;
				        $i++;
				        $notice_count++;

				        if($notice_count >= $list_page_rows)
				            break;

					}
					if($notice_count < $list_page_rows){
						$sql = " select * from {$write_table} where wr_is_comment = 0 and wr_id not in (".implode(', ', $notice_array).") order by wr_num asc limit ".($list_page_rows-$notice_count) ;
						$result = sql_query($sql);
						while ($row = sql_fetch_array($result)){
							$list[$i] = get_list($row, $board, $board_skin_url, G5_IS_MOBILE ? $board['bo_mobile_subject_len'] : $board['bo_subject_len']);
							$i++;
					 	}
					}
					for($i=0;$i<count($list);$i++){
						$class = $list[$i]['is_notice'] ? ' class="bg_noticeTitle"' : '';
					?>
						<tr<?php echo $class; ?>>
							<!--공지사항일때 bg_noticeTitle 클래스 추가 -->
							<td class="td_txt">
								<a href="<?php echo $list[$i]['href'] ?>"><?php if($class) echo '<strong class="notice_tag">공지</strong>'?><?php echo $list[$i]['subject'] ?></a>
								<?php if ($list[$i]['icon_new']){ ?><span><img src="<?php echo G5_RESOURCE_URL; ?>/images/contents/ico_n.png" alt=""></span><?php  } ?>
								<?php if ($list[$i]['icon_file']){ ?><span><img src="<?php echo G5_RESOURCE_URL; ?>/images/contents/ico_file.png" alt=""></span><?php  } ?>
							</td>
							<td class="td_date"><?php echo date("Y.m.d", strtotime($list[$i]['wr_datetime']))?></td>
						</tr>
					<?php
					}
					?>
					</tbody>
				</table>
			</div>
			<!--  //테이블박스  -->
			<!--  아코디언 슬라이드 박스  -->
			<div class="slide_boxArea">
				<div class="cont_topArea">
					<strong class="s_titleTxt">자주 묻는 질문</strong>
					<a href="<?php echo G5_BBS_URL; ?>/faq.php" class="more_btn">더보기</a>
				</div>
				<div class="slide_listBox">
					<ul>
					<?php
					$sql = " select *
				                from {$g5['faq_table']}
				                order by fa_order , fa_id
				                limit 0, 5 ";
				    $result = sql_query($sql);
					for ($i=0;$row=sql_fetch_array($result);$i++){
					?>
						<li>
							<div class="slide_question">
								<p><em>Q</em><?php echo conv_content($row['fa_subject'], 1); ?></p>
							</div>
							<div class="slide_answer">
								<p><em>A</em><span><?php echo conv_content($row['fa_content'], 1); ?></span></p>
							</div>
						</li>
					<?php  } ?>
					</ul>
				</div>
			</div>
			<!-- 아코디언 슬라이드 박스 -->
			<!--  테이블박스  -->
			<div class="service_tblBox">
				<div class="cont_topArea">
					<strong class="s_titleTxt">1대1 문의</strong>
					<a href="<?php echo get_pretty_url('qa'); ?>" class="more_btn">더보기</a>
				</div>
				<table class="tableBox">
					<caption>1대1 문의</caption>
					<col style="width:*">
					<col style="width:*">
					<tbody>
					<?php

					$sql = " select * from {$g5['write_prefix']}qa where wr_is_comment = '0' order by wr_num limit 5 ";
					$result = sql_query($sql);
					while($row=sql_fetch_array($result)){
						if($admin || $member['mb_id']==$row['mb_id'])
							$href = get_pretty_url('qa', $row['wr_id']);
						else
							$href = '#" onclick="alert(\'권한이 없습니다.\');return false';
					?>
						<tr>
							<td class="td_txt">
								<a href="<?php echo $href; ?>">
								<?php
									if($class) {
										echo '<strong class="notice_tag">공지</strong>';
									}else{
										if($row['wr_comment'])
											echo '<strong class="notice_tag fw400">완료</strong>';
										else
											echo '<strong class="notice_tag gray fw400">대기</strong>';
									}
									echo $row['wr_subject'];
								?>
								</a>
							</td>
							<td class="td_date"><?php echo date("Y.m.d", strtotime($row['wr_datetime'])); ?></td>
						</tr>
					<?php
					}
					?>
					</tbody>
				</table>
			</div>
			<!--// 테이블 박스-->
		</div>
		<!--// service_boxArea -->
		<div class="service_banner">
			<div class="service_txtBox">
				<div class="number_box">
					<span class="tel_number">02-123-4567</span>
					<span class="time_number">평일 09:00 ~ 18:00</span>
				</div>
				<div class="date_box">
					<span>점심시간 12:00 ~ 13:00</span>
					<span>주말 및 공휴일 휴무</span>
				</div>
			</div>
		</div>
		<!-- //service_banner -->
	</div>
	<!--  wrapper -->
</div>
<!--  //container      -->
<script>
  $('.slide_question').click(function() {

	$(this).toggleClass('on');
	$(this).next(".slide_answer").toggleClass('on');
  })

</script>
