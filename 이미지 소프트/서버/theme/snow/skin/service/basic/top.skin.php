<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
?>
<div class="top_listBox">
	<ul>
		<!-- on클래스는 선택된 페이지 l_lineNone클래스는 모바일에서 border-left선 지우는것         -->
		<li><a href="<?php echo G5_SERVICE_URL; ?>/index.php" class="<?php if($on=='main') echo 'on'; ?>">고객센터</a></li>
		<li><a href="<?php echo get_pretty_url('notice');  ?>" class="<?php if($on=='notice') echo 'on'; ?>">공지사항</a></li>
		<li><a href="<?php echo get_pretty_url('content', 'provision'); ?>" class="<?php if($on=='provision') echo 'on'; ?>">이용약관</a></li>
		<li class="l_lineNone"><a href="<?php echo get_pretty_url('content', 'privacy'); ?>" class="<?php if($on=='privacy') echo 'on'; ?>">개인정보 취급방침</a></li>
		<li><a href="<?php echo get_pretty_url('content', 'license'); ?>" class="<?php if($on=='license') echo 'on'; ?>">라이선스 약관</a></li>
		<li><a href="<?php echo get_pretty_url('content', 'contents'); ?>" class="<?php if($on=='contents') echo 'on'; ?>">콘텐츠 사용 안내</a></li>
		<li class="l_lineNone"><a href="<?php echo G5_BBS_URL; ?>/faq.php" class="<?php if($on=='faq') echo 'on'; ?>">자주묻는 질문</a></li>
		<li><a href="<?php echo get_pretty_url('qa'); ?>" class="<?php if($on=='qa') echo 'on'; ?>">1:1문의</a></li>
	</ul>
</div>
