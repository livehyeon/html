<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
?>
<script>
$(function(){
	$("#wrap").addClass('rule_wrap');
})
</script>


<div id="container">
	<div class="wrapper">
		<div class="service_boxArea">
			<h2><?php echo $co['co_subject']?></h2>
			<?php
			$on = $co_id;
			include_once("{$service_skin_path}/top.skin.php");

			echo $co['co_content'];
			?>

			<!--  //서비스 룰 박스  -->
		</div>
	</div>
	<!--  wrapper -->
</div>
<!--  //container      -->
