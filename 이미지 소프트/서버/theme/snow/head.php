<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
if (defined('G5_IS_ADMIN')) return; // 개별 페이지 접근 불가
if (G5_IS_MOBILE) {
	include_once(G5_THEME_MOBILE_PATH.'/head.php');
	return;
}


include_once(G5_THEME_PATH.'/head.sub.php');
include_once(G5_LIB_PATH.'/latest.lib.php');
include_once(G5_LIB_PATH.'/outlogin.lib.php');
include_once(G5_LIB_PATH.'/poll.lib.php');
include_once(G5_LIB_PATH.'/visit.lib.php');
include_once(G5_LIB_PATH.'/connect.lib.php');
include_once(G5_LIB_PATH.'/popular.lib.php');
?>
<div id="wrap" class="<?php echo $wrap_class; ?>">
	<?php
	if(defined('_INDEX_')) { // index에서만 실행
		include G5_BBS_PATH.'/newwin.inc.php'; // 팝업레이어
	}
	?>
	<header class="header_wrap">
		<div class="header">
			<h1><a href="<?php echo G5_URL; ?>"></a></h1>
			<ul class="gnb">
				<li><a href="<?php echo G5_URL; ?>">상품소개</a></li>
				<li><a href="<?php echo G5_ITEM_URL ?>/guide.php">이용방법</a></li>
				<li><a href="<?php echo G5_SERVICE_URL ?>/index.php">고객센터</a></li>
			</ul>
			<?php if($is_member){ ?>
			<div class="members_name">
				<a class="mem_nameTxt"><?php echo $member['mb_name']?> 님</a>
				<div class="members_listBox">
					<?php if($is_membership){?>
					<!-- 이용권을 구매하였을 경우  -->
					<div class="topBox membership">
						<p>만료일:<?php echo date("Y.m.d", strtotime($membership_expiry_date))?></p>
						<a href="<?php echo G5_MYPAGE_URL ?>/membership.php" class="btn_orange">이용권 관리</a>
					</div>
					<?php }else{ ?>
					<!-- 이용권을 구매하지 않았을경우  -->
					<div class="topBox demand">
						<p>이용권을 구매해보세요.</p>
						<a href="<?php echo G5_MYPAGE_URL ?>/order.php" class="btn_orange">이용권 구매</a>
					</div>

					<?php } ?>
					<ul>
						<li><a href="<?php echo G5_MYPAGE_URL?>">마이페이지 홈</a></li>
						<li><a href="<?php echo G5_MYPAGE_URL; ?>/info.php">회원정보</a></li>
						<li><a href="<?php echo G5_MYPAGE_URL; ?>/membership.php">내 이용권</a></li>
						<li><a href="<?php echo G5_MYPAGE_URL; ?>/coupon.php">쿠폰함</a></li>
						<li><a href="<?php echo G5_MYPAGE_URL; ?>/zzim.php">찜 목록</a></li>
						<li><a href="<?php echo G5_MYPAGE_URL; ?>/download.php">다운로드내역</a></li>
						<li><a href="<?php echo G5_MYPAGE_URL; ?>/order.php">주문/결제 내역</a></li>
						<li><a href="<?php echo G5_MYPAGE_URL; ?>/qa.php">내 문의함</a></li>
						<?php if($is_admin){ ?><li><a href="<?php echo G5_ADMIN_URL; ?>">관리자</a></li><?php }?>
						<li><a href="<?php echo G5_BBS_URL ?>/logout.php" class="logoutBtn">로그아웃</a></li>
					</ul>
				</div>
			</div>
			<script>
			/*개인회원 멤버 팝업*/
			$('.mem_nameTxt').click(function() {
				$(this).toggleClass('on');
				$('.members_listBox').toggleClass('on');
			})
			</script>
			<?php }else{ ?>
			<ul class="joinArea">
				<li class="login"><a href="<?php echo G5_BBS_URL ?>/login.php">로그인</a></li>
				<li class="join"><a href="<?php echo G5_BBS_URL ?>/register_form.php">회원가입</a></li>
			</ul>
			<?php } ?>
			<div class="m_menuBtn">
				<span></span>
				<span></span>
				<span></span>
			</div>
		</div>
	</header>
	<!-- //header  -->
	<?php if($is_member){ ?>
	<!-- 모바일 메뉴 로그인 후   -->
	<div class="m_menuArea menu_login">
		<div class="m_menuBox">
			<div class="top_box">
				<div class="joinMembers">
					<p><?php echo $member['mb_name']?>님</p>
					<a href="<?php echo G5_BBS_URL ?>/logout.php" class="m_logoutBtn"></a>
				</div>
				<div class="m_closeBtn">
					<span></span>
					<span></span>
				</div>
			</div>
			<div class="membersBox">
				<?php if($is_membership){ ?>
				<!-- 이용권을 구매 하였을 경우, 사용기간 노출 텍스트 -->
				<strong class="membership_txt">만료일 : <?php echo date("Y. m. d", strtotime($membership_expiry_date))?></strong>
				<a href="<?php echo G5_MYPAGE_URL; ?>/membership.php" class="membership_btn">이용권 관리</a>
				<?php }else{ ?>
				<!-- 이용권을 구매 하지 않았을 경우 노출 텍스트 -->
				<strong class="demand_txt">이용권을 구매해주세요.</strong>
				<a href="<?php echo G5_MYPAGE_URL; ?>/order.php" class="membership_btn">이용권 구매</a>
				<?php } ?>

			</div>
			<div class="members_list">
				<ul>
					<li><a href="<?php echo G5_MYPAGE_URL?>">마이페이지</a></li>
					<li><a href="<?php echo G5_MYPAGE_URL; ?>/info.php">회원정보</a></li>
					<li><a href="<?php echo G5_MYPAGE_URL; ?>/membership.php">내 이용권</a></li>
					<li><a href="<?php echo G5_MYPAGE_URL; ?>/coupon.php">쿠폰함</a></li>
					<li><a href="<?php echo G5_MYPAGE_URL; ?>/zzim.php">찜 목록</a></li>
					<li><a href="<?php echo G5_MYPAGE_URL; ?>/download.php">다운로드내역</a></li>
					<li><a href="<?php echo G5_MYPAGE_URL; ?>/order.php">주문/결제 내역</a></li>
					<li><a href="<?php echo G5_MYPAGE_URL; ?>/qa.php">내 문의함</a></li>
				</ul>
			</div>
			<div class="list_box">
				<ul>
					<li class="l_list">
						<a href="<?php echo G5_URL?>">상품소개</a>
					</li>
					<li class="l_list">
						<a href="<?php echo G5_ITEM_URL ?>/guide.php">이용방법</a>
					</li>
					<li class="l_list">
						<a href="javascript:void(0);" class="m_customerBtn">고객센터</a>
						<ul class="m_customerBox">
							<li><a href="<?php echo G5_SERVICE_URL ?>/index.php">메인</a></li>
							<li><a href="#">공지사항</a></li>
							<li><a href="#">이용약관</a></li>
							<li><a href="#">개인정보 취급방침</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>

	</div>
	<!-- //모바일 메뉴 로그인 후   -->
	<?php } else{ ?>
	<!-- 모바일 메뉴 로그인전  -->
	<div class="m_menuArea">
		<div class="m_menuBox">
			<div class="top_box">
				<ul class="joinArea">
					<li class="login"><a href="<?php echo G5_BBS_URL ?>/login.php">로그인</a></li>
					<li class="join"><a href="<?php echo G5_BBS_URL ?>/register.php">회원가입</a></li>
				</ul>
				<div class="m_closeBtn">
					<span></span>
					<span></span>
				</div>
			</div>
			<div class="list_box">
				<ul>
					<li class="l_list">
						<a href="">상품소개</a>
					</li>
					<li class="l_list">
						<a href="<?php echo G5_ITEM_URL ?>/guide.php">이용방법</a>
					</li>
					<li class="l_list">
						<a href="javascript:void(0);" class="m_customerBtn">고객센터</a>
						<ul class="m_customerBox">
							<li><a href="<?php echo G5_SERVICE_URL ?>/index.php">메인</a></li>
							<li><a href="#">공지사항</a></li>
							<li><a href="#">이용약관</a></li>
							<li><a href="#">개인정보 취급방침</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- //모바일 메뉴 로그인전       -->
	<?php } ?>
