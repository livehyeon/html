<?php
if (!defined('_INDEX_')) define('_INDEX_', true);
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

if (G5_IS_MOBILE) {
    include_once(G5_THEME_MOBILE_PATH.'/index.php');
    return;
}
$wrap_class = 'main_wrap';
include_once(G5_THEME_PATH.'/head.php');

$banner = array();
$sql = "select * from {$g5['banner_table']} where ('".G5_TIME_YMDHIS."' between bn_sdate and bn_edate) order by bn_order desc, bn_sdate desc, bn_no desc ";
$result = sql_query($sql);
while($row=sql_fetch_array($result))
	$banner[] = $row;
?>
<script>
var list_timer;
$(function(){
	get_list();

	$("#item-list").on("click", ".ico_rabbit", function(){
		$this = $(this);
		$it_id = $this.attr('data-id');
		$.get("<?php echo G5_ITEM_URL; ?>/ajax.zzim.php?it_id="+$it_id, function($response){
			if($response.msg){
				alert($response.msg);
				return false;
			}
			if($response.status=='1'){
				$this.addClass('on');
			}else{
				$this.removeClass('on');
			}
		}, 'json');

		return false;
	});

	$("#tab_search").submit(function(){
		get_list();
		return false;
	});

	$(".category").click(function(){
		clearTimeout(list_timer);
		list_timer = setTimeout(function(){
			get_list();
		}, 500)
	});

	$(".btn-sst").click(function(){
		$(".btn-sst").removeClass('on');
		$(this).addClass('on');
		$sst = $(this).attr('data-sst');
		$("#sst").val($sst);
		get_list();
		return false;
	});
});

function get_list(){
	clearTimeout(list_timer);
	$serialize = $("#tab_search").serialize();
	$.post("<?php echo G5_ITEM_URL; ?>/ajax.itemlist.php", $serialize, function($list){
		$("#item-list").html($list);
	});
}
</script>
	<!--  pc 메인배너-->
	<div class="mainBanner">
		<div class="main_bannerArea">
			<?php
			for($i=0;$i<count($banner);$i++){
				$row = $banner[$i];
				$class = $i%2==0?'left':'right';
			?>
			<div class="slide_backImg b_areaBox <?php echo $class ?> ">
				<a href="<?php echo $row['bn_url']; ?>" target="<?php echo $row['bn_target']=='1'?'_blank':'_self'  ?>" class="bannerArea"  >
					<img src="<?php echo G5_DATA_BANNER_URL."/{$row['bn_file1']}"; ?>" alt="">
				</a>
			</div>
			<?php } ?>
		</div>
	</div>
	<!-- 모바일 메인배너 -->
	<div class="mobile mainBanner swiper-container01">
		<div class="main_bannerArea swiper-wrapper">
			<?php
			for($i=0;$i<count($banner);$i++){
				$row = $banner[$i];
			?>
			<div class="slide_backImg b_areaBox swiper-slide " >
				<a href="<?php echo $row['bn_url']; ?>" target="<?php echo $row['bn_target']=='1'?'_blank':'_self'  ?>" class="bannerArea">
					<img src="<?php echo G5_DATA_BANNER_URL."/{$row['bn_file2']}"; ?>" alt="">
				</a>
			</div>
				<?php } ?>
		</div>
		<div class="swiper-pagination"></div>
	</div>

	<div id="container">

		<div class="wrapper">
			<div class="recom_slideArea">
				<div class="slide_arrowBox">
					<p><span>추천 상품을</span> 만나 보세요!</p>
					<div class="arrow_btnArea">
						<div class="swiper-button-prev"></div>
						<div class="swiper-button-next"></div>
						<!-- //스와이퍼 네비게이션 버튼-->
					</div>
				</div>
				<div class="swiperArea">
					<div class="slide_contBox swiper-container">
						<ul class="swiper-wrapper">
							<?php
							$result = sql_query(" select * from {$g5['item_table']} where it_use = '1' and it_main = '1' order by it_no desc ", true);
							while($row = sql_fetch_array($result)){
								$thumb = G5_DATA_ITEM_URL."/{$row['it_thumb']}";
								$subject = $row['it_name'];//"[{$g5['category_array'][$row['category']]}] {}";
							?>
							<li class="img_list swiper-slide">
								<a href="<?php echo G5_ITEM_URL; ?>/view.php?it_id=<?php echo $row['it_id']?>" target='_blank'>
									<div class="recom_imgBox">
										<img src="<?php echo $thumb; ?>" alt="<?php echo $subject; ?>">
									</div>
									<span class="list_txt"><?php echo $subject; ?></span>
								</a>
							</li>
							<?php
							}
							?>
						</ul>
					</div>
				</div>
			</div>
			<!-- //recomImg_slideArea -->
			<div class="main_dataList">
				<div class="left_contentArea">
					<div class="tab_filterArea">
						<h3>이미지 리스트</h3>

						<ul class="tab_list">
							<li>
								<a href="#" class="btn-sst tab_btn on" data-sst="it_hot">인기순</a>
							</li>
							<li>
								<a href="#" class="btn-sst tab_btn" dat-sst="it_no">최신순</a>
							</li>
						</ul>
						<form action="<?php echo G5_ITEM_URL; ?>/ajax.itemlist.php" id="tab_search">
						<input type="hidden" name="sst" id="sst" value="it_hot">
						<input type="hidden" name="sod" value="desc">
						<div class="searchArea">
							<fieldset>
								<legend>검색어 입력폼</legend>
								<div class="search_box">
									<input type="text" name="tag" value="<?php echo $tag; ?>" class="search_txt" id="search" placeholder="검색어 입력">
									<button id="img_search"  type="submit">
										<span class="s_txt">검색</span>
									</button>
								</div>
							</fieldset>
						</div>

						<!-- //검색박스  -->
						<div class="filter_boxArea">
							<?php

							foreach($g5['category_tree_array'] as $ca_id => $array){
								if($array['use'] != '1') continue;
							?>
							<div class="filter_listBox">
								<strong class="list_titTxt"><?php echo $array['name']; ?></strong>
								<div class="list_contBox">
									<?php
									foreach($array['sub'] as $ca_id1 => $array1){
										if($array1['use'] != '1') continue;
									?>
									<div class="list_cont">
										<input type="checkbox" name="category[]" value="<?php echo $ca_id1; ?>" class="inp_cont category" id="nyear<?php echo $ca_id1;' '?>">
										<label for="nyear<?php echo $ca_id1;' '?>">
											<span><?php echo $array1['name']; ?></span>
										</label>
									</div>
									<?php } ?>
								</div>
							</div>
							<?php } ?>
							<div class="m_filter_btnBox">
								<a href="javascript:void(0);" class="m_filter_btn">더보기</a>
							</div>
						</div>
					</div>
					</form>
					<!--  //필터 리스트 -->
					<div class="customer_boxArea">
						<a href="<?php echo get_pretty_url('talktalk'); ?>" class="customer_appeal">도안요청하기</a>
						<a href="<?php echo G5_SERVICE_URL; ?>/index.php" class="customer_service">고객센터</a>
					</div>
					<div class="footer">
						<ul>
							<li><a href="<?php echo G5_BBS_URL; ?>/faq.php">자주묻는 질문</a></li>
							<li><a href="<?php echo get_pretty_url('qa'); ?>">문의하기</a></li>
							<li><a href="<?php echo get_pretty_url('content', 'provision'); ?>">이용약관</a></li>
							<li><a href="<?php echo get_pretty_url('content', 'privacy'); ?>">개인정보 처리방침</a></li>
							<li>imagepage©All rights reserved</li>

						</ul>
					</div>
				</div>
				<div class="main_contentArea" id="item-list"></div>
			</div>
		</div>
		<!--  wrapper -->
	</div>
	<!--  //container      -->
	<div class="top">
		<a href="#"></a>
	</div>
</div>
<!-- //wrap-->

<script>
var swiper = new Swiper('.swiper-container01', {
  slidesPerView: 1,
 spaceBetween: 18,
pagination: {
el: '.swiper-pagination',
dynamicBullets: true,
},
});
</script>

<script>
var swiper = new Swiper('.swiper-container', {
	slidesPerView: 6,
	spaceBetween: 20,
//            loop: true,
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
	},
	breakpoints: {
		600: {
			slidesPerView: 2.5,
			spaceBetween: 15,
		},
		767: {
			slidesPerView: 3,
			spaceBetween: 15,
		},

		1000: {
			slidesPerView: 3,
			spaceBetween: 15,
		},
		1300: {
			slidesPerView: 4,
			spaceBetween: 20,
		},
		1500: {
			slidesPerView: 5,
			spaceBetween: 20,
		},
	}
});
</script>
<?php
include_once(G5_THEME_PATH.'/tail.sub.php');
