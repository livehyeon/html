<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

if (G5_IS_MOBILE) {
    include_once(G5_THEME_MOBILE_PATH.'/tail.php');
    return;
}
?>

<footer class="footer_wrap">
  <div class="footer">
	<ul>
	  <li><a href="#">자주묻는 질문</a></li>
	  <li><a href="#">문의하기</a></li>
	  <li><a href="#">이용약관</a></li>
	  <li><a href="#">개인정보 처리방침</a></li>
	</ul>
	<div class="f_reserve">imagepage©All rights reserved</div>
  </div>
</footer>
</div>
<!-- //wrap-->

<?php
include_once(G5_THEME_PATH."/tail.sub.php");
?>
